#!/usr/bin/env python3

from json    import loads
from pathlib import Path
from typing  import List, Tuple
from typing  import Tuple

import itertools
import matplotlib.pyplot as plt
import os
import re
import sys

def map_with_previous(prm_sequence):
	"""
	TODOCUMENT
	"""
	prev_item = None
	for item in prm_sequence:
		yield ( prev_item, item )
		prev_item = item

def add_indents_for_fitness_steps(generation_fitness_pairs):
	"""
	TODOCUMENT

	This handles general sequences including comprehensions.
	"""
	def step_fn(prev_entry_opt, this_entry):
		( this_gen, this_fitness ) = this_entry
		if prev_entry_opt:
			( prev_gen, prev_fitness ) = prev_entry_opt
			if prev_gen != this_gen - 1:
				return [
					( this_gen - 1, prev_fitness ),
					( this_gen,     this_fitness )
				]
		return [
			( this_gen, this_fitness )
		]
	return list( itertools.chain.from_iterable(
		step_fn( x, y ) for x, y in map_with_previous( generation_fitness_pairs )
	) )

# print( repr( add_indents_for_fitness_steps( [ ( 1, 50 ), ( 8, 60 ), ( 10, 80 ) ] ) ) )


if len( sys.argv ) != 2:
	print( "Usage: " + sys.argv[ 0 ] + ' directory to summarise' )
	exit()

prog_name, data_dirname = sys.argv
data_dir = Path( data_dirname )

def file_sort_key( prm_file: Path ) -> List:
	parts = re.split( '\.|_', prm_file.name )
	for index, part in enumerate(parts):
		try:
			parts[ index ] = int( part )
		except:
			pass
	return parts

def make_to_file(recurse_dir_and_file: Tuple[ Path, str ]) -> Path:
		( recurse_dir, file ) = recurse_dir_and_file
		# return Path( recurse_dir ).relative_to( data_dir ) / file
		return Path( recurse_dir ) / file

fitness_files = sorted(
	filter( lambda x: x.name.startswith( 'fitnesses' ) and x.name.endswith( '.json' ),
		map( make_to_file,
			( ( Path( recurse_dir ), file ) for recurse_dir, _, files in os.walk( data_dir ) for file in files )
		)
	),
	key=file_sort_key
)

for fitness_file in fitness_files:
	print( fitness_file )

fitness_data = {}
for fitness_file in fitness_files:
	if not fitness_file.exists():
		continue
	with open( fitness_file, 'r') as fitnesses_fh:
		fitnesses_data_from_json = loads(fitnesses_fh.read())
	num_instrs_name = re.search( r'fitnesses.*\.(\d+)_instrs.*\.\d+_loops.*.json', fitness_file.name )

	if num_instrs_name:
		num_instrs = num_instrs_name.group(1)
	else:
		print( 'Cannot get num instrs from ' + fitness_file.name )
		exit()

	data = add_indents_for_fitness_steps( (x['generation'], x['fitness']) for x in  fitnesses_data_from_json )

	# print(repr(data))
	# exit()

	if num_instrs not in fitness_data:
		fitness_data[ num_instrs ] = []
	fitness_data[ num_instrs ].append( (
		[ x[ 0 ] for x in data ],
		[ x[ 1 ] for x in data ]
	) )

# print(repr(fitness_data))
# exit()

# https://towardsdatascience.com/customizing-plots-with-python-matplotlib-bcf02691931f

colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']

fig, ax = plt.subplots(figsize=(16, 9))
for index, variable in enumerate(reversed(sorted(fitness_data.keys()))):
	datas =  fitness_data[variable]

	# print(repr(datas))
	# exit()
	plot_data = []
	for data_pair_idx, data_pair in enumerate(datas):
		# plot_data.extend( data_pair )

		ax.plot(
			*data_pair,
			label= ( '_nolegend_' if data_pair_idx > 0 else variable ),
			alpha=0.9,
			color=colors[ index % len( colors ) ],
			linewidth=0.4
		)

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

ax.set_xlabel('Generation')
ax.set_ylabel('Fitness')

# Major gridlines
ax.grid(color='grey', linestyle='-', linewidth=0.3, alpha=0.4)

ax.set_xlim( left=0, right=300000000 )
ax.set_ylim( bottom=-4000, top=0 )
ax.legend(loc='upper left')
plt.savefig( 'explore.pdf' )
# plt.show()
