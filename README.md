# evo-engine

## Stuff to look at soon

* sort out `engine_main.cpp` a bit - extract into re-usable functions a bit
* Find out if I'm too focused on avoiding new stuff depending on existing stuff: test effect of enabling advanced action_cats.
* &hellip;and if that focus was correct, consider principle: only depend on stuff if it's able to copy and diverge itself free.
* Start writing out a standard run results file
* &hellip;and use that to get a Python framework to do a simple experiment to learn about the effect of varying the number of threads on twinklestar on the total/per-thread rate.
* Python framework to keep extending results (increase runs per setup; extend length of runs)
* Try experiment to add crossover, possibly with a bit of random noise
* Add test for serializing an individual
* Generate a good, small individual; have a good look at it. Obvious errors?
* Create a Python runner (
  * start by duplicating what I'm doing with xargs (eg `seq 40 | awk '{print 40 * ( 19 + $1 - ( ( $1 - 1 ) % 5 ) ) / 5 }' | tac | xargs -P 32 -I VAR echo /home/lewis/evo-engine/ninja_gcc_rwdi/bin/engine -g 300000000 -i VAR`)
  * allow repeats for a value, eg 10, 10, 20, 20,...
  * allowing queueing of experiments - eg whilst this 4 day experiment is running, I spec up a new one and add it behind in a queue
* Improve the code/handling for dumping fitnesses as the run progresses
* What are the experiments that it's worth doing soon?
* What code is required to implement them?
* How should experiments be recorded?
  * a standard format directory with spec `json` and post-run conclusions `json`
  * name like: `2019115-how-many-instrs-so-some-remain-unused`
* What should be stored in a run dump? The spec? Separate original spec and updated spec?
* Prioritised testing plan
* Have a go at testing coverage?
* Start designing: comparison with second genome **important to understand soon: does second genome help?**
* Start designing: breakthrough bisection
* Start designing: knockout

## Books

* Read about Quantitative Genetics (maybe "Quantitative Genetics" by Armando Caballero?)
* Read "Reinforcement Learning: An Introduction" (Sutton, Barto)?
* Read "The Idea of the Brain: A History" (Cobb)?

## General notes

~~~sh
cmake-all-of clang_dbgchk clang_debug clang_memsan clang_rwdi clang_thrsan clang_ubasan gcc_dbgchk gcc_debug gcc_rwdi gcc_ubasan

recmake-all
~~~

~~~sh
export BUILDTYPE=ninja_gcc_debug
export BUILDTYPE=ninja_clang_ubasan
ninja -C ${BUILDTYPE} -k 0 && ${BUILDTYPE}/bin/engine
~~~

## C++20

Cereal seems to be the only obstacle...

* One compiler error involving an attempt to construct a LockGuard without parameters appears to be fixed since 1.3.0
* Ambiguous equality operator within rapidjson : https://github.com/USCiLab/cereal/issues/631#issuecomment-678979280

## Current representation design

Bunch of nodes

Each node contains:

* [dynamic] **comparator**, just a value
* [fixedish] **input** [fixed under standard actions] can be set to a value or a reference to the loop counter or either testcase

* [dynamic] **value ("cargo")** can be initialised to a constant or the value of either testcase
* [fixedish] **action** the action it's performing on another node (and in advanced cases a third-party node it's using)
* [fixedish] **target** the node+component that this is acting on

(where fixedish means fixed under the basic operations)

## Check headers with `clang-tidy`

These commands don't sort out include of required headers&hellip;

~~~sh
lsc | grep -v 3rd |                   xargs -I VAR unbuffer ~/source/llvm/bin/clang-tidy '-checks=*,-fuchsia*,-google-build-using-namespace,-google-runtime-references,-modernize-use-trailing-return-type,-readability-named-parameter' VAR -- ${=CMAKE_EXTRACTED_COMPILE_FLAGS} ${=TEL_BASE_CLANG_COMPILE_ARGS} ${=TEL_COMPILE_WARNINGS}
~~~

Headers only:

~~~sh
lsc | grep -v 3rd | grep -P '\.hpp' | xargs -I VAR unbuffer ~/source/llvm/bin/clang-tidy '-checks=*,-fuchsia*,-google-build-using-namespace,-google-runtime-references,-modernize-use-trailing-return-type,-readability-named-parameter' VAR -- ${=CMAKE_EXTRACTED_COMPILE_FLAGS} ${=TEL_BASE_CLANG_COMPILE_ARGS} ${=TEL_COMPILE_WARNINGS}
~~~

Fix, eg llvm-header-guard on headers-only:

~~~sh
lsc | grep -v 3rd | grep -P '\.hpp' | xargs -I VAR unbuffer ~/source/llvm/bin/clang-tidy '-checks=llvm-header-guard' -fix VAR -- ${=CMAKE_EXTRACTED_COMPILE_FLAGS} ${=TEL_BASE_CLANG_COMPILE_ARGS} ${=TEL_COMPILE_WARNINGS}
lsc | grep -v 3rd | grep -P '\.hpp' | xargs -I VAR unbuffer ~/source/llvm/bin/clang-tidy '-checks=llvm-namespace-comment' -fix VAR -- ${=CMAKE_EXTRACTED_COMPILE_FLAGS} ${=TEL_BASE_CLANG_COMPILE_ARGS} ${=TEL_COMPILE_WARNINGS}
~~~
