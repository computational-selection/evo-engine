cmake_minimum_required( VERSION 3.10 )

project( evo-engine CXX )

set( CONAN_SYSTEM_INCLUDES ON )
include( ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake )
conan_basic_setup(TARGETS)

enable_testing()

add_subdirectory( cpp-source )
