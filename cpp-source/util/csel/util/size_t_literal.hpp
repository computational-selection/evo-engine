/// \file
/// \brief The size_t literal header

#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SIZE_T_LITERAL_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SIZE_T_LITERAL_HPP

#include <cstdlib>

namespace csel {
	inline namespace literals {

		/// \brief User-defined literal for creating a size_t literal, eg `constexpr auto a = 3_z;`
		///
		/// TODO: Come C++23, use uz /zu instead ?
		///        * https://github.com/cplusplus/papers/issues/1
		///        * https://thephd.github.io/vendor/future_cxx/papers/d0330.html
		///        * https://wg21.link/p0330r8
		constexpr std::size_t operator "" _z ( unsigned long long int n ) { return static_cast<size_t>( n ); } // NOLINT(google-runtime-int) Can't use uint64 in a user-defined literal, as this clang-tidy check wants

	} // namespace literals
} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SIZE_T_LITERAL_HPP
