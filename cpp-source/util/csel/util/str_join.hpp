#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STR_JOIN_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STR_JOIN_HPP

#include <iterator>
#include <string>

#include "csel/util/cpp20/is_detected.hpp"
#include "csel/util/type_traits.hpp"

namespace csel {

	namespace detail {

		/// \brief Metafunction to get the is_string_appendable member type of T
		template <typename T>
		using string_appendable_return_type = decltype( ::std::string{} += ::std::declval<T>() );

		/// \brief metafunction that evaluates to std::true_type iff T has a sub type called is_string_appendable
		///         (to indicate it should be used in heterogeneous lookup) or std::false_type otherwise
		template <typename T>
		using is_string_appendable = csel::util::is_detected<string_appendable_return_type, T>;

		/// \brief Whether T contains a member type `is_string_appendable` (to indicate
		///        it should be used in heterogeneous lookup)
		template <typename T>
		inline constexpr bool is_string_appendable_v = is_string_appendable<T>::value;

		template <typename T>
		inline void append_thing_to_string( ::std::string &prm_string, const T &prm_thing ) {
			if constexpr ( is_string_appendable_v<remove_cvref_t<T>> ) {
				prm_string += prm_thing;
			} else {
				prm_string += to_string( prm_thing );
			}
		}
	} // namespace detail

	/// The result of joining the string (-like) entries in a range, using an optional separator
	///
	/// \param prm_rng       The range of things to join into a string. Each value in the range must be appendable to a string.
	/// \param prm_separator An optional separator between each element of the range
	template <typename Rng>
	inline ::std::string str_join( Rng &&prm_rng, const ::std::string &prm_separator = ::std::string{} ) {
		::std::string result;
		auto          itr     = ::std::begin( prm_rng );
		const auto    end_itr = ::std::end( prm_rng );
		if ( itr == end_itr ) {
			return result;
		}
		detail::append_thing_to_string( result, *itr );
		for ( ++itr; itr != end_itr; ++itr ) {
			result += prm_separator;
			detail::append_thing_to_string( result, *itr );
		}
		return result;
	}

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STR_JOIN_HPP
