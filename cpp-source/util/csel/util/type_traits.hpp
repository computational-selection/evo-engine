#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_TYPE_TRAITS_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_TYPE_TRAITS_HPP

#include <type_traits>

namespace csel {

	template <typename T>
	struct remove_cvref {
		using type = std::remove_cv_t<std::remove_reference_t<T>>;
	};

	template <typename T>
	using remove_cvref_t = typename remove_cvref<T>::type;

	template <typename T, typename U>
	inline constexpr bool is_same_mod_cvref_v = ::std::is_same_v<remove_cvref_t<T>, remove_cvref_t<U>>;

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_TYPE_TRAITS_HPP
