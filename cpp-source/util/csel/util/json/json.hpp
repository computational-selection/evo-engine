#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_JSON_JSON_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_JSON_JSON_HPP

#include <string>

#include <nlohmann/json.hpp>

#include "csel/util/type_traits.hpp"

namespace csel::util {

	/// Convert the specified value to a JSON string, via nlohmann json
	///
	/// \param prm_value The value to convert
	template <typename T>
	::std::string to_json_string( T &&prm_value ) {
		return ::nlohmann::json{ ::std::forward<T>( prm_value ) }[ 0 ].dump();
	}

	/// Convert the specified JSON string to an object of type T, via via nlohmann json
	///
	/// \param prm_json_string The JSON string to convert
	template <typename T>
	auto from_json_string( const ::std::string &prm_json_string ) {
		return ::nlohmann::json::parse( prm_json_string ).get<remove_cvref_t<T>>();
	}

	/// Convert the specified value to a JSON string and back again, via via nlohmann json
	///
	/// \param prm_value The value to convert
	template <typename T>
	auto json_round_trip( T &&prm_value ) {
		return from_json_string<remove_cvref_t<T>>( to_json_string( ::std::forward<T>( prm_value ) ) );
	}

} // namespace csel::util

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_JSON_JSON_HPP
