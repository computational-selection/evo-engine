#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_LERP_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_LERP_HPP

#include <algorithm>
#include <cmath>

namespace csel {

    /// Return the linear interpolation t of the way between a and b
    ///
    /// Requires Float is float or double
    ///
    /// TODO: Come C++20, replace this with std::lerp()
    template <typename Float>
    inline constexpr Float lerp( Float a, Float b, Float t ) {
        // Exact, monotonic, bounded, determinate, and (for a=b=0) consistent:
        if ( ( a <= 0 && b >= 0 ) || ( a >= 0 && b <= 0 ) ) {
            return t * b + ( 1 - t ) * a;
        }

        if ( t == 1 ) {
            return b; // exact
        }

        // Exact at t=0, monotonic except near t=1,
        // bounded, determinate, and consistent:
        const Float x = a + t * ( b - a );
        return ( ( t > 1 ) == ( b > a ) ) ? ::std::max( b, x ) : ::std::min( b, x ); // monotonic near t=1
    }

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_LERP_HPP
