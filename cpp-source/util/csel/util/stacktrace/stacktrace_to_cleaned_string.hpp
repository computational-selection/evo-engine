#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STACKTRACE_STACKTRACE_TO_CLEANED_STRING_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STACKTRACE_STACKTRACE_TO_CLEANED_STRING_HPP

#include <array>
#include <string>
#include <string_view>

#include <fmt/core.h>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/algorithm/none_of.hpp>
#include <range/v3/algorithm/starts_with.hpp>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/filter.hpp>
#include <range/v3/view/transform.hpp>

#include <boost/stacktrace.hpp>

#include "csel/util/str_join.hpp"
#include "csel/util/type_traits.hpp"

namespace csel::except {

	namespace detail {

		using ::std::literals::string_view_literals::operator""sv;

		/// \brief An array of string_views containing the prefixes of any stackframes to exclude from the output
		inline constexpr ::std::array standard_excluded_stackframe_prefixes = { "boost::stacktrace::basic_stacktrace"sv };

	} // namespace detail

	/// \brief Generate a string for the specified stacktrace, stripped of any frames with a name that has any of the specified prefixes
	///
	/// \param prm_stacktrace     The stacktrace to describe
	/// \param prm_strip_prefixes A range of prefixes to specify the stacktrace entries to be filtered out
	template <typename Rng = const decltype( detail::standard_excluded_stackframe_prefixes ) &>
	inline ::std::string to_string_stripped_by_prefixes( const ::boost::stacktrace::stacktrace &prm_stacktrace,
	                                                     Rng &&prm_strip_prefixes = detail::standard_excluded_stackframe_prefixes ) {
		// Lambda closure for whether the argument's `.name()` starts with any of the specified prefixes
		const auto does_not_start_with_a_prefix = [&]( const auto &x ) {
			return ::ranges::none_of( prm_strip_prefixes,
			                          [&]( auto &&y ) { return ::ranges::starts_with( x.name(), y ); } );
		};

		// Loop over the acceptable frames and append a description of each to a string, then return the string
		//
		// clang-format off
		return str_join(
			prm_stacktrace
				| ::ranges::views::filter( does_not_start_with_a_prefix )
				| ::ranges::views::enumerate
				| ::ranges::views::transform( [] ( const auto &indexed_frame ) {
					const auto &[ index, frame ] = indexed_frame;
					return ::fmt::format( "{:4d} {}\n", index, to_string( frame ) );
				} )
		);
		// clang-format on
	}

} // namespace csel::except

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_STACKTRACE_STACKTRACE_TO_CLEANED_STRING_HPP
