#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_ARCHIVE_PAIRS_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_ARCHIVE_PAIRS_HPP

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include "csel/util/cereal/serialize_fns.hpp"

namespace csel::util::cereal {

	struct unrecognised_archive_tag {
		unrecognised_archive_tag()                                       = delete;
		~unrecognised_archive_tag()                                      = delete;
		unrecognised_archive_tag( const unrecognised_archive_tag & )     = delete;
		unrecognised_archive_tag( unrecognised_archive_tag && ) noexcept = delete;
		unrecognised_archive_tag &operator=( const unrecognised_archive_tag & ) = delete;
		unrecognised_archive_tag &operator=( unrecognised_archive_tag && ) noexcept = delete;
	};

	namespace detail {

		template <typename Archive>
		struct output_archive_of_archive {
			using type = ::std::conditional_t<is_output_archive_v<Archive>, Archive, unrecognised_archive_tag>;
		};

		template <>
		struct output_archive_of_archive<::cereal::BinaryInputArchive> {
			using type = ::cereal::BinaryOutputArchive;
		};

		template <>
		struct output_archive_of_archive<::cereal::JSONInputArchive> {
			using type = ::cereal::JSONOutputArchive;
		};

		template <>
		struct output_archive_of_archive<::cereal::PortableBinaryInputArchive> {
			using type = ::cereal::PortableBinaryOutputArchive;
		};

		template <>
		struct output_archive_of_archive<::cereal::XMLInputArchive> {
			using type = ::cereal::XMLOutputArchive;
		};

		template <typename Archive>
		struct input_archive_of_archive {
			using type = ::std::conditional_t<is_input_archive_v<Archive>, Archive, unrecognised_archive_tag>;
		};

		template <>
		struct input_archive_of_archive<::cereal::BinaryOutputArchive> {
			using type = ::cereal::BinaryInputArchive;
		};

		template <>
		struct input_archive_of_archive<::cereal::JSONOutputArchive> {
			using type = ::cereal::JSONInputArchive;
		};

		template <>
		struct input_archive_of_archive<::cereal::PortableBinaryOutputArchive> {
			using type = ::cereal::PortableBinaryInputArchive;
		};

		template <>
		struct input_archive_of_archive<::cereal::XMLOutputArchive> {
			using type = ::cereal::XMLInputArchive;
		};

	} // namespace detail

	template <typename Archive>
	using output_archive_of_archive_t = typename detail::output_archive_of_archive<remove_cvref_t<Archive>>::type;

	template <typename Archive>
	using input_archive_of_archive_t = typename detail::input_archive_of_archive<remove_cvref_t<Archive>>::type;

} // namespace csel::util::cereal

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_ARCHIVE_PAIRS_HPP
