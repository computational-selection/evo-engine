#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include "csel/util/cereal/traits.hpp"

using namespace csel::util::cereal;

using ::cereal::BinaryInputArchive;
using ::cereal::BinaryOutputArchive;
using ::cereal::JSONInputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryInputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLInputArchive;
using ::cereal::XMLOutputArchive;

TEST_CASE( "is_input_archive_v" ) {
	static_assert( !is_input_archive_v<BinaryOutputArchive> );
	static_assert( !is_input_archive_v<JSONOutputArchive> );
	static_assert( !is_input_archive_v<PortableBinaryOutputArchive> );
	static_assert( !is_input_archive_v<XMLOutputArchive> );

	static_assert( is_input_archive_v<BinaryInputArchive> );
	static_assert( is_input_archive_v<JSONInputArchive> );
	static_assert( is_input_archive_v<PortableBinaryInputArchive> );
	static_assert( is_input_archive_v<XMLInputArchive> );
}

TEST_CASE( "is_output_archive_v" ) {
	static_assert( !is_output_archive_v<BinaryInputArchive> );
	static_assert( !is_output_archive_v<JSONInputArchive> );
	static_assert( !is_output_archive_v<PortableBinaryInputArchive> );
	static_assert( !is_output_archive_v<XMLInputArchive> );

	static_assert( is_output_archive_v<BinaryOutputArchive> );
	static_assert( is_output_archive_v<JSONOutputArchive> );
	static_assert( is_output_archive_v<PortableBinaryOutputArchive> );
	static_assert( is_output_archive_v<XMLOutputArchive> );
}
