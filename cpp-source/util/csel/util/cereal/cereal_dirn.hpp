#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_CEREAL_DIRN_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_CEREAL_DIRN_HPP

#include <string>

namespace csel::util {

	/// Whether a cereal serialisation is loading or saving
	enum class cereal_dirn : bool {
		LOADING, ///< Loading, ie building a data structure from serialisation
		SAVING   ///< Saving, ie forming a serialisation from a data structure
	};

	::std::string to_string( const cereal_dirn & );

} // namespace csel::util

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_CEREAL_DIRN_HPP
