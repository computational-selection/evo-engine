#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_TRAITS_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_TRAITS_HPP

#include <cstdint>
#include <type_traits>

#include <cereal/cereal.hpp>

#include "csel/util/type_traits.hpp"

namespace csel::util::cereal {

	namespace detail {

		/// A predicate trait for whether the first type is derived from the specified ArchiveBase type
		/// (::cereal::InputArchive or ::cereal::OutputArchive)
		template <typename T, template <typename, std::uint32_t> class ArchiveBase>
		struct is_derived_from_archive_base {
			// Match this if convertible to a pointer to ArchiveBase<U, Flags> and then return type indicating whether
			// different from ArchiveBase<U, Flags> (modulo cv-ref)
			template <typename U, std::uint32_t Flags>
			static auto test( ArchiveBase<U, Flags> * )
			  -> std::bool_constant<!std::is_same_v<remove_cvref_t<T>, ArchiveBase<T, Flags>>>;

			// Otherwise match this and return false_type
			static std::false_type test( void * );

			// Perform the test with a pointer to a value of the (cv-ref-stripped) type
			static constexpr bool value = decltype( test( std::declval<remove_cvref_t<T> *>() ) )::value;
		};

	} // namespace detail

	template <typename T>
	inline constexpr bool is_output_archive_v = detail::is_derived_from_archive_base<T, ::cereal::OutputArchive>::value;

	template <typename T>
	inline constexpr bool is_input_archive_v = detail::is_derived_from_archive_base<T, ::cereal::InputArchive>::value;

	template <typename T>
	inline constexpr bool is_archive_v = ::std::disjunction_v<is_output_archive_v<T>, is_input_archive_v<T>>;

	template <typename T>
	inline constexpr bool is_text_archive_v = ::cereal::traits::is_text_archive<T>::value;

} // namespace csel::util::cereal

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_TRAITS_HPP
