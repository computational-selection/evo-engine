#include <type_traits>

#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include "csel/util/cereal/archive_pairs.hpp"

using namespace csel::util::cereal;

using ::cereal::BinaryInputArchive;
using ::cereal::BinaryOutputArchive;
using ::cereal::JSONInputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryInputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLInputArchive;
using ::cereal::XMLOutputArchive;
using ::std::is_same_v;

TEST_CASE( "input_archive_of_archive_t" ) {
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<BinaryInputArchive>, BinaryInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<BinaryOutputArchive>, BinaryInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<JSONInputArchive>, JSONInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<JSONOutputArchive>, JSONInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<PortableBinaryInputArchive>, PortableBinaryInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<PortableBinaryOutputArchive>, PortableBinaryInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<XMLInputArchive>, XMLInputArchive> );
	STATIC_REQUIRE( is_same_v<input_archive_of_archive_t<XMLOutputArchive>, XMLInputArchive> );
}

TEST_CASE( "output_archive_of_archive_t" ) {
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<BinaryInputArchive>, BinaryOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<BinaryOutputArchive>, BinaryOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<JSONInputArchive>, JSONOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<JSONOutputArchive>, JSONOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<PortableBinaryInputArchive>, PortableBinaryOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<PortableBinaryOutputArchive>, PortableBinaryOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<XMLInputArchive>, XMLOutputArchive> );
	STATIC_REQUIRE( is_same_v<output_archive_of_archive_t<XMLOutputArchive>, XMLOutputArchive> );
}
