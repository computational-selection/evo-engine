#include "cereal_dirn.hpp"

#include <stdexcept>

#include "csel/util/exception/csel_throw_exception.hpp"

using ::std::logic_error;
using ::std::string;

/// \brief Generate a string describing the specified cereal_dirn
///
/// \param prm_cereal_dirn The cereal_dirn to describe
string csel::util::to_string( const cereal_dirn &prm_cereal_dirn ) {
	// clang-format off
	switch ( prm_cereal_dirn ) {
		case( cereal_dirn::LOADING ) : { return "LOADING" ; }
		case( cereal_dirn::SAVING  ) : { return "SAVING"  ; }
	}
	// clang-format on
	CSEL_THROW( logic_error( "Unhandled cereal_dirn" ) );
	return "";
}
