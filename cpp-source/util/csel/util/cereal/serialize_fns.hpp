#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_SERIALIZE_FNS_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_SERIALIZE_FNS_HPP

#include <sstream>

#include <cereal/cereal.hpp>

#include "csel/util/cereal/traits.hpp"

// Not using a string_view because cereal doesn't recognise string_view (and it's bad
// practice to get a char * from a string_view and pass it to something expecting a
// null-terminated string, even if it's technically OK here, see https://stackoverflow.com/a/57257485)
constexpr const char *const default_serialized_value_name = "serialized_value";

namespace csel::util::cereal {

	/// Return the result of deserialise one value of the specified type from the specified archive into a
	/// default-constructed instance
	///
	/// Requires T to be default constructible
	template <typename T, typename InputArchive>
	T deserialize_value_to( InputArchive &&prm_archive ) {
		static_assert( is_input_archive_v<InputArchive>, "InputArchive must be a cereal Input Archive type" );
		T the_value{};
		::std::forward<InputArchive>( prm_archive )( the_value );
		return the_value;
	}

	/// Serialise the specified value to the specified ostream
	///
	/// This requires the archive type (eg PortableBinaryOutputArchive) to be specified as the first template parameter, eg:
	///
	/// ~~~cpp
	/// serialise_to_ostream<PortableBinaryOutputArchive>( my_ostream, my_value );
	/// ~~~
	///
	/// \param prm_ostream The ostream to which the value should be serialized
	/// \param prm_value   The value to serialize to the ostream
	/// \param prm_name    The name to store this under in text archives
	template <typename ArchiveType, typename T>
	::std::ostream &serialise_to_ostream( ::std::ostream &prm_ostream,
	                                      const T &       prm_value,
	                                      const char *    prm_name = default_serialized_value_name ) {
		ArchiveType oarchive{ prm_ostream };
		oarchive( ::cereal::make_nvp( prm_name, prm_value ) );
		return prm_ostream;
	}

	/// Serialise the specified value to a string
	///
	/// This requires the archive type (eg PortableBinaryOutputArchive) to be specified as the first template parameter, eg:
	///
	/// ~~~cpp
	/// serialise_to_string<PortableBinaryOutputArchive>( my_value );
	/// ~~~
	///
	/// \param prm_value The value to serialize to a string
	template <typename ArchiveType, typename T>
	::std::string serialise_to_string( const T &prm_value, const char *prm_name = default_serialized_value_name ) {
		::std::ostringstream out_stream;
		serialise_to_ostream<ArchiveType>( out_stream, prm_value, prm_name );
		return out_stream.str();
	}

	// Serialise a value from an archive
	//
	// The value type must be specified as template parameters, eg:
	//
	//     serialise_from<Brief>( my_archive );
	//
	/// \param prm_istream The archive from which to (de)serialize
	template <typename T, typename ArchiveType>
	T serialise_from_archive( ArchiveType &&prm_archive ) {
		T t{};
		return ::std::forward<ArchiveType>( prm_archive )( t );
		return t;
	}

	/// Serialise a value from an istream by starting with a copy of the specified initial value
	/// (which is useful for types that don't provide a default-ctor)
	///
	/// The archive type (eg PortableBinaryInputArchive) must be specified as template parameters, eg:
	///
	///     serialise_from_with_init<PortableBinaryInputArchive>( my_istream, my_init_value );
	///
	/// \param prm_istream    The istream from which to (de)serialize
	/// \param prm_init_value The initial value from which a copy should be taken (by moving if rvalue) and assigned to
	template <typename ArchiveType, typename T>
	T serialise_from_with_init( ::std::istream &prm_istream, T prm_init_value ) {
		ArchiveType iarchive{ prm_istream };
		iarchive( prm_init_value );
		return prm_init_value;
	}

	// Serialise a value from an istream
	//
	// The archive type (eg PortableBinaryInputArchive) and value type must be specified as template parameters, eg:
	//
	//     serialise_from<PortableBinaryInputArchive, Brief>( my_istream );
	//
	/// \param prm_istream The istream from which to (de)serialize
	template <typename ArchiveType, typename T>
	T serialise_from( ::std::istream &prm_istream ) {
		return serialise_from_with_init<ArchiveType>( prm_istream, T{} );
	}

	/// Serialise a value from a string by starting with a copy of the specified initial value
	/// (which is useful for types that don't provide a default-ctor)
	///
	/// The archive type (eg PortableBinaryInputArchive) must be specified as template parameters, eg:
	///
	///     serialise_from_with_init<PortableBinaryInputArchive>( my_string, my_init_value );
	///
	/// \param prm_string     The string from which to (de)serialize
	/// \param prm_init_value The initial value from which a copy should be taken (by moving if rvalue) and assigned to
	template <typename ArchiveType, typename T>
	T serialise_from_with_init( const ::std::string &prm_string, T prm_init_value ) {
		::std::istringstream in_stream{ prm_string };
		return serialise_from_with_init<ArchiveType>( in_stream, ::std::move( prm_init_value ) );
	}

	/// Serialise a value from a string
	///
	/// The archive type (eg PortableBinaryInputArchive) and value type must be specified as template parameters, eg:
	///
	///     serialise_from<PortableBinaryInputArchive, Brief>( my_string );
	///
	/// \param prm_string The string from which to (de)serialize
	template <typename ArchiveType, typename T>
	T serialise_from( const ::std::string &prm_string ) {
		return serialise_from_with_init<ArchiveType>( prm_string, T{} );
	}

	/// Serialise a value from a string
	///
	///
	template <typename InputArchive, typename OutputArchive, typename T>
	T serialise_round_trip( const T &prm_value ) {
		static_assert( is_input_archive_v<InputArchive>, "InputArchive must be a cereal Input Archive type" );
		static_assert( is_output_archive_v<OutputArchive>, "OutputArchive must be a cereal Output Archive type" );
		return serialise_from<InputArchive, T>( serialise_to_string<OutputArchive>( prm_value ) );
	}

} // namespace csel::util::cereal

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_SERIALIZE_FNS_HPP
