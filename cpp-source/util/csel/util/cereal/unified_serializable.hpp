#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_UNIFIED_SERIALIZABLE_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_UNIFIED_SERIALIZABLE_HPP

#include <cstdint>
#include <utility>

#include "csel/util/cereal/cereal_dirn.hpp"

namespace csel::util {

	/// Boost.Operators-like CRTP base class for adding cereal load/save methods to a class that
	/// forward on to a unified_serialize() method that the class must provide
	///
	/// The call to unified_serialize() passes cereal_dirn as the first template argument
	///
	/// TODO: Create and use a concept for a type providing a suitable unified_serialize
	///       (and perhaps call that unified_serializable and rename this)
	template <typename T>
	struct unified_serializable {

		/// A load() method which forwards on to the unified_serialize() method of T
		///
		/// \param prm_archive The archive from/to which the data should be serialized
		/// \param prm_version The version of the serialization format
		template <typename Archive>
		void load( Archive &&prm_archive, const ::std::uint32_t &prm_version ) {
			T::template unified_serialize<cereal_dirn::LOADING>(
			  static_cast<T &>( *this ), ::std::forward<Archive>( prm_archive ), prm_version );
		}

		/// A save() method which forwards on to the unified_serialize() method of T
		///
		/// \param prm_archive The archive from/to which the data should be serialized
		/// \param prm_version The version of the serialization format
		template <typename Archive>
		void save( Archive &&prm_archive, const ::std::uint32_t &prm_version ) const {
			T::template unified_serialize<cereal_dirn::SAVING>(
			  static_cast<const T &>( *this ), ::std::forward<Archive>( prm_archive ), prm_version );
		}
	};

} // namespace csel::util

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CEREAL_UNIFIED_SERIALIZABLE_HPP
