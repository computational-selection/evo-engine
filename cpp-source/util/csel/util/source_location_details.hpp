#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SOURCE_LOCATION_DETAILS_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SOURCE_LOCATION_DETAILS_HPP

#include <string>

#include <boost/current_function.hpp>

// clang-format off
#define CSEL_SOURCE_LOCATION_DETAILS static_cast<const char *>( BOOST_CURRENT_FUNCTION ), static_cast<const char *>( __FILE__ ), __LINE__  // NOLINT(cppcoreguidelines-macro-usage)

#define CSEL_SOURCE_LOCATION_STRING ( "function `" + ::std::string{ static_cast<const char *>( BOOST_CURRENT_FUNCTION ) } + "` at " + ::std::string{ __FILE__ } + ":"  + ::std::to_string( __LINE__ ) )  // NOLINT(cppcoreguidelines-macro-usage)
// clang-format on

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_SOURCE_LOCATION_DETAILS_HPP
