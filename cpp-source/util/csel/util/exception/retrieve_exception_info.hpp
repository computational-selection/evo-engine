#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETRIEVE_EXCEPTION_INFO_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETRIEVE_EXCEPTION_INFO_HPP

#include <optional>
#include <string>
#include <type_traits>

#include <boost/exception/get_error_info.hpp>

#include "csel/util/exception/error_info.hpp"
#include "csel/util/stacktrace/stacktrace_to_cleaned_string.hpp"
#include "csel/util/type_traits.hpp"

namespace csel::except {

	namespace detail {

		/// \brief Return the what() from the specified value if it's of a type derived from std::exception, or none otherwise
		///
		/// More specifically if std::exception is a base of the argument's static type, this calls what() directly
		/// otherwise, it attempts to dynamic_cast to a std::exception and then call what() on the result if successful
		///
		/// \param prm_value The value to query
		template <typename Ex>
		inline ::std::optional<::std::string> get_what_of_std_exception( const Ex &prm_value ) {
			if constexpr ( ::std::is_base_of_v<::std::exception, remove_cvref_t<Ex>> ) {
				return ::std::string{ prm_value.what() };
			} else {
				const auto std_except_ptr = dynamic_cast<const ::std::exception *>( &prm_value );
				return ( std_except_ptr != nullptr ) ? ::std::make_optional( ::std::string{ std_except_ptr->what() } )
				                                     : ::std::nullopt;
			}
		}

	} // namespace detail

	/// \brief Generate a string describing the specified boost::exception, retrieving info added by CSEL_THROW()
	///
	/// \param prm_exception The boost::exception, hopefully thrown via CSEL_THROW
	template <typename Ex>
	inline std::string retrieve_exception_info( const Ex &prm_exception ) {
		static_assert(
		  ::std::is_base_of_v<::boost::exception, remove_cvref_t<Ex>>, "csel can only retrieve_exception_info() when passed with a static type derived from boost::exception (or boost::exception itself)" );

		const auto &file_value_ptr     = ::boost::get_error_info<::boost::throw_file>( prm_exception );
		const auto &line_value_ptr     = ::boost::get_error_info<::boost::throw_line>( prm_exception );
		const auto &function_value_ptr = ::boost::get_error_info<::boost::throw_function>( prm_exception );
		const auto &stacktrace_ptr     = ::boost::get_error_info<detail::stacktrace_error_info>( prm_exception );
		const auto &what_msg           = detail::get_what_of_std_exception( prm_exception );
		const auto &dynamic_type_name  = ::boost::core::demangle( typeid( prm_exception ).name() );

		return dynamic_type_name + " that had been thrown"
		       + ( ( function_value_ptr != nullptr ) ? ( " in '" + ::std::string{ *function_value_ptr } + "'" ) : "" )
		       + ( ( file_value_ptr != nullptr && line_value_ptr != nullptr )
		             ? " at " + ::std::string{ *file_value_ptr } + ":" + ::std::to_string( *line_value_ptr )
		             : "" )
		       + ( what_msg ? ( " with message '" + *what_msg + "'" ) : "" )
		       + ( ( stacktrace_ptr != nullptr ) ? "\nStacktrace:\n" + to_string_stripped_by_prefixes( *stacktrace_ptr ) : "" );
	}

} // namespace csel::except

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETRIEVE_EXCEPTION_INFO_HPP
