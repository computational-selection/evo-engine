#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETHROW_AND_HANDLE_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETHROW_AND_HANDLE_HPP

#include <exception>
#include <string>

#include <spdlog/spdlog.h>

#include <boost/exception/exception.hpp>

#include "csel/util/exception/error_info.hpp"
#include "csel/util/exception/retrieve_exception_info.hpp"

namespace csel::except {

	/// The action to take when handling a rethrown exception
	enum class rethrow_action : bool {
		LOG_INFO_AND_EXIT, ///< Log info about the exception and call exit( EXIT_FAILURE )
		RETURN_INFO        ///< Return info about the exception
	};

	namespace detail {

		/// Rethrow the current exception and return a string diagnosing it, beginning with the specified context string
		///
		/// if there is no current exception, this calls exit( EXIT_FAILURE )
		///
		/// \param prm_diagnostic_context_prefix A string describing the context of the original catch (eg "In function `int main(int, char **)` at main.cpp:25")
		inline ::std::string rethrow_and_return_diagnostic_info( const ::std::string &prm_diagnostic_context_prefix ) {
			if ( ::std::current_exception() == nullptr ) {
				spdlog::error( "{}, called rethrow_and_handle() without an active exception. Exiting.",
				               prm_diagnostic_context_prefix );
				exit( EXIT_FAILURE );
			}
			try {
				throw;
			} catch ( const ::boost::exception &ex ) {
				return prm_diagnostic_context_prefix + ", caught a boost::exception :" + retrieve_exception_info( ex );
			} catch ( const ::std::exception &ex ) {
				return prm_diagnostic_context_prefix + ", caught a ::std::exception : \"" + ex.what() + "\"";
			} catch ( ... ) {
				return prm_diagnostic_context_prefix + ", caught an unrecognised exception";
			}
		}

	} // namespace detail

	/// Rethrow and catch the current exception to retrieve information about it and then perform the specified action
	///
	/// Use the CSEL_RETHROW_AND_HANDLE() macro to provide the source location details
	///
	/// \param prm_current_function The name of the function from which this was called
	/// \param prm_filename         The name of the source file from which this was called
	/// \param prm_line_num         The number of the source line from which this was called
	/// \param prm_rethrow_action   What action to take once the exception has been
	inline ::std::string rethrow_and_handle( const char *          prm_current_function,
	                                         const char *          prm_filename,
	                                         const int &           prm_line_num,
	                                         const rethrow_action &prm_rethrow_action = rethrow_action::LOG_INFO_AND_EXIT ) {
		const ::std::string diagnostic_string = detail::rethrow_and_return_diagnostic_info(
		  "In function `" + ::std::string{ prm_current_function } + "` at " + ::std::string{ prm_filename } + ":"
		  + ::std::to_string( prm_line_num ) );

		switch ( prm_rethrow_action ) {
			case ( rethrow_action::LOG_INFO_AND_EXIT ): {
				::spdlog::error( diagnostic_string );
				exit( EXIT_FAILURE );
			}
			case ( rethrow_action::RETURN_INFO ): {
				return diagnostic_string;
			}
		}
	}

} // namespace csel

// clang-format off
#define CSEL_RETHROW_AND_HANDLE(prm_rethrow_action) ::csel::except::rethrow_and_handle( CSEL_SOURCE_LOCATION_DETAILS, prm_rethrow_action ) // NOLINT(cppcoreguidelines-macro-usage)
// clang-format on 

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_RETHROW_AND_HANDLE_HPP
