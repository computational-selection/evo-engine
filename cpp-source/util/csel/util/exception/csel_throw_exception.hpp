#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_CSEL_THROW_EXCEPTION_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_CSEL_THROW_EXCEPTION_HPP

#include <boost/current_function.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/stacktrace.hpp>

#include "csel/util/exception/error_info.hpp"
#include "csel/util/source_location_details.hpp"

namespace csel {

	/// Throw the specified exception with Boost Exception wrapping and with the source location details and stacktrace
	///
	/// Use CSEL_THROW() to provide the source location details
	///
	/// \param prm_exception        The exception to throw
	/// \param prm_current_function The name of the function of the throw site
	/// \param prm_filename         The name of the source file of the throw site
	/// \param prm_line_num         The number of the source line of the throw site
	/// \param prm_stack_trace      The stack trace of the the throw (just let this use the default)
	template <class Excptn>
	void throw_exception( const Excptn &                         prm_exception,
	                      const char *const                      prm_current_function,
	                      const char *const                      prm_filename,
	                      const int &                            prm_line_num,
	                      const ::boost::stacktrace::stacktrace &prm_stack_trace = ::boost::stacktrace::stacktrace() ) {
		// clang-format off
		throw ::boost::enable_error_info( prm_exception )
			<< ::boost::throw_function              ( prm_current_function )
			<< ::boost::throw_file                  ( prm_filename         )
			<< ::boost::throw_line                  ( prm_line_num         )
			<< except::detail::stacktrace_error_info( prm_stack_trace      );
		// clang-format on
	}

} // namespace csel

// clang-format off
#define CSEL_THROW( x ) ::csel::throw_exception( x, CSEL_SOURCE_LOCATION_DETAILS ) // NOLINT(cppcoreguidelines-macro-usage)
// clang-format on 

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_CSEL_THROW_EXCEPTION_HPP
