#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_ERROR_INFO_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_ERROR_INFO_HPP

#include <filesystem>

#include <boost/exception/error_info.hpp>
#include <boost/stacktrace/stacktrace_fwd.hpp>

namespace csel::except {

	/// \brief A tag under which the input file can be stored in a Boost exception
	struct input_file_tag final {
		input_file_tag()                             = delete;
		~input_file_tag()                            = delete;
		input_file_tag( const input_file_tag & )     = delete;
		input_file_tag( input_file_tag && ) noexcept = delete;
		input_file_tag &operator=( const input_file_tag & ) = delete;
		input_file_tag &operator=( input_file_tag && ) noexcept = delete;
	};

	/// \brief An error_info for storing an input file in a Boost exception
	using input_file_error_info = ::boost::error_info<input_file_tag, ::std::filesystem::path>;

	namespace detail {

		/// \brief A tag under which the stacktrace can be stored in a Boost exception
		struct stacktrace_tag final {
			stacktrace_tag()                             = delete;
			~stacktrace_tag()                            = delete;
			stacktrace_tag( const stacktrace_tag & )     = delete;
			stacktrace_tag( stacktrace_tag && ) noexcept = delete;
			stacktrace_tag &operator=( const stacktrace_tag & ) = delete;
			stacktrace_tag &operator=( stacktrace_tag && ) noexcept = delete;
		};

		/// \brief An error_info for storing a stacktrace in a Boost exception
		using stacktrace_error_info = ::boost::error_info<stacktrace_tag, ::boost::stacktrace::stacktrace>;

	} // namespace detail

} // namespace csel::except

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_EXCEPTION_ERROR_INFO_HPP
