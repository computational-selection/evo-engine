#ifndef _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CPP20_IS_DETECTED_HPP
#define _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CPP20_IS_DETECTED_HPP

// This implementation copied/pasted from cppreference.com (from 20170409 snapshot)

#include <type_traits>

namespace csel::util {

	namespace detail {

		template <typename Default,
		          typename AlwaysVoid,
		          template <typename...> typename Op,
		          typename... Args>
		struct detector {
			using value_t = std::false_type;
			using type    = Default;
		};

		template <typename Default,
		          template <typename...> typename Op,
		          typename... Args>
		struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
			using value_t = std::true_type;
			using type    = Op<Args...>;
		};

	} // namespace detail

	struct nonesuch final {
		nonesuch() = delete;
		~nonesuch() noexcept = delete;
		nonesuch(const nonesuch &) = delete;
		nonesuch(nonesuch &&) noexcept = delete;
		nonesuch & operator=(const nonesuch &) = delete;
		nonesuch & operator=(nonesuch &&) noexcept = delete;
	};

	template <template <typename...> typename Op, typename... Args>
	using is_detected = typename detail::detector<nonesuch, void, Op, Args...>::value_t;

	template <template <typename...> typename Op, typename... Args>
	using detected_t = typename detail::detector<nonesuch, void, Op, Args...>::type;

	template <typename Default, template <typename...> typename Op, typename... Args>
	using detected_or = detail::detector<Default, void, Op, Args...>;

	template <template <typename...> typename Op, typename... Args >
	inline constexpr bool is_detected_v = is_detected<Op, Args...>::value;

	template <typename Default, template <typename...> typename Op, typename... Args >
	using detected_or_t = typename detected_or<Default, Op, Args...>::type;

	template <typename Expected, template <typename...> typename Op, typename... Args>
	using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;

	template <typename Expected, template <typename...> typename Op, typename... Args>
	inline constexpr bool is_detected_exact_v = is_detected_exact<Expected, Op, Args...>::value;

	template <typename To, template <typename...> typename Op, typename... Args>
	using is_detected_convertible = std::is_convertible<detected_t<Op, Args...>, To>;

	template <typename To, template <typename...> typename Op, typename... Args>
	inline constexpr bool is_detected_convertible_v = is_detected_convertible<To, Op, Args...>::value;

} // namespace csel::util

#endif // _EVO_ENGINE_CPP_SOURCE_UTIL_CSEL_UTIL_CPP20_IS_DETECTED_HPP
