##### DON'T EDIT THIS FILE - IT'S AUTO-GENERATED #####

set(
	NORMSOURCES_ENGINE_CSEL_ENGINE
		engine/csel/engine/action.cpp
		engine/csel/engine/cargo.cpp
		engine/csel/engine/fitness_point.cpp
		engine/csel/engine/individual.cpp
		engine/csel/engine/input.cpp
		engine/csel/engine/node.cpp
		engine/csel/engine/target.cpp
)

set(
	NORMSOURCES_ENGINE_CSEL
		${NORMSOURCES_ENGINE_CSEL_ENGINE}
)

set(
	NORMSOURCES_ENGINE
		${NORMSOURCES_ENGINE_CSEL}
)

set(
	NORMSOURCES_EXE_ENGINE
		exe_engine/engine_main.cpp
)

set(
	NORMSOURCES_PROBLEM_CSEL_PROBLEM_PICTURE
		problem/csel/problem/picture/bitmap.cpp
		problem/csel/problem/picture/color_value.cpp
		problem/csel/problem/picture/render.cpp
)

set(
	NORMSOURCES_PROBLEM_CSEL_PROBLEM
		${NORMSOURCES_PROBLEM_CSEL_PROBLEM_PICTURE}
)

set(
	NORMSOURCES_PROBLEM_CSEL
		${NORMSOURCES_PROBLEM_CSEL_PROBLEM}
)

set(
	NORMSOURCES_PROBLEM
		${NORMSOURCES_PROBLEM_CSEL}
)

set(
	NORMSOURCES_TEST_MAIN
		test_main/test_main.cpp
)

set(
	NORMSOURCES_UTIL_CSEL_UTIL_CEREAL
		util/csel/util/cereal/cereal_dirn.cpp
)

set(
	NORMSOURCES_UTIL_CSEL_UTIL
		${NORMSOURCES_UTIL_CSEL_UTIL_CEREAL}
)

set(
	NORMSOURCES_UTIL_CSEL
		${NORMSOURCES_UTIL_CSEL_UTIL}
)

set(
	NORMSOURCES_UTIL
		${NORMSOURCES_UTIL_CSEL}
)

set(
	NORMSOURCES
		${NORMSOURCES_ENGINE}
		${NORMSOURCES_EXE_ENGINE}
		${NORMSOURCES_PROBLEM}
		${NORMSOURCES_TEST_MAIN}
		${NORMSOURCES_UTIL}
)

set(
	TESTSOURCES_ENGINE_CSEL_ENGINE
		engine/csel/engine/action_test.cpp
		engine/csel/engine/cargo_test.cpp
		engine/csel/engine/fitness_point_test.cpp
		engine/csel/engine/input_test.cpp
		engine/csel/engine/target_test.cpp
)

set(
	TESTSOURCES_ENGINE_CSEL
		${TESTSOURCES_ENGINE_CSEL_ENGINE}
)

set(
	TESTSOURCES_ENGINE
		${TESTSOURCES_ENGINE_CSEL}
)

set(
	TESTSOURCES_PROBLEM_CSEL_PROBLEM_PICTURE
		problem/csel/problem/picture/bitmap_test.cpp
		problem/csel/problem/picture/dimension_range_test.cpp
)

set(
	TESTSOURCES_PROBLEM_CSEL_PROBLEM
		${TESTSOURCES_PROBLEM_CSEL_PROBLEM_PICTURE}
)

set(
	TESTSOURCES_PROBLEM_CSEL
		${TESTSOURCES_PROBLEM_CSEL_PROBLEM}
)

set(
	TESTSOURCES_PROBLEM
		${TESTSOURCES_PROBLEM_CSEL}
)

set(
	TESTSOURCES_UTIL_CSEL_UTIL_CEREAL
		util/csel/util/cereal/archive_pairs_test.cpp
		util/csel/util/cereal/traits_test.cpp
)

set(
	TESTSOURCES_UTIL_CSEL_UTIL
		${TESTSOURCES_UTIL_CSEL_UTIL_CEREAL}
)

set(
	TESTSOURCES_UTIL_CSEL
		${TESTSOURCES_UTIL_CSEL_UTIL}
)

set(
	TESTSOURCES_UTIL
		${TESTSOURCES_UTIL_CSEL}
)

set(
	TESTSOURCES
		${TESTSOURCES_ENGINE}
		${TESTSOURCES_PROBLEM}
		${TESTSOURCES_UTIL}
)

##### DON'T EDIT THIS FILE - IT'S AUTO-GENERATED #####
