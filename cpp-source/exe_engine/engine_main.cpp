#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <unistd.h>

// #include <boost/stacktrace.hpp>

#include <cereal/archives/json.hpp>

#include <lyra/lyra.hpp>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/indices.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/transform.hpp>

#include <fmt/core.h>

#include <spdlog/spdlog.h>

#include "csel/engine/fitness_point.hpp"
#include "csel/engine/individual.hpp"
#include "csel/engine/node.hpp"
#include "csel/problem/picture/bitmap.hpp"
#include "csel/problem/picture/color_value.hpp"
#include "csel/util/exception/rethrow_and_handle.hpp"
#include "csel/util/json/json.hpp"
#include "csel/util/source_location_details.hpp"
#include "csel/util/stacktrace/stacktrace_to_cleaned_string.hpp"
#include "csel/util/str_join.hpp"

using namespace ::csel;
using namespace ::csel::except;
using namespace ::csel::prob::pict;
using namespace ::csel::util;

using ::boost::stacktrace::stacktrace;
using ::cereal::JSONOutputArchive;
using ::csel::util::cereal::serialise_to_string;
using ::lyra::args;
using ::lyra::help;
using ::lyra::opt;
using ::nlohmann::json;
using ::ranges::to_vector;
using ::std::array;
using ::std::cbegin;
using ::std::cend;
using ::std::cerr;
using ::std::clamp;
using ::std::mt19937;
using ::std::ofstream;
using ::std::random_device;
using ::std::string;
using ::std::swap;
using ::std::to_string;
using ::std::tuple;
using ::std::vector;
using ::std::chrono::duration;
using ::std::chrono::duration_cast;
using ::std::chrono::steady_clock;
using ::std::filesystem::path;

namespace views = ::ranges::views;

// using namespace clara;

// 0.22s 200000 * 20 * 35

// 160m

// const size_t1000 indivs

// constexpr size_t DEFAULT_NUM_GENS                   =  10000;
// constexpr size_t DEFAULT_NUM_GENS                   =  10000000;
// constexpr size_t DEFAULT_NUM_GENS                   = 990000000;
// constexpr size_t DEFAULT_NUM_GENS                   = 15000;
// constexpr size_t DEFAULT_NUM_GENS                   = 300000000;
constexpr size_t DEFAULT_NUM_GENS  = 150000;
constexpr size_t DEFAULT_NUM_LOOPS = 1;
// constexpr size_t DEFAULT_NUM_LOOPS                  =      6;
constexpr size_t DEFAULT_NUM_INSTRUCTIONS = 40;
// constexpr size_t DEFAULT_NUM_INSTRUCTIONS           =    400;

// using datum_type = tuple<float, float, float>;
using datum_type = tuple<float, float, float, float, float>;

vector<datum_type> get_data();

int main( int argc, char *argv[] ) {
	mt19937 rng( random_device{}() );

	size_t num_gens         = DEFAULT_NUM_GENS;
	size_t num_loops        = DEFAULT_NUM_LOOPS;
	size_t num_instructions = DEFAULT_NUM_INSTRUCTIONS;
	bool   show_help        = false;
	// clang-format off
	const auto cli =
		help( show_help )
			| opt( num_gens,         "num-gens"   )[ "-g" ][ "--num-gens"   ]( "Evaluate for <num> generations"           )
			| opt( num_loops,        "num-loops"  )[ "-l" ][ "--num-loops"  ]( "Evaluate the individuals for <num> loops" )
			| opt( num_instructions, "num-instrs" )[ "-i" ][ "--num-instrs" ]( "Use <num> instructions"                   );
	// clang-format on
	const auto result = cli.parse( args( argc, argv ) );
	if ( !result ) {
		cerr << "Error in command line: " << result.errorMessage() << "\n";
		return EXIT_FAILURE;
	}
	if ( show_help ) {
		cerr << cli << "\n";
		return 0;
	}

	try {
		cerr << "num_instructions : " << num_instructions << "\n";

		const path fitness_history_file{ "fitnesses." + to_string( num_instructions ) + "_instrs."
			                             + to_string( num_loops ) + "_loops." + to_string( getpid() ) + ".json" };

		::std::cerr << ::std::filesystem::exists( fitness_history_file ) << "\n";

		const vector<datum_type> data = get_data();

		vector<fitness_point> fitnesses;

		individual the_indiv = generate_random_individual_with_n_nodes( rng, static_cast<uint16_t>( num_instructions ) );
		vector<node> eval_nodes{ cbegin( the_indiv ), cend( the_indiv ) };
		float        fitness_score = fitness( eval_nodes, the_indiv, data, num_loops );
		fitnesses.emplace_back( 0UL, fitness_score );
		individual mutation_indiv{ the_indiv };

		const auto start_time = steady_clock::now();

		for ( const size_t &generation_ctr : views::iota( 1UL, num_gens ) ) {
			mutation_indiv = the_indiv;
			mutation_indiv.mutate( rng );
			float      mutation_fitness_score = fitness( eval_nodes, mutation_indiv, data, num_loops );
			const bool strict_improvement     = ( mutation_fitness_score > fitness_score );
			if ( mutation_fitness_score >= fitness_score ) {
				swap( the_indiv, mutation_indiv );
				fitness_score = mutation_fitness_score;
			}
			if ( strict_improvement || generation_ctr + 1 == num_gens ) {
				fitnesses.emplace_back( generation_ctr + 1, fitness_score );
				// ::std::cerr << to_string( the_indiv ) << "\n";
				spdlog::warn( "{:>12} : {:8.4f}", generation_ctr, fitness_score );
			}

			if ( generation_ctr % 1000000 == 0 ) {
				ofstream fitness_history_ostream{ fitness_history_file.string().c_str() };
				fitness_history_ostream << to_json_string( fitnesses );
				fitness_history_ostream.close();
			}
		}

		const auto   the_duration     = steady_clock::now() - start_time;
		const auto   total_node_evals = static_cast<double>( num_gens * num_loops * data.size() * num_instructions );
		const double total_seconds    = duration_cast<duration<double>>( the_duration ).count();

		fmt::print( "Number of generations  :{:>12}\n", num_gens );
		fmt::print( "Number of loops        :{:>12}\n", num_loops );
		fmt::print( "Number of instructions :{:>12}\n", num_instructions );
		fmt::print( "Number of testcases    :{:>12}\n", data.size() );
		fmt::print( "Total node evaluations :{:15.2f}\n", total_node_evals );
		fmt::print( "Duration               :{:15.2f}s\n", total_seconds );
		fmt::print( "Evaluation rate        :{:15.2f}mops\n", total_node_evals / total_seconds / 1000000.0 );

		const string indiv_json_str = serialise_to_string<JSONOutputArchive>( the_indiv, "best_individual" );
		::std::cerr << "The best individual is " << indiv_json_str << "\n";

		ofstream fitness_history_ostream{ fitness_history_file.string().c_str() };
		fitness_history_ostream << to_json_string( fitnesses );
		fitness_history_ostream.close();

		::std::cerr << "The indiv is:\n" << the_indiv << "\n";

		// constexpr size_t num_pixels_in_pic = 20;
		constexpr size_t num_pixels_in_pic = 800;

		// clang-format off

		// individual test_indiv{ {
		// 	node{
		// 		test_case_sub_0_input(),
		// 		cargo{ -9.0 },
		// 		comparator{ -100.0 },
		// 		make_action( action_cat::ADD_MY_VALUE_TO, 0 ),
		// 		make_target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION, 0 )
		// 	},
		// 	node{
		// 		test_case_sub_1_input(),
		// 		cargo{ -9.0 },
		// 		comparator{ 3.0 },
		// 		make_action( action_cat::SUBTR_MY_VALUE_FROM, 1 ),
		// 		make_target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION, 1 )
		// 	},
		// 	node{
		// 		test_case_sub_0_input(),
		// 		cargo{ -9.0 },
		// 		comparator{ -5.0 },
		// 		make_action( action_cat::SUBTR_MY_VALUE_FROM, 2 ),
		// 		make_target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION, 2 )
		// 	},
		// 	node{
		// 		test_case_sub_0_input(),
		// 		cargo{ limits<float>::infinity() },
		// 		comparator{ -5.0 },
		// 		make_action( action_cat::SUBTR_MY_VALUE_FROM, 2 ),
		// 		make_target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION, 2 )
		// 	}
		// } };
		// const vector<array<float, 3>> test_indiv_pic_result = evaluation_output<3>(
		// 	test_indiv,
		// 	views::cartesian_product( views::indices( num_pixels_in_pic ), views::indices( num_pixels_in_pic ) )
		// 		| views::transform( [] (const auto &y_idx_and_x_idx) {
		// 			const auto &[ y_idx, x_idx ] = y_idx_and_x_idx;
		// 			return tuple{
		// 				20.0F * static_cast<float>( x_idx ) / static_cast<float>( num_pixels_in_pic ) - 10.0F,
		// 				20.0F * static_cast<float>( y_idx ) / static_cast<float>( num_pixels_in_pic ) - 10.0F
		// 			};
		// 		} ),
		// 	num_loops
		// );

		// write_to_png(
		// 	bitmap_from_row_major_pixels(
		// 		test_indiv_pic_result
		// 			| views::transform( []( const array<float, 3> &x ) {
		// 				return color_value{
		// 					clamp( x[ 0 ].value / 20.0F + 0.5F, 0.0F, 1.0F ),
		// 					clamp( x[ 1 ].value / 20.0F + 0.5F, 0.0F, 1.0F ),
		// 					clamp( x[ 2 ].value / 20.0F + 0.5F, 0.0F, 1.0F )
		// 				};
		// 			} ),
		// 		num_pixels_in_pic,
		// 		num_pixels_in_pic
		// 	),
		// 	path{ "/tmp/test_indiv_pic_result.png" }
		// );

		// clang-format on

		const vector<array<float, 3>> row_major_pic_result = evaluation_output<3>(
		  the_indiv,
		  views::cartesian_product( views::indices( num_pixels_in_pic ), views::indices( num_pixels_in_pic ) )
		    | views::transform( []( const auto &y_idx_and_x_idx ) {
			      const auto &[ y_idx, x_idx ] = y_idx_and_x_idx;
			      return tuple{ 20.0F * static_cast<float>( x_idx ) / static_cast<float>( num_pixels_in_pic ) - 10.0F,
				                20.0F * static_cast<float>( y_idx ) / static_cast<float>( num_pixels_in_pic ) - 10.0F };
		      } ),
		  num_loops );

		const auto convert_to_color_fn = []( const array<float, 3> &x ) {
			return color_value{ clamp( x[ 0 ] / 20.0F + 0.5F, 0.0F, 1.0F ),
				                clamp( x[ 1 ] / 20.0F + 0.5F, 0.0F, 1.0F ),
				                clamp( x[ 2 ] / 20.0F + 0.5F, 0.0F, 1.0F ) };
		};

		const vector<color_value> pixels = row_major_pic_result | views::transform( convert_to_color_fn ) | to_vector;

		write_to_png( bitmap_from_row_major_pixels(
		                row_major_pic_result | views::transform( convert_to_color_fn ), num_pixels_in_pic, num_pixels_in_pic ),
		              path{ "/tmp/lovely.png" } );

		// clang-format off

		write_to_png(
			bitmap_from_row_major_pixels(
				data
					| views::transform( []( const auto &x ) {
						return color_value{
							clamp( ::std::get<2>( x ) / 20.0F + 0.5F, 0.0F, 1.0F ),
							clamp( ::std::get<3>( x ) / 20.0F + 0.5F, 0.0F, 1.0F ),
							clamp( ::std::get<4>( x ) / 20.0F + 0.5F, 0.0F, 1.0F )
						};
					} ),
				16,
				16
			),
			path{ "/tmp/target.png" }
		);

		// clang-format on

	} catch ( ... ) {
		CSEL_RETHROW_AND_HANDLE( rethrow_action::LOG_INFO_AND_EXIT );
	}

	// constexpr node eg_node{

	// };
	// mt19937 the_rng( random_device{}() );

	// individual core{ the_rng, 60000, 10 };
	// individual temp{ the_rng, 60000, 10 };
	// for (unsigned int x = 0; x<500; ++x) {
	// 	// fmt::print( "{:150}|{:5.3f}\n", to_string( core ), core.fitness_fraction() );
	// 	fmt::print( "{:>8} {:5.3f}\n", x, core.fitness_fraction() );
	// 	temp = core;
	// 	temp.mutate_with_gene_probability( the_rng, 0.00333 );
	// 	if ( temp.fitness_fraction() > core.fitness_fraction() ) {
	// 		core = ::std::move( temp );
	// 	}
	// }
	return EXIT_SUCCESS;
}

vector<datum_type> get_data() {
	return vector<datum_type> {

		// clang-format off
#if 0

		tuple{-10.00000F, -10.00000F,   2.26562F },
		tuple{-10.00000F,  -8.66667F,   2.89062F },
		tuple{-10.00000F,  -7.33333F,   2.57812F },
		tuple{-10.00000F,  -6.00000F,   1.79688F },
		tuple{-10.00000F,  -4.66667F,  -3.59375F },
		tuple{-10.00000F,  -3.33333F,  -4.06250F },
		tuple{-10.00000F,  -2.00000F,  -5.62500F },
		tuple{-10.00000F,  -0.66667F,  -3.12500F },
		tuple{-10.00000F,   0.66667F,   5.46875F },
		tuple{-10.00000F,   2.00000F,   6.56250F },
		tuple{-10.00000F,   3.33333F,   6.48438F },
		tuple{-10.00000F,   4.66667F,   6.40625F },
		tuple{-10.00000F,   6.00000F,   5.93750F },
		tuple{-10.00000F,   7.33333F,   5.15625F },
		tuple{-10.00000F,   8.66667F,   3.04688F },
		tuple{-10.00000F,  10.00000F,  -5.93750F },
		tuple{ -8.66667F, -10.00000F,   3.04688F },
		tuple{ -8.66667F,  -8.66667F,   3.28125F },
		tuple{ -8.66667F,  -7.33333F,   2.73438F },
		tuple{ -8.66667F,  -6.00000F,  -1.09375F },
		tuple{ -8.66667F,  -4.66667F,  -5.15625F },
		tuple{ -8.66667F,  -3.33333F,  -5.62500F },
		tuple{ -8.66667F,  -2.00000F,  -5.85938F },
		tuple{ -8.66667F,  -0.66667F,  -0.46875F },
		tuple{ -8.66667F,   0.66667F,   5.07812F },
		tuple{ -8.66667F,   2.00000F,   6.48438F },
		tuple{ -8.66667F,   3.33333F,   6.25000F },
		tuple{ -8.66667F,   4.66667F,   6.32812F },
		tuple{ -8.66667F,   6.00000F,   6.56250F },
		tuple{ -8.66667F,   7.33333F,   6.32812F },
		tuple{ -8.66667F,   8.66667F,   5.15625F },
		tuple{ -8.66667F,  10.00000F,  -1.87500F },
		tuple{ -7.33333F, -10.00000F,   2.34375F },
		tuple{ -7.33333F,  -8.66667F,   2.18750F },
		tuple{ -7.33333F,  -7.33333F,  -0.23438F },
		tuple{ -7.33333F,  -6.00000F,  -4.84375F },
		tuple{ -7.33333F,  -4.66667F,  -5.46875F },
		tuple{ -7.33333F,  -3.33333F,  -6.48438F },
		tuple{ -7.33333F,  -2.00000F,  -5.78125F },
		tuple{ -7.33333F,  -0.66667F,   1.64062F },
		tuple{ -7.33333F,   0.66667F,   4.92188F },
		tuple{ -7.33333F,   2.00000F,   5.46875F },
		tuple{ -7.33333F,   3.33333F,   5.23438F },
		tuple{ -7.33333F,   4.66667F,   4.76562F },
		tuple{ -7.33333F,   6.00000F,   2.81250F },
		tuple{ -7.33333F,   7.33333F,   0.85938F },
		tuple{ -7.33333F,   8.66667F,   1.09375F },
		tuple{ -7.33333F,  10.00000F,   2.10938F },
		tuple{ -6.00000F, -10.00000F,   2.26562F },
		tuple{ -6.00000F,  -8.66667F,   2.03125F },
		tuple{ -6.00000F,  -7.33333F,  -3.59375F },
		tuple{ -6.00000F,  -6.00000F,  -5.23438F },
		tuple{ -6.00000F,  -4.66667F,  -5.39062F },
		tuple{ -6.00000F,  -3.33333F,  -6.40625F },
		tuple{ -6.00000F,  -2.00000F,  -5.00000F },
		tuple{ -6.00000F,  -0.66667F,  -1.09375F },
		tuple{ -6.00000F,   0.66667F,  -4.45312F },
		tuple{ -6.00000F,   2.00000F,  -3.90625F },
		tuple{ -6.00000F,   3.33333F,   1.56250F },
		tuple{ -6.00000F,   4.66667F,   1.17188F },
		tuple{ -6.00000F,   6.00000F,  -4.06250F },
		tuple{ -6.00000F,   7.33333F,  -3.82812F },
		tuple{ -6.00000F,   8.66667F,  -0.46875F },
		tuple{ -6.00000F,  10.00000F,   1.79688F },
		tuple{ -4.66667F, -10.00000F,   2.89062F },
		tuple{ -4.66667F,  -8.66667F,   1.32812F },
		tuple{ -4.66667F,  -7.33333F,  -4.76562F },
		tuple{ -4.66667F,  -6.00000F,  -6.56250F },
		tuple{ -4.66667F,  -4.66667F,  -6.40625F },
		tuple{ -4.66667F,  -3.33333F,  -7.34375F },
		tuple{ -4.66667F,  -2.00000F,  -4.21875F },
		tuple{ -4.66667F,  -0.66667F,  -0.85938F },
		tuple{ -4.66667F,   0.66667F,  -1.32812F },
		tuple{ -4.66667F,   2.00000F,  -4.45312F },
		tuple{ -4.66667F,   3.33333F,   1.01562F },
		tuple{ -4.66667F,   4.66667F,   2.96875F },
		tuple{ -4.66667F,   6.00000F,  -4.37500F },
		tuple{ -4.66667F,   7.33333F,  -3.28125F },
		tuple{ -4.66667F,   8.66667F,  -1.17188F },
		tuple{ -4.66667F,  10.00000F,  -0.07812F },
		tuple{ -3.33333F, -10.00000F,   1.40625F },
		tuple{ -3.33333F,  -8.66667F,  -1.87500F },
		tuple{ -3.33333F,  -7.33333F,  -5.46875F },
		tuple{ -3.33333F,  -6.00000F,  -5.93750F },
		tuple{ -3.33333F,  -4.66667F,  -6.79688F },
		tuple{ -3.33333F,  -3.33333F,  -6.87500F },
		tuple{ -3.33333F,  -2.00000F,  -4.37500F },
		tuple{ -3.33333F,  -0.66667F,  -2.65625F },
		tuple{ -3.33333F,   0.66667F,  -4.84375F },
		tuple{ -3.33333F,   2.00000F,  -2.81250F },
		tuple{ -3.33333F,   3.33333F,   3.67188F },
		tuple{ -3.33333F,   4.66667F,   5.85938F },
		tuple{ -3.33333F,   6.00000F,  -0.70312F },
		tuple{ -3.33333F,   7.33333F,  -2.96875F },
		tuple{ -3.33333F,   8.66667F,  -2.26562F },
		tuple{ -3.33333F,  10.00000F,   3.75000F },
		tuple{ -2.00000F, -10.00000F,  -0.23438F },
		tuple{ -2.00000F,  -8.66667F,  -1.48438F },
		tuple{ -2.00000F,  -7.33333F,  -6.01562F },
		tuple{ -2.00000F,  -6.00000F,  -5.46875F },
		tuple{ -2.00000F,  -4.66667F,  -7.42188F },
		tuple{ -2.00000F,  -3.33333F,  -6.64062F },
		tuple{ -2.00000F,  -2.00000F,  -3.67188F },
		tuple{ -2.00000F,  -0.66667F,   2.42188F },
		tuple{ -2.00000F,   0.66667F,   1.56250F },
		tuple{ -2.00000F,   2.00000F,   4.21875F },
		tuple{ -2.00000F,   3.33333F,   6.25000F },
		tuple{ -2.00000F,   4.66667F,   7.03125F },
		tuple{ -2.00000F,   6.00000F,   5.31250F },
		tuple{ -2.00000F,   7.33333F,   4.45312F },
		tuple{ -2.00000F,   8.66667F,   5.62500F },
		tuple{ -2.00000F,  10.00000F,   6.32812F },
		tuple{ -0.66667F, -10.00000F,  -1.01562F },
		tuple{ -0.66667F,  -8.66667F,  -1.87500F },
		tuple{ -0.66667F,  -7.33333F,  -6.79688F },
		tuple{ -0.66667F,  -6.00000F,  -7.34375F },
		tuple{ -0.66667F,  -4.66667F,  -7.73438F },
		tuple{ -0.66667F,  -3.33333F,  -7.73438F },
		tuple{ -0.66667F,  -2.00000F,  -4.53125F },
		tuple{ -0.66667F,  -0.66667F,   5.31250F },
		tuple{ -0.66667F,   0.66667F,   5.78125F },
		tuple{ -0.66667F,   2.00000F,   2.57812F },
		tuple{ -0.66667F,   3.33333F,   6.56250F },
		tuple{ -0.66667F,   4.66667F,   6.71875F },
		tuple{ -0.66667F,   6.00000F,   3.82812F },
		tuple{ -0.66667F,   7.33333F,   1.64062F },
		tuple{ -0.66667F,   8.66667F,   4.68750F },
		tuple{ -0.66667F,  10.00000F,   4.92188F },
		tuple{  0.66667F, -10.00000F,   1.17188F },
		tuple{  0.66667F,  -8.66667F,  -3.28125F },
		tuple{  0.66667F,  -7.33333F,  -7.10938F },
		tuple{  0.66667F,  -6.00000F,  -7.57812F },
		tuple{  0.66667F,  -4.66667F,  -7.03125F },
		tuple{  0.66667F,  -3.33333F,  -7.65625F },
		tuple{  0.66667F,  -2.00000F,  -5.93750F },
		tuple{  0.66667F,  -0.66667F,   1.71875F },
		tuple{  0.66667F,   0.66667F,  -0.39062F },
		tuple{  0.66667F,   2.00000F,  -0.85938F },
		tuple{  0.66667F,   3.33333F,   0.78125F },
		tuple{  0.66667F,   4.66667F,   0.62500F },
		tuple{  0.66667F,   6.00000F,   1.17188F },
		tuple{  0.66667F,   7.33333F,  -0.70312F },
		tuple{  0.66667F,   8.66667F,  -1.17188F },
		tuple{  0.66667F,  10.00000F,   1.25000F },
		tuple{  2.00000F, -10.00000F,   2.18750F },
		tuple{  2.00000F,  -8.66667F,  -4.37500F },
		tuple{  2.00000F,  -7.33333F,  -6.87500F },
		tuple{  2.00000F,  -6.00000F,  -8.04688F },
		tuple{  2.00000F,  -4.66667F,  -6.87500F },
		tuple{  2.00000F,  -3.33333F,  -7.50000F },
		tuple{  2.00000F,  -2.00000F,  -7.26562F },
		tuple{  2.00000F,  -0.66667F,  -2.89062F },
		tuple{  2.00000F,   0.66667F,  -3.51562F },
		tuple{  2.00000F,   2.00000F,  -0.39062F },
		tuple{  2.00000F,   3.33333F,  -0.31250F },
		tuple{  2.00000F,   4.66667F,  -2.89062F },
		tuple{  2.00000F,   6.00000F,   0.62500F },
		tuple{  2.00000F,   7.33333F,  -0.39062F },
		tuple{  2.00000F,   8.66667F,  -3.43750F },
		tuple{  2.00000F,  10.00000F,  -0.62500F },
		tuple{  3.33333F, -10.00000F,  -1.95312F },
		tuple{  3.33333F,  -8.66667F,  -7.26562F },
		tuple{  3.33333F,  -7.33333F,  -7.81250F },
		tuple{  3.33333F,  -6.00000F,  -8.20312F },
		tuple{  3.33333F,  -4.66667F,  -6.87500F },
		tuple{  3.33333F,  -3.33333F,  -8.20312F },
		tuple{  3.33333F,  -2.00000F,  -8.35938F },
		tuple{  3.33333F,  -0.66667F,  -3.59375F },
		tuple{  3.33333F,   0.66667F,  -1.01562F },
		tuple{  3.33333F,   2.00000F,  -0.31250F },
		tuple{  3.33333F,   3.33333F,   0.46875F },
		tuple{  3.33333F,   4.66667F,  -0.62500F },
		tuple{  3.33333F,   6.00000F,   0.15625F },
		tuple{  3.33333F,   7.33333F,   0.62500F },
		tuple{  3.33333F,   8.66667F,   0.23438F },
		tuple{  3.33333F,  10.00000F,  -0.23438F },
		tuple{  4.66667F, -10.00000F,  -3.04688F },
		tuple{  4.66667F,  -8.66667F,  -6.95312F },
		tuple{  4.66667F,  -7.33333F,  -8.12500F },
		tuple{  4.66667F,  -6.00000F,  -8.28125F },
		tuple{  4.66667F,  -4.66667F,  -7.42188F },
		tuple{  4.66667F,  -3.33333F,  -8.67188F },
		tuple{  4.66667F,  -2.00000F,  -8.12500F },
		tuple{  4.66667F,  -0.66667F,  -6.95312F },
		tuple{  4.66667F,   0.66667F,  -1.87500F },
		tuple{  4.66667F,   2.00000F,   0.39062F },
		tuple{  4.66667F,   3.33333F,  -1.48438F },
		tuple{  4.66667F,   4.66667F,  -2.89062F },
		tuple{  4.66667F,   6.00000F,  -2.26562F },
		tuple{  4.66667F,   7.33333F,   0.85938F },
		tuple{  4.66667F,   8.66667F,   0.62500F },
		tuple{  4.66667F,  10.00000F,  -2.73438F },
		tuple{  6.00000F, -10.00000F,  -3.59375F },
		tuple{  6.00000F,  -8.66667F,  -4.92188F },
		tuple{  6.00000F,  -7.33333F,  -6.48438F },
		tuple{  6.00000F,  -6.00000F,  -7.50000F },
		tuple{  6.00000F,  -4.66667F,  -7.81250F },
		tuple{  6.00000F,  -3.33333F,  -8.51562F },
		tuple{  6.00000F,  -2.00000F,  -7.73438F },
		tuple{  6.00000F,  -0.66667F,  -9.14062F },
		tuple{  6.00000F,   0.66667F,  -5.31250F },
		tuple{  6.00000F,   2.00000F,  -0.15625F },
		tuple{  6.00000F,   3.33333F,   1.32812F },
		tuple{  6.00000F,   4.66667F,   0.85938F },
		tuple{  6.00000F,   6.00000F,   1.71875F },
		tuple{  6.00000F,   7.33333F,   2.10938F },
		tuple{  6.00000F,   8.66667F,  -1.17188F },
		tuple{  6.00000F,  10.00000F,  -7.26562F },
		tuple{  7.33333F, -10.00000F,  -3.67188F },
		tuple{  7.33333F,  -8.66667F,  -1.79688F },
		tuple{  7.33333F,  -7.33333F,  -2.73438F },
		tuple{  7.33333F,  -6.00000F,  -6.64062F },
		tuple{  7.33333F,  -4.66667F,  -8.35938F },
		tuple{  7.33333F,  -3.33333F,  -8.59375F },
		tuple{  7.33333F,  -2.00000F,  -8.75000F },
		tuple{  7.33333F,  -0.66667F,  -8.98438F },
		tuple{  7.33333F,   0.66667F,  -8.51562F },
		tuple{  7.33333F,   2.00000F,  -4.68750F },
		tuple{  7.33333F,   3.33333F,  -0.07812F },
		tuple{  7.33333F,   4.66667F,   1.17188F },
		tuple{  7.33333F,   6.00000F,   0.62500F },
		tuple{  7.33333F,   7.33333F,  -1.71875F },
		tuple{  7.33333F,   8.66667F,  -5.31250F },
		tuple{  7.33333F,  10.00000F,  -8.35938F },
		tuple{  8.66667F, -10.00000F,  -2.42188F },
		tuple{  8.66667F,  -8.66667F,   0.07812F },
		tuple{  8.66667F,  -7.33333F,   4.14062F },
		tuple{  8.66667F,  -6.00000F,  -6.17188F },
		tuple{  8.66667F,  -4.66667F,  -7.81250F },
		tuple{  8.66667F,  -3.33333F,  -8.82812F },
		tuple{  8.66667F,  -2.00000F,  -9.06250F },
		tuple{  8.66667F,  -0.66667F,  -8.82812F },
		tuple{  8.66667F,   0.66667F,  -8.20312F },
		tuple{  8.66667F,   2.00000F,  -6.25000F },
		tuple{  8.66667F,   3.33333F,  -5.23438F },
		tuple{  8.66667F,   4.66667F,  -4.60938F },
		tuple{  8.66667F,   6.00000F,  -4.60938F },
		tuple{  8.66667F,   7.33333F,  -4.45312F },
		tuple{  8.66667F,   8.66667F,  -5.70312F },
		tuple{  8.66667F,  10.00000F,  -8.28125F },
		tuple{ 10.00000F, -10.00000F,   1.95312F },
		tuple{ 10.00000F,  -8.66667F,   4.68750F },
		tuple{ 10.00000F,  -7.33333F,   4.68750F },
		tuple{ 10.00000F,  -6.00000F,   3.90625F },
		tuple{ 10.00000F,  -4.66667F,  -2.96875F },
		tuple{ 10.00000F,  -3.33333F,  -8.59375F },
		tuple{ 10.00000F,  -2.00000F,  -8.51562F },
		tuple{ 10.00000F,  -0.66667F,  -8.35938F },
		tuple{ 10.00000F,   0.66667F,  -8.28125F },
		tuple{ 10.00000F,   2.00000F,  -5.62500F },
		tuple{ 10.00000F,   3.33333F,  -4.45312F },
		tuple{ 10.00000F,   4.66667F,  -4.37500F },
		tuple{ 10.00000F,   6.00000F,  -3.98438F },
		tuple{ 10.00000F,   7.33333F,  -4.29688F },
		tuple{ 10.00000F,   8.66667F,  -7.03125F },
		tuple{ 10.00000F,  10.00000F,  -7.81250F },

#else

		tuple{-10.00000F, -10.00000F,  -9.60938F,  -9.45312F,  -8.43750F },
		tuple{-10.00000F,  -8.66667F,  -9.68750F,  -9.53125F,  -8.51562F },
		tuple{-10.00000F,  -7.33333F,  -9.45312F,  -9.53125F,  -8.28125F },
		tuple{-10.00000F,  -6.00000F,  -9.60938F,  -9.68750F,  -8.43750F },
		tuple{-10.00000F,  -4.66667F,  -9.53125F,  -9.60938F,  -8.51562F },
		tuple{-10.00000F,  -3.33333F,  -8.82812F,  -9.84375F,  -9.06250F },
		tuple{-10.00000F,  -2.00000F,  -7.42188F, -10.00000F, -10.00000F },
		tuple{-10.00000F,  -0.66667F,  -2.65625F,  -6.01562F,  -6.48438F },
		tuple{-10.00000F,   0.66667F,  -4.76562F,  -6.95312F,  -7.26562F },
		tuple{-10.00000F,   2.00000F,  -8.35938F,  -9.60938F,  -9.53125F },
		tuple{-10.00000F,   3.33333F,  -9.53125F,  -9.68750F,  -8.82812F },
		tuple{-10.00000F,   4.66667F,  -8.04688F, -10.00000F, -10.00000F },
		tuple{-10.00000F,   6.00000F,  -1.56250F,  -8.98438F, -10.00000F },
		tuple{-10.00000F,   7.33333F,   7.26562F,  -2.03125F,  -5.15625F },
		tuple{-10.00000F,   8.66667F,   6.64062F,  -1.48438F,  -5.62500F },
		tuple{-10.00000F,  10.00000F,  -0.39062F,  -7.89062F, -10.00000F },
		tuple{ -8.66667F, -10.00000F, -10.00000F,  -9.84375F,  -8.82812F },
		tuple{ -8.66667F,  -8.66667F, -10.00000F,  -9.14062F,  -9.45312F },
		tuple{ -8.66667F,  -7.33333F, -10.00000F,  -7.73438F, -10.00000F },
		tuple{ -8.66667F,  -6.00000F,  -5.31250F,  -2.42188F,  -5.78125F },
		tuple{ -8.66667F,  -4.66667F,  -8.98438F,  -7.34375F,  -8.67188F },
		tuple{ -8.66667F,  -3.33333F,  -8.82812F, -10.00000F, -10.00000F },
		tuple{ -8.66667F,  -2.00000F,  -2.57812F,  -8.04688F, -10.00000F },
		tuple{ -8.66667F,  -0.66667F,   8.04688F,   0.93750F,  -6.01562F },
		tuple{ -8.66667F,   0.66667F,   9.06250F,   2.50000F,  -7.26562F },
		tuple{ -8.66667F,   2.00000F,   5.15625F,   0.23438F,  -7.96875F },
		tuple{ -8.66667F,   3.33333F,  -6.17188F,  -8.51562F, -10.00000F },
		tuple{ -8.66667F,   4.66667F,  -8.35938F, -10.00000F, -10.00000F },
		tuple{ -8.66667F,   6.00000F,  -6.25000F, -10.00000F, -10.00000F },
		tuple{ -8.66667F,   7.33333F,   3.28125F,  -2.89062F,  -7.34375F },
		tuple{ -8.66667F,   8.66667F,   9.14062F,   0.39062F,  -7.57812F },
		tuple{ -8.66667F,  10.00000F,   9.45312F,  -0.70312F, -10.00000F },
		tuple{ -7.33333F, -10.00000F,   4.53125F,   1.71875F,  -4.06250F },
		tuple{ -7.33333F,  -8.66667F,  -7.65625F,  -7.89062F, -10.00000F },
		tuple{ -7.33333F,  -7.33333F,  -5.07812F,  -0.31250F, -10.00000F },
		tuple{ -7.33333F,  -6.00000F,  -1.87500F,   5.07812F,  -6.01562F },
		tuple{ -7.33333F,  -4.66667F,  -1.01562F,   4.92188F,  -4.76562F },
		tuple{ -7.33333F,  -3.33333F,  -7.18750F,  -3.90625F, -10.00000F },
		tuple{ -7.33333F,  -2.00000F,  -6.64062F,  -7.73438F, -10.00000F },
		tuple{ -7.33333F,  -0.66667F,   1.01562F,  -2.50000F,  -8.82812F },
		tuple{ -7.33333F,   0.66667F,   8.12500F,   4.21875F,  -8.20312F },
		tuple{ -7.33333F,   2.00000F,   9.92188F,   5.70312F,  -7.96875F },
		tuple{ -7.33333F,   3.33333F,   9.92188F,   3.43750F,  -7.03125F },
		tuple{ -7.33333F,   4.66667F,   2.42188F,  -5.23438F, -10.00000F },
		tuple{ -7.33333F,   6.00000F,   3.98438F,  -3.82812F,  -7.65625F },
		tuple{ -7.33333F,   7.33333F,  -2.34375F, -10.00000F, -10.00000F },
		tuple{ -7.33333F,   8.66667F,   0.85938F,  -7.26562F, -10.00000F },
		tuple{ -7.33333F,  10.00000F,   6.17188F,  -1.95312F,  -6.71875F },
		tuple{ -6.00000F, -10.00000F,   6.40625F,   6.64062F,  -5.31250F },
		tuple{ -6.00000F,  -8.66667F,  -3.67188F,  -2.73438F, -10.00000F },
		tuple{ -6.00000F,  -7.33333F,  -8.43750F,  -5.78125F, -10.00000F },
		tuple{ -6.00000F,  -6.00000F,  -5.23438F,  -2.10938F,  -7.81250F },
		tuple{ -6.00000F,  -4.66667F,   2.26562F,   4.76562F,  -3.59375F },
		tuple{ -6.00000F,  -3.33333F,  -2.03125F,  -2.50000F, -10.00000F },
		tuple{ -6.00000F,  -2.00000F,   6.01562F,   0.31250F,  -7.65625F },
		tuple{ -6.00000F,  -0.66667F,   3.28125F,  -3.67188F, -10.00000F },
		tuple{ -6.00000F,   0.66667F,  -3.82812F,  -7.73438F, -10.00000F },
		tuple{ -6.00000F,   2.00000F,   5.93750F,   2.03125F,  -4.76562F },
		tuple{ -6.00000F,   3.33333F,   1.95312F,  -4.45312F, -10.00000F },
		tuple{ -6.00000F,   4.66667F,   7.57812F,  -1.09375F,  -8.28125F },
		tuple{ -6.00000F,   6.00000F,   9.21875F,  -0.78125F,  -9.14062F },
		tuple{ -6.00000F,   7.33333F,   8.43750F,  -1.25000F,  -8.28125F },
		tuple{ -6.00000F,   8.66667F,   2.18750F,  -4.92188F,  -8.43750F },
		tuple{ -6.00000F,  10.00000F,  -4.06250F,  -9.92188F, -10.00000F },
		tuple{ -4.66667F, -10.00000F,  -4.14062F,  -1.01562F, -10.00000F },
		tuple{ -4.66667F,  -8.66667F,  -2.65625F,   1.32812F,  -8.12500F },
		tuple{ -4.66667F,  -7.33333F,  -1.95312F,   3.98438F,  -5.70312F },
		tuple{ -4.66667F,  -6.00000F,  -6.48438F,  -2.42188F, -10.00000F },
		tuple{ -4.66667F,  -4.66667F,  -7.03125F,  -8.20312F, -10.00000F },
		tuple{ -4.66667F,  -3.33333F,   0.62500F,  -4.53125F,  -9.21875F },
		tuple{ -4.66667F,  -2.00000F,   7.89062F,   0.46875F,  -8.75000F },
		tuple{ -4.66667F,  -0.66667F,   9.92188F,   2.50000F,  -7.65625F },
		tuple{ -4.66667F,   0.66667F,   8.04688F,   2.34375F,  -5.62500F },
		tuple{ -4.66667F,   2.00000F,  -2.03125F,  -8.04688F, -10.00000F },
		tuple{ -4.66667F,   3.33333F,   4.53125F,  -3.82812F, -10.00000F },
		tuple{ -4.66667F,   4.66667F,   9.06250F,  -0.15625F,  -8.59375F },
		tuple{ -4.66667F,   6.00000F,   9.60938F,   1.40625F,  -9.60938F },
		tuple{ -4.66667F,   7.33333F,   9.92188F,   2.50000F,  -8.90625F },
		tuple{ -4.66667F,   8.66667F,   9.92188F,   2.96875F,  -7.26562F },
		tuple{ -4.66667F,  10.00000F,   3.82812F,  -3.20312F, -10.00000F },
		tuple{ -3.33333F, -10.00000F,   1.87500F,   2.89062F,  -6.32812F },
		tuple{ -3.33333F,  -8.66667F,  -5.00000F,  -2.26562F, -10.00000F },
		tuple{ -3.33333F,  -7.33333F,   0.39062F,   6.32812F,  -5.39062F },
		tuple{ -3.33333F,  -6.00000F,   3.35938F,   9.92188F,  -0.85938F },
		tuple{ -3.33333F,  -4.66667F,  -3.90625F,   0.54688F,  -6.79688F },
		tuple{ -3.33333F,  -3.33333F,  -5.23438F,  -4.60938F, -10.00000F },
		tuple{ -3.33333F,  -2.00000F,  -0.31250F,  -5.23438F, -10.00000F },
		tuple{ -3.33333F,  -0.66667F,   9.53125F,   2.57812F,  -6.48438F },
		tuple{ -3.33333F,   0.66667F,   9.92188F,   4.60938F,  -7.18750F },
		tuple{ -3.33333F,   2.00000F,   7.10938F,   1.25000F, -10.00000F },
		tuple{ -3.33333F,   3.33333F,   8.43750F,   1.01562F,  -8.82812F },
		tuple{ -3.33333F,   4.66667F,   5.62500F,  -2.50000F, -10.00000F },
		tuple{ -3.33333F,   6.00000F,   8.59375F,   0.85938F, -10.00000F },
		tuple{ -3.33333F,   7.33333F,   9.92188F,   3.04688F,  -8.75000F },
		tuple{ -3.33333F,   8.66667F,   6.71875F,   0.31250F, -10.00000F },
		tuple{ -3.33333F,  10.00000F,   8.35938F,   2.34375F,  -8.75000F },
		tuple{ -2.00000F, -10.00000F,   6.64062F,   6.48438F,  -8.90625F },
		tuple{ -2.00000F,  -8.66667F,   9.14062F,   9.84375F,  -3.98438F },
		tuple{ -2.00000F,  -7.33333F,  -2.26562F,   0.15625F, -10.00000F },
		tuple{ -2.00000F,  -6.00000F,   0.15625F,   4.06250F,  -5.39062F },
		tuple{ -2.00000F,  -4.66667F,   0.31250F,   5.39062F,  -4.45312F },
		tuple{ -2.00000F,  -3.33333F,   4.84375F,   9.21875F,   0.54688F },
		tuple{ -2.00000F,  -2.00000F,  -3.20312F,  -1.09375F,  -7.26562F },
		tuple{ -2.00000F,  -0.66667F,  -4.68750F,  -5.70312F, -10.00000F },
		tuple{ -2.00000F,   0.66667F,   2.73438F,  -2.10938F, -10.00000F },
		tuple{ -2.00000F,   2.00000F,   9.92188F,   2.81250F,  -7.57812F },
		tuple{ -2.00000F,   3.33333F,   9.92188F,   2.50000F,  -8.82812F },
		tuple{ -2.00000F,   4.66667F,   9.37500F,   1.09375F,  -9.45312F },
		tuple{ -2.00000F,   6.00000F,   5.54688F,  -2.96875F, -10.00000F },
		tuple{ -2.00000F,   7.33333F,   5.31250F,  -2.81250F, -10.00000F },
		tuple{ -2.00000F,   8.66667F,   3.59375F,  -2.96875F, -10.00000F },
		tuple{ -2.00000F,  10.00000F,   9.76562F,   3.75000F,  -6.56250F },
		tuple{ -0.66667F, -10.00000F,  -4.06250F,  -5.39062F, -10.00000F },
		tuple{ -0.66667F,  -8.66667F,   6.40625F,   5.15625F,  -4.37500F },
		tuple{ -0.66667F,  -7.33333F,   9.45312F,   8.75000F,  -2.10938F },
		tuple{ -0.66667F,  -6.00000F,  -5.93750F,  -4.53125F, -10.00000F },
		tuple{ -0.66667F,  -4.66667F,  -0.78125F,   4.92188F,  -6.87500F },
		tuple{ -0.66667F,  -3.33333F,   1.79688F,   9.06250F,  -2.10938F },
		tuple{ -0.66667F,  -2.00000F,   1.79688F,   8.04688F,  -1.71875F },
		tuple{ -0.66667F,  -0.66667F,  -6.17188F,  -2.81250F,  -9.84375F },
		tuple{ -0.66667F,   0.66667F,  -6.56250F,  -8.28125F, -10.00000F },
		tuple{ -0.66667F,   2.00000F,  -0.15625F,  -6.09375F,  -8.90625F },
		tuple{ -0.66667F,   3.33333F,   8.35938F,  -0.62500F,  -7.73438F },
		tuple{ -0.66667F,   4.66667F,   9.14062F,  -0.46875F,  -7.81250F },
		tuple{ -0.66667F,   6.00000F,   4.06250F,  -2.96875F,  -6.87500F },
		tuple{ -0.66667F,   7.33333F,  -5.00000F, -10.00000F, -10.00000F },
		tuple{ -0.66667F,   8.66667F,  -3.67188F,  -8.35938F, -10.00000F },
		tuple{ -0.66667F,  10.00000F,   1.48438F,  -2.81250F,  -6.01562F },
		tuple{  0.66667F, -10.00000F,   1.64062F,  -8.35938F,  -7.34375F },
		tuple{  0.66667F,  -8.66667F,   3.75000F,  -3.90625F,  -4.60938F },
		tuple{  0.66667F,  -7.33333F,   1.25000F,  -1.87500F,  -5.93750F },
		tuple{  0.66667F,  -6.00000F,  -5.62500F,  -4.29688F, -10.00000F },
		tuple{  0.66667F,  -4.66667F,   1.87500F,   7.65625F,  -3.20312F },
		tuple{  0.66667F,  -3.33333F,  -1.48438F,   5.70312F,  -5.46875F },
		tuple{  0.66667F,  -2.00000F,  -1.09375F,   4.53125F,  -3.90625F },
		tuple{  0.66667F,  -0.66667F,  -6.95312F,  -5.93750F,  -7.96875F },
		tuple{  0.66667F,   0.66667F,   2.50000F,  -3.82812F,   4.14062F },
		tuple{  0.66667F,   2.00000F,   4.14062F,  -5.23438F,   4.37500F },
		tuple{  0.66667F,   3.33333F,  -1.56250F,  -9.68750F,  -6.95312F },
		tuple{  0.66667F,   4.66667F,   1.17188F,  -5.70312F,  -4.60938F },
		tuple{  0.66667F,   6.00000F,  -5.00000F, -10.00000F,  -6.40625F },
		tuple{  0.66667F,   7.33333F,  -0.93750F,  -6.87500F,   0.23438F },
		tuple{  0.66667F,   8.66667F,   1.64062F,  -5.78125F,   3.28125F },
		tuple{  0.66667F,  10.00000F,  -2.65625F, -10.00000F,  -0.62500F },
		tuple{  2.00000F, -10.00000F,   3.98438F,  -7.10938F,  -5.39062F },
		tuple{  2.00000F,  -8.66667F,   9.92188F,  -0.78125F,   0.93750F },
		tuple{  2.00000F,  -7.33333F,   7.26562F,  -3.43750F,  -2.03125F },
		tuple{  2.00000F,  -6.00000F,  -1.01562F,  -7.73438F,  -8.75000F },
		tuple{  2.00000F,  -4.66667F,  -1.95312F,  -0.85938F,  -7.03125F },
		tuple{  2.00000F,  -3.33333F,   5.62500F,   9.92188F,   2.57812F },
		tuple{  2.00000F,  -2.00000F,   3.98438F,   6.71875F,   1.40625F },
		tuple{  2.00000F,  -0.66667F,  -5.85938F,  -6.71875F,  -7.18750F },
		tuple{  2.00000F,   0.66667F,  -1.09375F,  -7.65625F,  -1.01562F },
		tuple{  2.00000F,   2.00000F,   8.04688F,  -0.54688F,   8.90625F },
		tuple{  2.00000F,   3.33333F,   8.20312F,   1.48438F,   9.06250F },
		tuple{  2.00000F,   4.66667F,  -1.48438F,  -8.04688F,  -0.70312F },
		tuple{  2.00000F,   6.00000F,  -2.65625F,  -9.92188F,  -1.56250F },
		tuple{  2.00000F,   7.33333F,   2.26562F,  -6.56250F,   3.75000F },
		tuple{  2.00000F,   8.66667F,   4.53125F,  -6.17188F,   6.71875F },
		tuple{  2.00000F,  10.00000F,   3.98438F,  -7.73438F,   6.56250F },
		tuple{  3.33333F, -10.00000F,   3.75000F,  -9.21875F,  -6.09375F },
		tuple{  3.33333F,  -8.66667F,   9.06250F,  -4.14062F,  -1.09375F },
		tuple{  3.33333F,  -7.33333F,   5.39062F,  -7.65625F,  -5.31250F },
		tuple{  3.33333F,  -6.00000F,   6.17188F,  -5.31250F,  -3.90625F },
		tuple{  3.33333F,  -4.66667F,   4.06250F,  -4.14062F,  -4.37500F },
		tuple{  3.33333F,  -3.33333F,   1.09375F,  -2.65625F,  -5.62500F },
		tuple{  3.33333F,  -2.00000F,   0.62500F,   2.42188F,  -4.60938F },
		tuple{  3.33333F,  -0.66667F,   5.93750F,   9.29688F,   2.34375F },
		tuple{  3.33333F,   0.66667F,  -1.87500F,  -1.09375F,  -3.82812F },
		tuple{  3.33333F,   2.00000F,  -2.42188F,  -5.00000F,  -3.35938F },
		tuple{  3.33333F,   3.33333F,   6.25000F,  -0.23438F,   6.17188F },
		tuple{  3.33333F,   4.66667F,   6.56250F,  -2.03125F,   6.79688F },
		tuple{  3.33333F,   6.00000F,   8.43750F,  -0.39062F,   8.67188F },
		tuple{  3.33333F,   7.33333F,   0.70312F,  -8.51562F,   1.09375F },
		tuple{  3.33333F,   8.66667F,   1.32812F,  -8.43750F,   2.18750F },
		tuple{  3.33333F,  10.00000F,   2.42188F,  -7.65625F,   3.59375F },
		tuple{  4.66667F, -10.00000F,  -6.56250F, -10.00000F,  -8.98438F },
		tuple{  4.66667F,  -8.66667F,  -4.37500F,  -7.65625F,  -4.84375F },
		tuple{  4.66667F,  -7.33333F,  -6.95312F,  -8.51562F,  -3.75000F },
		tuple{  4.66667F,  -6.00000F,  -4.06250F,  -7.26562F,  -2.57812F },
		tuple{  4.66667F,  -4.66667F,   3.20312F,  -5.31250F,  -2.89062F },
		tuple{  4.66667F,  -3.33333F,   5.23438F,  -3.35938F,  -3.43750F },
		tuple{  4.66667F,  -2.00000F,   1.71875F,  -1.87500F,  -4.45312F },
		tuple{  4.66667F,  -0.66667F,  -1.95312F,  -1.09375F,  -6.48438F },
		tuple{  4.66667F,   0.66667F,  -0.31250F,   4.60938F,  -4.29688F },
		tuple{  4.66667F,   2.00000F,  -6.25000F,  -2.50000F,  -9.06250F },
		tuple{  4.66667F,   3.33333F,  -6.25000F,  -8.35938F,  -7.65625F },
		tuple{  4.66667F,   4.66667F,   1.48438F,  -5.31250F,   1.01562F },
		tuple{  4.66667F,   6.00000F,   7.96875F,  -1.79688F,   8.20312F },
		tuple{  4.66667F,   7.33333F,   9.21875F,  -0.70312F,   9.68750F },
		tuple{  4.66667F,   8.66667F,   4.37500F,  -2.65625F,   5.00000F },
		tuple{  4.66667F,  10.00000F,  -5.23438F, -10.00000F,  -4.60938F },
		tuple{  6.00000F, -10.00000F,  -9.53125F,  -9.53125F,  -8.59375F },
		tuple{  6.00000F,  -8.66667F, -10.00000F,  -9.37500F,  -7.18750F },
		tuple{  6.00000F,  -7.33333F, -10.00000F,  -7.81250F,  -3.20312F },
		tuple{  6.00000F,  -6.00000F,  -5.39062F,  -0.78125F,   4.68750F },
		tuple{  6.00000F,  -4.66667F,  -4.21875F,  -0.93750F,   3.59375F },
		tuple{  6.00000F,  -3.33333F,  -4.29688F,  -5.70312F,  -1.95312F },
		tuple{  6.00000F,  -2.00000F,   8.28125F,  -1.09375F,   1.71875F },
		tuple{  6.00000F,  -0.66667F,   6.09375F,  -4.37500F,  -3.51562F },
		tuple{  6.00000F,   0.66667F,  -3.43750F,  -7.89062F, -10.00000F },
		tuple{  6.00000F,   2.00000F,  -1.79688F,  -2.96875F,  -9.45312F },
		tuple{  6.00000F,   3.33333F,   9.53125F,   9.06250F,  -2.65625F },
		tuple{  6.00000F,   4.66667F,   3.43750F,   1.48438F,  -7.57812F },
		tuple{  6.00000F,   6.00000F,  -1.56250F,  -7.34375F,  -6.01562F },
		tuple{  6.00000F,   7.33333F,   6.56250F,  -1.71875F,   5.93750F },
		tuple{  6.00000F,   8.66667F,   2.26562F,  -6.56250F,   2.65625F },
		tuple{  6.00000F,  10.00000F,   3.59375F,  -5.70312F,   4.45312F },
		tuple{  7.33333F, -10.00000F,  -9.45312F,  -9.45312F,  -8.67188F },
		tuple{  7.33333F,  -8.66667F,  -9.68750F,  -9.53125F,  -8.51562F },
		tuple{  7.33333F,  -7.33333F,  -9.84375F,  -9.60938F,  -8.28125F },
		tuple{  7.33333F,  -6.00000F, -10.00000F,  -8.20312F,  -5.85938F },
		tuple{  7.33333F,  -4.66667F, -10.00000F,  -2.34375F,   1.25000F },
		tuple{  7.33333F,  -3.33333F,  -3.90625F,   2.89062F,   7.10938F },
		tuple{  7.33333F,  -2.00000F,  -4.06250F,  -3.75000F,   0.00000F },
		tuple{  7.33333F,  -0.66667F,  -2.50000F,  -5.39062F,  -4.76562F },
		tuple{  7.33333F,   0.66667F,  -2.18750F,  -4.76562F, -10.00000F },
		tuple{  7.33333F,   2.00000F,   8.28125F,   6.48438F,  -2.42188F },
		tuple{  7.33333F,   3.33333F,   8.90625F,   8.20312F,  -2.03125F },
		tuple{  7.33333F,   4.66667F,   9.92188F,   9.92188F,   0.85938F },
		tuple{  7.33333F,   6.00000F,   8.20312F,   7.73438F,   1.17188F },
		tuple{  7.33333F,   7.33333F,  -1.48438F,  -4.37500F,  -5.78125F },
		tuple{  7.33333F,   8.66667F,  -2.34375F,  -9.45312F,  -3.43750F },
		tuple{  7.33333F,  10.00000F,   4.14062F,  -5.07812F,   4.53125F },
		tuple{  8.66667F, -10.00000F,  -9.68750F,  -9.53125F,  -8.59375F },
		tuple{  8.66667F,  -8.66667F,  -9.76562F,  -9.60938F,  -8.59375F },
		tuple{  8.66667F,  -7.33333F,  -9.68750F,  -9.53125F,  -8.51562F },
		tuple{  8.66667F,  -6.00000F,  -9.84375F,  -9.45312F,  -7.89062F },
		tuple{  8.66667F,  -4.66667F, -10.00000F,  -9.37500F,  -7.10938F },
		tuple{  8.66667F,  -3.33333F,  -9.29688F,  -6.71875F,  -2.57812F },
		tuple{  8.66667F,  -2.00000F,  -5.70312F,  -0.62500F,   6.56250F },
		tuple{  8.66667F,  -0.66667F,  -9.06250F,  -4.21875F,   0.23438F },
		tuple{  8.66667F,   0.66667F,  -3.51562F,  -1.56250F,  -5.85938F },
		tuple{  8.66667F,   2.00000F,   8.90625F,   9.14062F,   1.09375F },
		tuple{  8.66667F,   3.33333F,   9.92188F,   9.92188F,   2.89062F },
		tuple{  8.66667F,   4.66667F,   8.59375F,   8.43750F,   1.64062F },
		tuple{  8.66667F,   6.00000F,   6.17188F,   5.78125F,  -0.54688F },
		tuple{  8.66667F,   7.33333F,   9.92188F,   9.92188F,   4.37500F },
		tuple{  8.66667F,   8.66667F,   5.15625F,   3.98438F,  -1.25000F },
		tuple{  8.66667F,  10.00000F,  -5.54688F,  -6.95312F, -10.00000F },
		tuple{ 10.00000F, -10.00000F,  -9.60938F,  -9.45312F,  -8.51562F },
		tuple{ 10.00000F,  -8.66667F,  -9.68750F,  -9.53125F,  -8.59375F },
		tuple{ 10.00000F,  -7.33333F,  -9.76562F,  -9.45312F,  -8.51562F },
		tuple{ 10.00000F,  -6.00000F,  -9.68750F,  -9.37500F,  -8.43750F },
		tuple{ 10.00000F,  -4.66667F,  -9.76562F,  -9.45312F,  -8.51562F },
		tuple{ 10.00000F,  -3.33333F, -10.00000F,  -9.53125F,  -7.65625F },
		tuple{ 10.00000F,  -2.00000F, -10.00000F,  -9.29688F,  -6.01562F },
		tuple{ 10.00000F,  -0.66667F,  -9.68750F,  -6.40625F,  -2.50000F },
		tuple{ 10.00000F,   0.66667F,  -6.25000F,   1.40625F,   4.92188F },
		tuple{ 10.00000F,   2.00000F,  -3.28125F,   4.06250F,   5.00000F },
		tuple{ 10.00000F,   3.33333F,   2.50000F,   4.76562F,   1.17188F },
		tuple{ 10.00000F,   4.66667F,   8.28125F,   8.12500F,   1.32812F },
		tuple{ 10.00000F,   6.00000F,   9.60938F,   9.53125F,   0.46875F },
		tuple{ 10.00000F,   7.33333F,   8.98438F,   8.90625F,  -2.03125F },
		tuple{ 10.00000F,   8.66667F,   9.84375F,   9.92188F,  -2.81250F },
		tuple{ 10.00000F,  10.00000F,   5.07812F,   5.07812F,  -8.51562F }

#endif

		// clang-format on
	};
}
