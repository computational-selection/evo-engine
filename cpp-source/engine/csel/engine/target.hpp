#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_TARGET_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_TARGET_HPP

#include <array>
#include <cstdint>
#include <random>

#include <boost/operators.hpp>

#include <cereal/cereal.hpp>

#include "csel/engine/input_cat.hpp"
#include "csel/util/cereal/serialize_fns.hpp"
#include "csel/util/cereal/traits.hpp"
#include "csel/util/cereal/unified_serializable.hpp"

namespace csel {

	/// Point to the part in the target that this node should modified
	///
	/// For the two actions that change the target's nodes input, it indicates what the target's
	/// input should be changed to
	enum class target_part : bool {
		COMP_OR_TC0_OR_LOOP = 0, ///< comparator (or testcase0 or loop-ctr)
		VALU_OR_TC1_OR_MYVL = 1  ///< value      (or testcase1 or my-value)
	};

	/// All possible target_part values
	inline constexpr ::std::array ALL_TARGET_PARTS = { target_part::COMP_OR_TC0_OR_LOOP, target_part::VALU_OR_TC1_OR_MYVL };

	target_part target_part_from_string( const ::std::string & );

	::std::istream &operator>>( ::std::istream &, target_part & );

	::std::string to_string( const target_part & );

	::std::ostream &operator<<( ::std::ostream &, const target_part & );

	/// Define how cereal should convert a target_part value to a string
	///
	/// \param prm_target_part The value to convert
	template <class Archive>
	::std::string save_minimal( const Archive &, const target_part &prm_target_part ) {
		return to_string( prm_target_part );
	}

	/// Define how cereal should convert a string to a target_part value
	///
	/// \param prm_target_part The value to populate
	/// \param value           The string from which the value should be extracted
	template <class Archive>
	void load_minimal( const Archive &, target_part &prm_target_part, const ::std::string &value ) {
		prm_target_part = target_part_from_string( value );
	}

	/// Whether the action is to be applied to the target or be set-to the target (ie become the new action for the target)
	///
	/// Currently unused - evaluate_nodes() throws for anything other than APPLY_ACTION
	enum class action_meaning : ::std::uint8_t {
		APPLY_ACTION,         ///< Apply this node's action to the target node
		SET_ACTION_TO,        ///< Set the action in the target node to the action set in this node
		META_SET_ACTION_ONCE, ///< Set the target node to set the action in it's target node

		/// Set the target node to set _its_ target node to set _its_ target node etc....
		/// This will just have the effect of preventing that resulting chain of nodes from doing anything else,
		/// which seems a bit silly.
		META_SET_ACTION_INF
	};

	/// All possible action_meaning values
	inline constexpr ::std::array ALL_ACTION_MEANINGS = {
		action_meaning::APPLY_ACTION,
		action_meaning::SET_ACTION_TO,
		action_meaning::META_SET_ACTION_ONCE,
		action_meaning::META_SET_ACTION_INF,
	};

	::std::string to_string( const action_meaning & );

	::std::ostream &operator<<( ::std::ostream &, const action_meaning & );

	namespace detail {
		using target_value_t = ::std::uint16_t;

		inline constexpr ::std::uint8_t NUM_BITS_LEFT_FOR_FLAG = 15;

		inline constexpr ::std::uint8_t NUM_BITS_LEFT_FOR_MEANING = 13;

		inline constexpr ::std::uint16_t BITMASK_FOR_FLAG = 0x8000;

		inline constexpr ::std::uint16_t BITMASK_FOR_MEANING = 0x6000;

		inline constexpr ::std::uint16_t BITMASK_FOR_NOTTARGET = 0xF000;

		inline constexpr ::std::uint16_t BITMASK_FOR_TARGET = 0x0FFF;

		/// Pack the constituent parts of a target value into a uint16_t
		///
		/// \param prm_part     The part within the target node
		/// \param prm_meaning  What to do with the action in target node (eg apply the action, write the action)
		/// \param prm_node_idx The index of the target node
		inline constexpr ::std::uint16_t make_target_value( const target_part &    prm_part,
		                                                    const action_meaning & prm_meaning,
		                                                    const ::std::uint16_t &prm_node_idx ) noexcept {
			// clang-format off
			const auto flag_ored_with_meaning = (
				static_cast<::std::uint16_t>( static_cast<::std::uint16_t>( prm_part    ) << NUM_BITS_LEFT_FOR_FLAG   )
				|
				static_cast<::std::uint16_t>( static_cast<::std::uint16_t>( prm_meaning ) << NUM_BITS_LEFT_FOR_MEANING )
			);
			return static_cast<::std::uint16_t>(
				static_cast<::std::uint16_t>(
					flag_ored_with_meaning
				)
				|
				prm_node_idx

			);
			// clang-format on
		}

	} // namespace detail

	/// The details of where(node+part)/how(applied/copied) an action will be applied
	struct target
	        : private ::boost::equality_comparable<target>
	        , util::unified_serializable<target> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<target>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

		/// A single target_value_t containing the different components of the target packed together
		detail::target_value_t value =
		  detail::make_target_value( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION, 0 );

	  public:
		constexpr target() noexcept = default;
		constexpr target( const target_part &, const action_meaning &, const ::std::uint16_t & );

		constexpr target &update_node_idx( const ::std::uint16_t & );

		[[nodiscard]] constexpr target_part part() const;

		[[nodiscard]] constexpr action_meaning meaning() const;

		[[nodiscard]] constexpr ::std::uint16_t node_index() const;

		/// Whether the two specified target values are equal
		///
		/// \param prm_lhs The first  target to compare
		/// \param prm_rhs The second target to compare
		friend constexpr bool operator==( const target &prm_lhs, const target &prm_rhs ) {
			// clang-format off
			return (
				prm_lhs.part       () == prm_rhs.part       ()
				&&
				prm_lhs.meaning    () == prm_rhs.meaning    ()
				&&
				prm_lhs.node_index () == prm_rhs.node_index ()
			);
			// clang-format on
		}
	};

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The target to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void target::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		if constexpr ( !util::cereal::is_text_archive_v<Archive> ) {
			prm_archive( prm_serializee.value );
		} else {
			target_part     the_part       = prm_serializee.part();
			action_meaning  the_meaning    = prm_serializee.meaning();
			::std::uint16_t the_node_index = prm_serializee.node_index();
			prm_archive( ::cereal::make_nvp( "part", the_part ),
			             ::cereal::make_nvp( "meaning", the_meaning ),
			             ::cereal::make_nvp( "node_index", the_node_index ) );
			if constexpr ( dirn == util::cereal_dirn::LOADING ) {
				prm_serializee.value = detail::make_target_value( the_part, the_meaning, the_node_index );
			}
		}
	}

	/// Ctor
	///
	/// \param prm_meaning    prm_part     The part within the target node
	/// \param prm_input_cat  prm_meaning  What to do with the action in target node (eg apply the action, write the action)
	/// \param prm_new_target prm_node_idx The index of the target node
	inline constexpr target::target( const target_part &    prm_part,
	                                 const action_meaning & prm_meaning,
	                                 const ::std::uint16_t &prm_node_idx ) :
	        value{ detail::make_target_value( prm_part, prm_meaning, prm_node_idx ) } {
	}

	inline constexpr target &target::update_node_idx( const ::std::uint16_t &prm_new_target_idx ) {
		value = ( static_cast<::std::uint16_t>( value & detail::BITMASK_FOR_NOTTARGET ) | prm_new_target_idx );
		return *this;
	}

	inline constexpr target_part target::part() const {
		// clang-format off
		return static_cast<target_part>(
			static_cast<::std::uint16_t>(
				value & static_cast<::std::uint16_t>( detail::BITMASK_FOR_FLAG )
			) >> detail::NUM_BITS_LEFT_FOR_FLAG
		);
		// clang-format on
	}

	inline constexpr action_meaning target::meaning() const {
		return static_cast<action_meaning>(
		  static_cast<::std::uint16_t>( value & static_cast<::std::uint16_t>( detail::BITMASK_FOR_MEANING ) )
		  >> detail::NUM_BITS_LEFT_FOR_MEANING );
	}

	inline constexpr ::std::uint16_t target::node_index() const {
		return ( value & static_cast<::std::uint16_t>( detail::BITMASK_FOR_TARGET ) );
	}

	target_part     generate_random_flag( ::std::mt19937 & );
	action_meaning  generate_random_action_meaning( ::std::mt19937 & );
	::std::uint16_t generate_random_target_index( ::std::mt19937 &, const ::std::uint16_t & );
	target          generate_random_target( ::std::mt19937 &, const ::std::uint16_t & );

	::std::string to_string( const target & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_TARGET_HPP
