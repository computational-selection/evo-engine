#include "individual.hpp"

#include <ostream>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/action/join.hpp>
#include <range/v3/view/indices.hpp>
#include <range/v3/view/transform.hpp>

using namespace csel;

using ::std::mt19937;
using ::std::ostream;
using ::std::string;
using ::std::vector;

namespace actions = ::ranges::actions;
namespace views   = ::ranges::views;

/// Randomly generate an individual with the specified number of nodes
///
/// \param prm_rng       The random number engine to use
/// \param prm_num_nodes The number of nodes to generate
individual csel::generate_random_individual_with_n_nodes( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	return individual{ generate_n_random_nodes( prm_rng, prm_num_nodes ) };
}

/// Randomly generate the specified number of nodes
///
/// \param prm_rng       The random number engine to use
/// \param prm_num_nodes The number of nodes to generate
vector<node> csel::generate_n_random_nodes( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	vector<node> result;
	result.reserve( prm_num_nodes );
	for ( [[maybe_unused]] const uint16_t &node_ctr : views::indices( prm_num_nodes ) ) {
		result.push_back( generate_random_node( prm_rng, prm_num_nodes ) );
	}
	return result;
}

/// \brief Generate a string describing the specified individual
///
/// \param prm_individual The individual to describe
string csel::to_string( const individual &prm_individual ) {
	// clang-format off
	return (
		"individual["
		+ (
			prm_individual
				| views::transform( []( const node &x ) { return "\n\t" + to_string( x ); } )
				| actions::join
		)
		+ "\n]"
	);
	// clang-format on
}

/// \brief Insert a description of the specified individual into the specified ostream
///
/// \param prm_ostream    The ostream into which the description should be inserted
/// \param prm_individual The individual to describe
ostream &csel::operator<<( ostream &prm_ostream, const individual &prm_individual ) {
	prm_ostream << to_string( prm_individual );
	return prm_ostream;
}
