#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_ACTION_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_ACTION_HPP

#include <cstdint>
#include <iosfwd>
#include <random>
#include <stdexcept>
#include <string>

#include <boost/operators.hpp>

#include <gsl/gsl>

#include <cereal/cereal.hpp>

#include "csel/util/cereal/traits.hpp"
#include "csel/util/cereal/unified_serializable.hpp"
#include "csel/util/exception/csel_throw_exception.hpp"

namespace csel {

	namespace detail {
		using action_value_t = ::std::uint16_t;
	} // namespace detail

	/// The categories of action
	///
	// clang-format off
	enum class action_cat : detail::action_value_t {
		ADD_MY_VALUE_TO          =  0, // COMP / VAL      [according to the target_part]
		SUBTR_MY_VALUE_FROM      =  1, // COMP / VAL      [according to the target_part]
		MULTIPLY_MY_VALUE_TO     =  2, // COMP / VAL      [according to the target_part]
		DIVIDE_MY_VALUE_FROM     =  3, // COMP / VAL      [according to the target_part]
		WRITE_MY_VALUE_OVER      =  4, // COMP / VAL      [according to the target_part]

		CHANGE_INPUT_TO_TC       =  5, // TC0  / TC1      [according to the target_part]
		CHANGE_INPUT_TO_XX       =  6, // LOOP / <MY-VAL> [according to the target_part]

		// Specific ordering is used by uses_other_node(), below

		CHANGE_TARGET_TO_OTHER   =  7, // COMP / VAL      [according to the target_part]

		ADD_OTHERS_VALUE_TO      =  8, // COMP / VAL      [according to the target_part]
		SUBTR_OTHERS_VALUE_FROM  =  9, // COMP / VAL      [according to the target_part]
		MULTIPLY_OTHERS_VALUE_TO = 10, // COMP / VAL      [according to the target_part]
		DIVIDE_OTHERS_VALUE_FROM = 11, // COMP / VAL      [according to the target_part]
		WRITE_OTHERS_VALUE_OVER  = 12, // COMP / VAL      [according to the target_part]
	};
	// clang-format on

	/// All possible action_cat values
	inline constexpr ::std::array ALL_ACTION_CAT_VALUES = {
		action_cat::ADD_MY_VALUE_TO,         action_cat::SUBTR_MY_VALUE_FROM,      action_cat::MULTIPLY_MY_VALUE_TO,
		action_cat::DIVIDE_MY_VALUE_FROM,    action_cat::WRITE_MY_VALUE_OVER,      action_cat::CHANGE_INPUT_TO_TC,
		action_cat::CHANGE_INPUT_TO_XX,      action_cat::CHANGE_TARGET_TO_OTHER,   action_cat::ADD_OTHERS_VALUE_TO,
		action_cat::SUBTR_OTHERS_VALUE_FROM, action_cat::MULTIPLY_OTHERS_VALUE_TO, action_cat::DIVIDE_OTHERS_VALUE_FROM,
		action_cat::WRITE_OTHERS_VALUE_OVER
	};

	inline constexpr detail::action_value_t MAX_ACTION_CAT_UNDERLYING =
	  static_cast<detail::action_value_t>( action_cat::CHANGE_TARGET_TO_OTHER );

	inline constexpr bool uses_other_node( const action_cat &prm_action_cat ) {
		return static_cast<detail::action_value_t>( prm_action_cat )
		       >= static_cast<detail::action_value_t>( action_cat::CHANGE_TARGET_TO_OTHER );
	}

	action_cat action_cat_from_string( const ::std::string & );

	::std::istream &operator>>( ::std::istream &, action_cat & );

	::std::string to_string( const action_cat & );

	::std::ostream &operator<<( ::std::ostream &, const action_cat & );

	/// Define how cereal should convert an action_cat value to a string
	///
	/// \param prm_action_cat The value to convert
	template <class Archive>
	::std::string save_minimal( const Archive &, const action_cat &prm_action_cat ) {
		return to_string( prm_action_cat );
	}

	/// Define how cereal should convert a string to an action_cat value
	///
	/// \param prm_target_part The value to populate
	/// \param value           The string from which the value should be extracted
	template <class Archive>
	void load_minimal( const Archive &, action_cat &prm_action_cat, const ::std::string &value ) {
		prm_action_cat = action_cat_from_string( value );
	}

	namespace detail {

		inline constexpr ::std::uint8_t NUM_BITS_LEFT_FOR_ACTION_CAT = 12;

		inline constexpr ::std::uint16_t BITMASK_FOR_ACTION_CAT         = 0xF000;
		inline constexpr ::std::uint16_t BITMASK_FOR_NOT_OTHER_NODE_IDX = 0xF000;
		inline constexpr ::std::uint16_t BITMASK_FOR_OTHER_NODE_IDX     = 0x0FFF;

		/// Pack the constituent parts of an action into a uint16_t
		///
		/// \param prm_action_cat     What the action does
		/// \param prm_other_node_idx The index of the other node (if setting the target node's target to another node or setting data in the target to data read from another node)
		inline constexpr detail::action_value_t make_action_value( const action_cat &     prm_action_cat,
		                                                           const ::std::uint16_t &prm_other_node_idx ) {
			Expects( prm_other_node_idx < ( 1UL << NUM_BITS_LEFT_FOR_ACTION_CAT ) );
			// clang-format off
			return static_cast<::std::uint16_t>(
				static_cast<::std::uint16_t>( ( static_cast<::std::uint16_t>( prm_action_cat ) << NUM_BITS_LEFT_FOR_ACTION_CAT ) )
				|
				prm_other_node_idx
			);
			// clang-format on
		}

	} // namespace detail

	/// The action to perform on another node (using some third-party node in advanced cases)
	///
	/// TODO: Decide on whether to go strict/lax re other_node_idx
	///       and either police it on construction too
	///       or allow users to do whatever they like
	class action
	        : private ::boost::equality_comparable<action>
	        , util::unified_serializable<action> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<action>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

		/// The default value for what the action does
		inline static constexpr action_cat DEFAULT_ACTION_CAT = action_cat::ADD_MY_VALUE_TO;

		/// The default value for the index of the other node
		inline static constexpr ::std::uint16_t DEFAULT_OTHER_NODE_IDX = 5;

		/// A default value (by packing together the defaults for action_cat and other_node_idx)
		inline static constexpr detail::action_value_t DEFAULT_VALUE =
		  detail::make_action_value( DEFAULT_ACTION_CAT, DEFAULT_OTHER_NODE_IDX );

		detail::action_value_t m_value = DEFAULT_VALUE;

	  public:
		constexpr action() = default;

		constexpr action( const action_cat &, const ::std::uint16_t & );

		constexpr action &update_other_node_idx( const ::std::uint16_t & );

		[[nodiscard]] constexpr action_cat cat() const noexcept;

		[[nodiscard]] constexpr ::std::uint16_t other_node_idx() const;

		/// Whether the two specified action values are equal
		///
		/// \param prm_lhs The first  action to compare
		/// \param prm_rhs The second action to compare
		friend constexpr bool operator==( const action &prm_lhs, const action &prm_rhs ) {
			// clang-format off
			return (
				prm_lhs.cat() == prm_rhs.cat()
				&&
				(
					! uses_other_node( prm_lhs.cat() )
					||
					prm_lhs.other_node_idx() == prm_rhs.other_node_idx()
				)
			);
			// clang-format on
		}
	};

	static_assert( sizeof( action ) == 2 );

	/// Whether the action_cat of the specified action involves an other_node_idx
	///
	/// \param prm_action The action of interest
	inline constexpr bool has_other_node_index( const action &prm_action ) {
		return uses_other_node( prm_action.cat() );
	}

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The action to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void action::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		if constexpr ( !util::cereal::is_text_archive_v<Archive> ) {
			prm_archive( prm_serializee.m_value );
		} else {
			action_cat temp_cat = prm_serializee.cat();
			prm_archive( ::cereal::make_nvp( "action_cat", temp_cat ) );

			::std::uint16_t temp_other_node_idx = 0;
			if ( uses_other_node( temp_cat ) ) {
				if constexpr ( dirn == util::cereal_dirn::SAVING ) {
					temp_other_node_idx = prm_serializee.other_node_idx();
				}
				prm_archive( ::cereal::make_nvp( "other_node_idx", temp_other_node_idx ) );
			}

			if constexpr ( dirn == util::cereal_dirn::LOADING ) {
				prm_serializee = action{ temp_cat, temp_other_node_idx };
			}
		}
	}

	/// Ctor
	///
	/// TODO Consider tightening up ctors to expect second parameter iff uses_other_node( prm_cat )
	///
	/// \param prm_action_cat     What the action does
	/// \param prm_other_node_idx The index of the other node
	inline constexpr action::action( const action_cat &prm_action_cat, const ::std::uint16_t &prm_other_node_idx ) :
	        m_value{ detail::make_action_value( prm_action_cat, prm_other_node_idx ) } {
	}

	/// Update this action's other_node_idx
	///
	/// \param prm_new_other_node_idx The new index of the other node
	inline constexpr action &action::update_other_node_idx( const ::std::uint16_t &prm_new_other_node_idx ) {
		Expects( prm_new_other_node_idx < ( 1UL << detail::NUM_BITS_LEFT_FOR_ACTION_CAT ) );
		m_value =
		  ( static_cast<::std::uint16_t>( m_value & detail::BITMASK_FOR_NOT_OTHER_NODE_IDX ) | prm_new_other_node_idx );
		return *this;
	}

	/// Getter for what the action does
	inline constexpr action_cat action::cat() const noexcept {
		return static_cast<action_cat>(
		  static_cast<::std::uint16_t>( m_value & static_cast<::std::uint16_t>( detail::BITMASK_FOR_ACTION_CAT ) )
		  >> detail::NUM_BITS_LEFT_FOR_ACTION_CAT );
	}

	/// Getter for the index of the other node
	inline constexpr ::std::uint16_t action::other_node_idx() const {
		if ( !has_other_node_index( *this ) ) {
			CSEL_THROW( ::std::invalid_argument( "oh dear" ) );
		}
		return ( m_value & static_cast<::std::uint16_t>( detail::BITMASK_FOR_OTHER_NODE_IDX ) );
	}

	action_cat      generate_random_action_cat( ::std::mt19937 & );
	::std::uint16_t generate_random_other_node_idx( ::std::mt19937 &, const ::std::uint16_t & );
	action          generate_random_action( ::std::mt19937 &, const ::std::uint16_t & );

	::std::string action_cat_string_of_action( const action & );

	::std::string to_string( const action & );

	::std::ostream &operator<<( ::std::ostream &, const action & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_ACTION_HPP
