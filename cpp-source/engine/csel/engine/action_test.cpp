#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include <fmt/core.h>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/indices.hpp>

#include "csel/engine/action.hpp"
#include "csel/util/cereal/archive_pairs.hpp"
#include "csel/util/size_t_literal.hpp"

using namespace csel;
using namespace csel::util::cereal;

using ::cereal::BinaryOutputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLOutputArchive;

namespace views = ::ranges::views;

TEST_CASE( "action basic" ) {
	using ::std::to_string;

	// clang-format off
	CHECK( to_string( action_cat::ADD_MY_VALUE_TO         ) == "ADD_MY_VALUE_TO"        );
	CHECK( to_string( action_cat::CHANGE_TARGET_TO_OTHER  ) == "CHANGE_TARGET_TO_OTHER" );

	CHECK( to_string( action( action_cat::ADD_MY_VALUE_TO,         16U     ) ) == "action[cat:          ADD_MY_VALUE_TO                  ]" );
	CHECK( to_string( action( action_cat::CHANGE_TARGET_TO_OTHER,  16U     ) ) == "action[cat:   CHANGE_TARGET_TO_OTHER, other node:   16]" );
	CHECK( to_string( action( action_cat::ADD_OTHERS_VALUE_TO,     16U     ) ) == "action[cat:      ADD_OTHERS_VALUE_TO, other node:   16]" );
	CHECK( to_string( action( action_cat::ADD_OTHERS_VALUE_TO,     0x0FFFU ) ) == "action[cat:      ADD_OTHERS_VALUE_TO, other node: 4095]" );
	// clang-format on
}

TEMPLATE_TEST_CASE( "action serialization round-trips",
                    "[action]",
                    BinaryOutputArchive,
                    JSONOutputArchive,
                    PortableBinaryOutputArchive,
                    XMLOutputArchive ) {
	for ( const action_cat &action_cat : ALL_ACTION_CAT_VALUES ) {
		for ( const uint16_t &other_node_ix : views::indices( static_cast<uint16_t>( 5 ) ) ) {
			const action the_action( action_cat, other_node_ix );
			CHECK( serialise_round_trip<input_archive_of_archive_t<TestType>, output_archive_of_archive_t<TestType>>( the_action )
			       == the_action );
		}
	}
}

TEST_CASE( "example action json", "[action]" ) {
	CHECK( serialise_to_string<JSONOutputArchive>( action( action_cat::ADD_MY_VALUE_TO, 5 ) ) == R"({
    "serialized_value": {
        "cereal_class_version": 0,
        "action_cat": "ADD_MY_VALUE_TO"
    }
})" );
}

TEST_CASE( "example action xml", "[action]" ) {
	CHECK( serialise_to_string<XMLOutputArchive>( action( action_cat::ADD_OTHERS_VALUE_TO, 5 ) ) == R"(<?xml version="1.0" encoding="utf-8"?>
<cereal>
	<serialized_value>
		<cereal_class_version>0</cereal_class_version>
		<action_cat>ADD_OTHERS_VALUE_TO</action_cat>
		<other_node_idx>5</other_node_idx>
	</serialized_value>
</cereal>

)" );
}
