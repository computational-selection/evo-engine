#include "input.hpp"

#include <stdexcept>

#include <fmt/core.h>

#include "csel/util/exception/csel_throw_exception.hpp"

using namespace csel;

using ::std::bernoulli_distribution;
using ::std::logic_error;
using ::std::mt19937;
using ::std::ostream;
using ::std::string;
using ::std::uniform_int_distribution; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981
using ::std::uniform_real_distribution; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981

input csel::generate_random_input( mt19937 &prm_rng ) {
	// clang-format off
	switch ( uniform_int_distribution{ 0, 7 }( prm_rng ) ) {
		case ( 0 ) : { return loop_ctr_input       (); }
		case ( 1 ) : { return test_case_sub_0_input(); }
		case ( 2 ) : { return test_case_sub_1_input(); }
		default    : { return input{ uniform_real_distribution{ -100.0F, 100.0F }( prm_rng ) }; }
	}
	// clang-format on
}

void csel::mutate_input( input &prm_input, mt19937 &prm_rng ) {
	constexpr float MULT_DISTN_MIN = 0.995F;
	constexpr float MULT_DISTN_MAX = 1.005F;
	if ( bernoulli_distribution{}( prm_rng ) ) {
		prm_input = generate_random_input( prm_rng );
	} else {
		prm_input.value *= uniform_real_distribution{ MULT_DISTN_MIN, MULT_DISTN_MAX }( prm_rng );
	}
}

/// \brief Generate a string describing the specified input
///
/// \param prm_input The input to describe
string csel::to_string( const input &prm_input ) {
	return is_loop_ctr( prm_input )
	         ? "     loop"
	         : is_testcase_sub_0( prm_input )
	             ? "testcase0"
	             : is_testcase_sub_1( prm_input ) ? "testcase1" : fmt::format( "{:9.3f}", prm_input.value );
}

/// \brief Insert a description of the specified input into the specified ostream
///
/// \param prm_ostream The ostream into which the description should be inserted
/// \param prm_input   The input to describe
ostream &csel::operator<<( ostream &prm_ostream, const input &prm_input ) {
	prm_ostream << to_string( prm_input );
	return prm_ostream;
}
