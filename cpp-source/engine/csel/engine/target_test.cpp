#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>


#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
// #include <range/v3/range/conversion.hpp>
// #include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/indices.hpp>

#include "csel/engine/target.hpp"
#include "csel/util/cereal/archive_pairs.hpp"

using namespace csel;
using namespace csel::util::cereal;

using ::cereal::BinaryOutputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLOutputArchive;

namespace views = ::ranges::views;

TEST_CASE( "target basic" ) {

	// clang-format off
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,            0 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,           0 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE,    0 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,     0 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );

	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,         4095 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,        4095 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE, 4095 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,  4095 ).part() == target_part::COMP_OR_TC0_OR_LOOP            );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,            0 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,           0 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE,    0 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,     0 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,         4095 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,        4095 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE, 4095 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,  4095 ).part() == target_part::VALU_OR_TC1_OR_MYVL            );
	// clang-format on

	// clang-format off
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,            0 ).meaning() == action_meaning::APPLY_ACTION         );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,           0 ).meaning() == action_meaning::SET_ACTION_TO        );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE,    0 ).meaning() == action_meaning::META_SET_ACTION_ONCE );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,     0 ).meaning() == action_meaning::META_SET_ACTION_INF  );

	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,         4095 ).meaning() == action_meaning::APPLY_ACTION         );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,        4095 ).meaning() == action_meaning::SET_ACTION_TO        );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE, 4095 ).meaning() == action_meaning::META_SET_ACTION_ONCE );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,  4095 ).meaning() == action_meaning::META_SET_ACTION_INF  );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,            0 ).meaning() == action_meaning::APPLY_ACTION         );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,           0 ).meaning() == action_meaning::SET_ACTION_TO        );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE,    0 ).meaning() == action_meaning::META_SET_ACTION_ONCE );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,     0 ).meaning() == action_meaning::META_SET_ACTION_INF  );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,         4095 ).meaning() == action_meaning::APPLY_ACTION         );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,        4095 ).meaning() == action_meaning::SET_ACTION_TO        );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE, 4095 ).meaning() == action_meaning::META_SET_ACTION_ONCE );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,  4095 ).meaning() == action_meaning::META_SET_ACTION_INF  );
	// clang-format on

	// clang-format off
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,            0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,           0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE,    0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,     0 ).node_index() ==    0 );

	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::APPLY_ACTION,         4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::SET_ACTION_TO,        4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_ONCE, 4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::COMP_OR_TC0_OR_LOOP, action_meaning::META_SET_ACTION_INF,  4095 ).node_index() == 4095 );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,            0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,           0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE,    0 ).node_index() ==    0 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,     0 ).node_index() ==    0 );

	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::APPLY_ACTION,         4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::SET_ACTION_TO,        4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_ONCE, 4095 ).node_index() == 4095 );
	STATIC_REQUIRE( target( target_part::VALU_OR_TC1_OR_MYVL, action_meaning::META_SET_ACTION_INF,  4095 ).node_index() == 4095 );
	// clang-format on
}

TEMPLATE_TEST_CASE( "target serialization round-trips",
                    "[target]",
                    BinaryOutputArchive,
                    JSONOutputArchive,
                    PortableBinaryOutputArchive,
                    XMLOutputArchive ) {
	for ( const target_part &part : ALL_TARGET_PARTS ) {
		for ( const action_meaning &meaning : ALL_ACTION_MEANINGS ) {
			for ( const uint16_t &other_node_ix : views::indices( static_cast<uint16_t>( 5 ) ) ) {
				const target the_action( part, meaning, other_node_ix );
				CHECK( serialise_round_trip<input_archive_of_archive_t<TestType>, output_archive_of_archive_t<TestType>>( the_action )
					== the_action );
			}
		}
	}
}

TEST_CASE( "example target json", "[target]" ) {
	CHECK( serialise_to_string<JSONOutputArchive>( target() ) == R"({
    "serialized_value": {
        "cereal_class_version": 0,
        "part": "comp_or_tc0_or_loop",
        "meaning": 0,
        "node_index": 0
    }
})" );
}

TEST_CASE( "example target xml", "[target]" ) {
	CHECK( serialise_to_string<XMLOutputArchive>( target() ) == R"(<?xml version="1.0" encoding="utf-8"?>
<cereal>
	<serialized_value>
		<cereal_class_version>0</cereal_class_version>
		<part>comp_or_tc0_or_loop</part>
		<meaning>0</meaning>
		<node_index>0</node_index>
	</serialized_value>
</cereal>

)" );
}
