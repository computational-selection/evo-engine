#include "fitness_point.hpp"

#include <nlohmann/json.hpp>

using namespace csel;

using ::nlohmann::json;

/// Store a fitness_point in a nlohmann::json
///
/// \param prm_json          The nlohmann::json in which the fitness_point should be stored
/// \param prm_fitness_point The fitness_point to store
void csel::to_json( json &prm_json, const fitness_point &prm_fitness_point ) {
	prm_json = json::array();
	prm_json.push_back( prm_fitness_point.generation );
	prm_json.push_back( prm_fitness_point.fitness );
}

/// Extract a fitness_point from a nlohmann::json
///
/// \param prm_json          The nlohmann::json from which the fitness_point should be extracted
/// \param prm_fitness_point The fitness_point to extract
void csel::from_json( const json &prm_json, fitness_point &prm_fitness_point ) {
	prm_json.front().get_to( prm_fitness_point.generation );
	prm_json.back().get_to( prm_fitness_point.fitness );
}
