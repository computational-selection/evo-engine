#include <catch2/catch.hpp>

#include <fmt/core.h>

#include <nlohmann/json.hpp>

#include "csel/engine/fitness_point.hpp"
#include "csel/util/json/json.hpp"

using namespace csel;
using namespace csel::util;

using ::nlohmann::json;

TEST_CASE( "fitness_point serialization round-trips", "[fitness_point]" ) {
	for ( const fitness_point &the_fitness_point : {
	        fitness_point{ 0, 3.0F },
	        fitness_point{ 4, 5.0F },
	      } ) {

		CHECK( json_round_trip( the_fitness_point ) == the_fitness_point );
	}
}
