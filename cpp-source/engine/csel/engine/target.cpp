#include "target.hpp"

#include <cassert>
#include <ostream>
#include <stdexcept>
#include <string>

#include <fmt/core.h>

#include "csel/util/exception/csel_throw_exception.hpp"
#include "csel/util/type_traits.hpp"

using namespace csel;

using ::std::bernoulli_distribution;
using ::std::invalid_argument;
using ::std::istream;
using ::std::logic_error;
using ::std::mt19937;
using ::std::ostream;
using ::std::string;
using ::std::uint16_t;
using ::std::uniform_int_distribution; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981

/// \brief Extract from the specified stream into the specified target_part
///
/// \param prm_is         The stream to extract from
/// \param prm_action_cat The target_part to extract into
target_part csel::target_part_from_string( const string &prm_input_string ) {
	// clang-format off
	if ( prm_input_string == "comp_or_tc0_or_loop" ) { return target_part::COMP_OR_TC0_OR_LOOP ; }
	if ( prm_input_string == "valu_or_tc1_or_myvl" ) { return target_part::VALU_OR_TC1_OR_MYVL ; }
	// clang-format on

	CSEL_THROW( invalid_argument( "Unrecognised target_part" ) );

	// Superfluous, post-throw return statement to appease compilers
	return target_part::COMP_OR_TC0_OR_LOOP;
}

/// \brief Extract from the specified stream into the specified target_part
///
/// \param prm_is         The stream to extract from
/// \param prm_action_cat The target_part to extract into
istream &csel::operator>>( istream &prm_is, target_part &prm_action_cat ) {
	string input_string;
	prm_is >> input_string;
	prm_action_cat = target_part_from_string( input_string );
	return prm_is;
}

/// \brief Generate a string describing the specified target_part
///
/// \param prm_part The target_part to describe
string csel::to_string( const target_part &prm_part ) {
	// clang-format off
	switch ( prm_part ) {
		case ( target_part::COMP_OR_TC0_OR_LOOP ) : { return "comp_or_tc0_or_loop" ; }
		case ( target_part::VALU_OR_TC1_OR_MYVL ) : { return "valu_or_tc1_or_myvl" ; }
	}
	// clang-format on
	CSEL_THROW( logic_error( "Unhandled target_part" ) );
	return "";
}

/// \brief Insert a description of the specified target_part into the specified ostream
///
/// \param prm_ostream The ostream into which the description should be inserted
/// \param prm_part    The target_part to describe
ostream &csel::operator<<( ostream &prm_ostream, const target_part &prm_part ) {
	prm_ostream << to_string( prm_part );
	return prm_ostream;
}

/// \brief Generate a string describing the specified action_meaning
///
/// \param prm_action_meaning The action_meaning to describe
string csel::to_string( const action_meaning &prm_action_meaning ) {
	// clang-format off
	switch ( prm_action_meaning ) {
		case ( action_meaning::APPLY_ACTION         ) : { return "apply-action"         ; }
		case ( action_meaning::SET_ACTION_TO        ) : { return "set-action-to"        ; }
		case ( action_meaning::META_SET_ACTION_ONCE ) : { return "meta-set-action-once" ; }
		case ( action_meaning::META_SET_ACTION_INF  ) : { return "meta-set-action-inf"  ; }
	}
	// clang-format on
	CSEL_THROW( logic_error( "Unhandled action_meaning" ) );
	return "";
}

/// \brief Insert a description of the specified action_meaning into the specified ostream
///
/// \param prm_ostream        The ostream into which the description should be inserted
/// \param prm_action_meaning The action_meaning to describe
ostream &csel::operator<<( ostream &prm_ostream, const action_meaning &prm_action_meaning ) {
	prm_ostream << to_string( prm_action_meaning );
	return prm_ostream;
}

target_part csel::generate_random_flag( mt19937 &prm_rng ) {
	return bernoulli_distribution{}( prm_rng ) ? target_part::COMP_OR_TC0_OR_LOOP : target_part::VALU_OR_TC1_OR_MYVL;
}

action_meaning csel::generate_random_action_meaning( [[maybe_unused]] mt19937 &prm_rng ) {
	return action_meaning::APPLY_ACTION;
	// const auto random_number = uniform_int_distribution{ 1, 23 }( prm_rng );
	// // clang-format off
	// return ( random_number <= 16 ) ? action_meaning::APPLY_ACTION         :
	//        ( random_number <= 20 ) ? action_meaning::SET_ACTION_TO        :
	//        ( random_number <= 22 ) ? action_meaning::META_SET_ACTION_ONCE :
	//                                  action_meaning::META_SET_ACTION_INF;
	// // clang-format on
}

uint16_t csel::generate_random_target_index( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	assert( prm_num_nodes > 0 );
	return uniform_int_distribution{ static_cast<uint16_t>( 0 ), static_cast<uint16_t>( prm_num_nodes - 1U ) }( prm_rng );
}

target csel::generate_random_target( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	return { generate_random_flag( prm_rng ),
		     generate_random_action_meaning( prm_rng ),
		     generate_random_target_index( prm_rng, prm_num_nodes ) };
}

string csel::to_string( const target &prm_target ) {
	return fmt::format( "target[target_part: {}, {:>20} {:>4}]",
	                    to_string( prm_target.part() ),
	                    to_string( prm_target.meaning() ),
	                    prm_target.node_index() );
}
