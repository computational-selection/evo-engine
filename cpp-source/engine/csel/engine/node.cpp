#include "node.hpp"

#include <ostream>

#include <fmt/core.h>

using namespace csel;

using ::std::mt19937;
using ::std::ostream;
using ::std::string;
using ::std::uniform_real_distribution; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981

/// Randomly generate a node
///
/// \param prm_rng       The random number engine to use
/// \param prm_num_nodes The number of nodes available as targets
node csel::generate_random_node( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	return node{ generate_random_input( prm_rng ),
		         cargo{ uniform_real_distribution{ -100.0F, 100.0F }( prm_rng ) },
		         comparator{ uniform_real_distribution{ -100.0F, 100.0F }( prm_rng ) },
		         generate_random_action( prm_rng, prm_num_nodes ),
		         generate_random_target( prm_rng, prm_num_nodes ) };
}

/// \brief Generate a string describing the specified node
///
/// \param prm_node The node to describe
string csel::to_string( const node &prm_node ) {
	return fmt::format( "node[input:{}, comp: {:7.2f}, {}, {}, value: {:7.2f}]",
	                    to_string( prm_node.the_input ),
	                    prm_node.the_comparator.value,
	                    to_string( prm_node.the_action ),
	                    to_string( prm_node.the_target ),
	                    prm_node.the_cargo.value() );
}

/// \brief Insert a description of the specified node into the specified ostream
///
/// \param prm_ostream The ostream into which the description should be inserted
/// \param prm_node    The node to describe
ostream &csel::operator<<( ostream &prm_ostream, const node &prm_node ) {
	prm_ostream << to_string( prm_node );
	return prm_ostream;
}
