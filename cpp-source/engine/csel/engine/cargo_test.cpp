#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include "csel/engine/cargo.hpp"
#include "csel/util/cereal/archive_pairs.hpp"
#include "csel/util/cereal/serialize_fns.hpp"

using namespace csel;
using namespace csel::util::cereal;

using ::cereal::BinaryOutputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLOutputArchive;

TEMPLATE_TEST_CASE( "cargo serialization round-trips", "[cargo]", BinaryOutputArchive, JSONOutputArchive, PortableBinaryOutputArchive, XMLOutputArchive ) {
	for ( const cargo &the_cargo : {
	        cargo{ 0.0 },
	        test_case_sub_0_cargo(),
	        test_case_sub_1_cargo(),
	      } ) {

		CHECK( serialise_round_trip<input_archive_of_archive_t<TestType>, output_archive_of_archive_t<TestType>>( the_cargo )
		       == the_cargo );
	}
}
