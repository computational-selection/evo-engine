#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_COMPARATOR_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_COMPARATOR_HPP

namespace csel {

	namespace detail {

		/// \brief Type alias for the value used inside a comparator
		using comparator_value_t = float;

	} // namespace detail

	/// A fixed value, against which the input is compared
	struct comparator {

		/// The value
		detail::comparator_value_t value;
	};

	static_assert( sizeof( comparator ) == 4 );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_COMPARATOR_HPP
