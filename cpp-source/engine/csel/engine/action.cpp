#include "action.hpp"

#include <ostream>
#include <stdexcept>

#include <fmt/core.h>

#include "csel/util/exception/csel_throw_exception.hpp"

using namespace csel;

using ::std::invalid_argument;
using ::std::istream;
using ::std::logic_error;
using ::std::mt19937;
using ::std::ostream;
using ::std::string;
using ::std::uniform_int_distribution; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981

/// \brief Extract from the specified stream into the specified action_cat
///
/// \param prm_is         The stream to extract from
/// \param prm_action_cat The action_cat to extract into
action_cat csel::action_cat_from_string( const string &prm_input_string ) {
	// clang-format off
	if ( prm_input_string == "ADD_MY_VALUE_TO"          ) { return action_cat::ADD_MY_VALUE_TO          ; }
	if ( prm_input_string == "SUBTR_MY_VALUE_FROM"      ) { return action_cat::SUBTR_MY_VALUE_FROM      ; }
	if ( prm_input_string == "MULTIPLY_MY_VALUE_TO"     ) { return action_cat::MULTIPLY_MY_VALUE_TO     ; }
	if ( prm_input_string == "DIVIDE_MY_VALUE_FROM"     ) { return action_cat::DIVIDE_MY_VALUE_FROM     ; }
	if ( prm_input_string == "WRITE_MY_VALUE_OVER"      ) { return action_cat::WRITE_MY_VALUE_OVER      ; }
	if ( prm_input_string == "CHANGE_INPUT_TO_TC"       ) { return action_cat::CHANGE_INPUT_TO_TC       ; }
	if ( prm_input_string == "CHANGE_INPUT_TO_XX"       ) { return action_cat::CHANGE_INPUT_TO_XX       ; }
	if ( prm_input_string == "CHANGE_TARGET_TO_OTHER"   ) { return action_cat::CHANGE_TARGET_TO_OTHER   ; }
	if ( prm_input_string == "ADD_OTHERS_VALUE_TO"      ) { return action_cat::ADD_OTHERS_VALUE_TO      ; }
	if ( prm_input_string == "SUBTR_OTHERS_VALUE_FROM"  ) { return action_cat::SUBTR_OTHERS_VALUE_FROM  ; }
	if ( prm_input_string == "MULTIPLY_OTHERS_VALUE_TO" ) { return action_cat::MULTIPLY_OTHERS_VALUE_TO ; }
	if ( prm_input_string == "DIVIDE_OTHERS_VALUE_FROM" ) { return action_cat::DIVIDE_OTHERS_VALUE_FROM ; }
	if ( prm_input_string == "WRITE_OTHERS_VALUE_OVER"  ) { return action_cat::WRITE_OTHERS_VALUE_OVER  ; }
	// clang-format on

	CSEL_THROW( invalid_argument( "Unrecognised action_cat" ) );

	// Superfluous, post-throw return statement to appease compilers
	return action_cat::ADD_MY_VALUE_TO;
}

/// \brief Extract from the specified stream into the specified action_cat
///
/// \param prm_is         The stream to extract from
/// \param prm_action_cat The action_cat to extract into
istream &csel::operator>>( istream &prm_is, action_cat &prm_action_cat ) {
	string input_string;
	prm_is >> input_string;
	prm_action_cat = action_cat_from_string( input_string );
	return prm_is;
}

/// \brief Generate a string describing the specified action_cat
///
/// \param prm_action_cat The action_cat to describe
string csel::to_string( const action_cat &prm_action_cat ) {
	// clang-format off
	switch ( prm_action_cat ) {

		case( action_cat::ADD_MY_VALUE_TO          ) : { return "ADD_MY_VALUE_TO"          ; }
		case( action_cat::SUBTR_MY_VALUE_FROM      ) : { return "SUBTR_MY_VALUE_FROM"      ; }
		case( action_cat::MULTIPLY_MY_VALUE_TO     ) : { return "MULTIPLY_MY_VALUE_TO"     ; }
		case( action_cat::DIVIDE_MY_VALUE_FROM     ) : { return "DIVIDE_MY_VALUE_FROM"     ; }
		case( action_cat::WRITE_MY_VALUE_OVER      ) : { return "WRITE_MY_VALUE_OVER"      ; }

		case( action_cat::CHANGE_INPUT_TO_TC       ) : { return "CHANGE_INPUT_TO_TC"       ; }
		case( action_cat::CHANGE_INPUT_TO_XX       ) : { return "CHANGE_INPUT_TO_XX"       ; }

		case( action_cat::CHANGE_TARGET_TO_OTHER   ) : { return "CHANGE_TARGET_TO_OTHER"   ; }

		case( action_cat::ADD_OTHERS_VALUE_TO      ) : { return "ADD_OTHERS_VALUE_TO"      ; }
		case( action_cat::SUBTR_OTHERS_VALUE_FROM  ) : { return "SUBTR_OTHERS_VALUE_FROM"  ; }
		case( action_cat::MULTIPLY_OTHERS_VALUE_TO ) : { return "MULTIPLY_OTHERS_VALUE_TO" ; }
		case( action_cat::DIVIDE_OTHERS_VALUE_FROM ) : { return "DIVIDE_OTHERS_VALUE_FROM" ; }
		case( action_cat::WRITE_OTHERS_VALUE_OVER  ) : { return "WRITE_OTHERS_VALUE_OVER"  ; }
	}
	// clang-format on
	CSEL_THROW( logic_error( "Unhandled action_cat" ) );
	return "";
}

/// \brief Insert a description of the specified action_cat into the specified ostream
///
/// \param prm_ostream    The ostream into which the description should be inserted
/// \param prm_action_cat The action_cat to describe
ostream &csel::operator<<( ostream &prm_ostream, const action_cat &prm_action_cat ) {
	prm_ostream << to_string( prm_action_cat );
	return prm_ostream;
}

/// Randomly generate an action_cat
///
/// \param prm_rng The random number engine to use
action_cat csel::generate_random_action_cat( mt19937 &prm_rng ) {
	return static_cast<action_cat>( uniform_int_distribution{ static_cast<uint16_t>( 0 ), MAX_ACTION_CAT_UNDERLYING }( prm_rng ) );
}

/// Generate a random other-node-index
///
/// \param prm_rng       The random number engine to use
/// \param prm_num_nodes The number of nodes available as targets
uint16_t csel::generate_random_other_node_idx( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	assert( prm_num_nodes > 0 );
	return uniform_int_distribution{ static_cast<uint16_t>( 0 ), static_cast<uint16_t>( prm_num_nodes - 1U ) }( prm_rng );
}

/// Generate a random action
///
/// \param prm_rng       The random number engine to use
/// \param prm_num_nodes The number of nodes available as targets
action csel::generate_random_action( mt19937 &prm_rng, const uint16_t &prm_num_nodes ) {
	return { generate_random_action_cat( prm_rng ), generate_random_other_node_idx( prm_rng, prm_num_nodes ) };
}

/// \brief Generate a string describing the specified action
///
/// \param prm_action The action to describe
string csel::to_string( const action &prm_action ) {
	using ::std::to_string;
	return fmt::format( "action[cat: {:>24}", to_string( prm_action.cat() ) )
	       + ( uses_other_node( prm_action.cat() ) ? fmt::format( ", other node: {:>4}]", prm_action.other_node_idx() )
	                                               : "                  ]" );
}

/// \brief Insert a description of the specified action into the specified ostream
///
/// \param prm_ostream The ostream into which the description should be inserted
/// \param prm_action  The action to describe
ostream &csel::operator<<( ostream &prm_ostream, const action &prm_action ) {
	prm_ostream << to_string( prm_action );
	return prm_ostream;
}
