#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_FITNESS_POINT_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_FITNESS_POINT_HPP

#include <nlohmann/json.hpp>

namespace csel {

	/// A fitness value at a specific generation (ie one point on a fitness-over-time curve)
	struct fitness_point {

		/// The generation at which the fitness occurred
		size_t generation = 0;

		/// The fitness value
		float fitness = 0.0F;

		/// Default ctor
		constexpr fitness_point() noexcept = default;

		/// Standard ctr
		///
		/// \TODO Come C++20, try removing this and allowing aggregate initialisation (P0960)
		///       because this is only here vector::emplace_back
		///
		/// \param prm_generation The generation at which the fitness occurred
		/// \param prm_fitness    The fitness value
		inline constexpr fitness_point( const size_t &prm_generation, const float &prm_fitness ) noexcept :
		        generation{ prm_generation }, fitness{ prm_fitness } {
		}

		/// Whether the two specified fitness_point values are equal
		///
		/// \param prm_lhs The first  fitness_point to compare
		/// \param prm_rhs The second fitness_point to compare
		friend constexpr bool operator==( const fitness_point &prm_lhs, const fitness_point &prm_rhs ) {
			// clang-format off
			return (
				prm_lhs.generation == prm_rhs.generation
				&&
				prm_lhs.fitness    == prm_rhs.fitness

			);
			// clang-format on
		}
	};

	void to_json( ::nlohmann::json &, const fitness_point & );

	void from_json( const ::nlohmann::json &, fitness_point & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_FITNESS_POINT_HPP
