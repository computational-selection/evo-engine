#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_NODE_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_NODE_HPP

#include <iosfwd>
#include <random>

#include <cereal/cereal.hpp>

#include "csel/engine/action.hpp"
#include "csel/engine/cargo.hpp"
#include "csel/engine/comparator.hpp"
#include "csel/engine/input.hpp"
#include "csel/engine/target.hpp"
#include "csel/util/cereal/traits.hpp"
#include "csel/util/cereal/unified_serializable.hpp"

namespace csel {

	struct node : private util::unified_serializable<node> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<node>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

	  public:
		input      the_input;
		cargo      the_cargo;
		comparator the_comparator;
		action     the_action;
		target     the_target;

		node( const input &, const cargo &, const comparator &, const action &, const target & );
	};
	inline constexpr size_t EXPECTED_SIZEOF_NODE = 16;
	static_assert( sizeof( node ) == EXPECTED_SIZEOF_NODE );

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The node to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void node::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		// clang-format off
		prm_archive(
			::cereal::make_nvp( "input",      prm_serializee.the_input            ),
			::cereal::make_nvp( "cargo",      prm_serializee.the_cargo            ),
			::cereal::make_nvp( "comparator", prm_serializee.the_comparator.value ),
			::cereal::make_nvp( "action",     prm_serializee.the_action           ),
			::cereal::make_nvp( "target",     prm_serializee.the_target           )
		);
		// clang-format on
	}

	inline node::node( const input &     prm_input,
	                   const cargo &     prm_cargo,
	                   const comparator &prm_comparator,
	                   const action &    prm_action,
	                   const target &    prm_target ) :
	        the_input{ prm_input }, the_cargo{ prm_cargo }, the_comparator{ prm_comparator }, the_action{ prm_action }, the_target{ prm_target } {
	}

	node generate_random_node( ::std::mt19937 &, const ::std::uint16_t & );

	::std::string to_string( const node & );

	::std::ostream &operator<<( ::std::ostream &, const node & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_NODE_HPP
