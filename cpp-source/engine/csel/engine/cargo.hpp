#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_CARGO_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_CARGO_HPP

#include <cmath>

#include "csel/util/cereal/serialize_fns.hpp"
#include "csel/util/cereal/traits.hpp"
#include "csel/util/cereal/unified_serializable.hpp"

namespace csel {

	namespace detail {

		/// \brief Type alias for the value used inside a cargo
		using cargo_value_t = float;

		/// A constant for the cargo value that means "initialise to testcase_0"
		constexpr cargo_value_t test_case_sub_0_cargo_value = ::std::numeric_limits<detail::cargo_value_t>::infinity();

		/// A constant for the cargo value that means "initialise to testcase_1"
		constexpr cargo_value_t test_case_sub_1_cargo_value = -::std::numeric_limits<detail::cargo_value_t>::infinity();

	} // namespace detail

	/// The value in the body of the a node that can be modified by other nodes
	///
	/// This can be initialised to a constant or to the value of either testcase
	struct cargo : private util::unified_serializable<cargo> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<cargo>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

		detail::cargo_value_t m_value = 0.0F;

	  public:
		constexpr cargo() noexcept = default;
		constexpr explicit cargo( const detail::cargo_value_t & );

		[[nodiscard]] constexpr const detail::cargo_value_t &value() const;

		constexpr detail::cargo_value_t &value();

		// constexpr cargo & set_value( const detail::cargo_value_t & );

		/// \brief Whether the two specified cargo values are equal
		///
		/// \brief The first  cargo to compare
		/// \brief The second cargo to compare
		friend constexpr bool operator==( const cargo &prm_lhs, const cargo &prm_rhs ) {
			return prm_lhs.value() == prm_rhs.value();
		}
	};

	static_assert( sizeof( cargo ) == 4 );

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The cargo to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void cargo::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		if constexpr ( !util::cereal::is_text_archive_v<Archive> ) {
			if constexpr ( dirn == util::cereal_dirn::SAVING ) {
				prm_archive( prm_serializee.value() );
			} else {
				prm_serializee.value() = util::cereal::deserialize_value_to<detail::cargo_value_t>( prm_archive );
			}
		} else {
			constexpr auto testcase_0_name = "testcase_0";
			constexpr auto testcase_1_name = "testcase_1";

			if constexpr ( dirn == util::cereal_dirn::SAVING ) {
				constexpr auto type_name   = "type";
				constexpr auto value_name  = "value";
				constexpr auto normal_name = "normal";

				// clang-format off
				const ::std::string type_str = is_testcase_sub_0( prm_serializee ) ? testcase_0_name :
				                               is_testcase_sub_1( prm_serializee ) ? testcase_1_name :
				                                                                     normal_name ;
				// clang-format on
				prm_archive( ::cereal::make_nvp( type_name, type_str ) );

				if ( is_normal_cargo( prm_serializee ) ) {
					prm_archive( ::cereal::make_nvp( value_name, prm_serializee.value() ) );
				}
			} else {
				const ::std::string type_str = util::cereal::deserialize_value_to<::std::string>( prm_archive );
				if ( type_str == testcase_0_name ) {
					prm_serializee.value() = detail::test_case_sub_0_cargo_value;
				} else if ( type_str == testcase_1_name ) {
					prm_serializee.value() = detail::test_case_sub_1_cargo_value;
				} else {
					prm_serializee.value() = util::cereal::deserialize_value_to<detail::cargo_value_t>( prm_archive );
				}
			}
		}
	}

	/// Ctor
	///
	/// \param prm_value The value from which the cargo should be constructed
	constexpr cargo::cargo( const detail::cargo_value_t &prm_value ) : m_value{ prm_value } {
	}

	/// Const Getter for the value
	constexpr const detail::cargo_value_t &cargo::value() const {
		return m_value;
	}

	/// Non-const Setter for the value
	///
	/// \param prm_value The value to set
	constexpr detail::cargo_value_t &cargo::value() {
		return m_value;
	}

	// /// Setter for the value
	// ///
	// /// \param prm_value The value to set
	// constexpr cargo &cargo::set_value( const detail::cargo_value_t &prm_value ) {
	// 	m_value = prm_value;
	// 	return *this;
	// }

	/// A cargo that will be initialised to the first  component of the testcase
	constexpr cargo test_case_sub_0_cargo() {
		return cargo{ detail::test_case_sub_0_cargo_value };
	}

	/// A cargo that will be initialised to the second component of the testcase
	constexpr cargo test_case_sub_1_cargo() {
		return cargo{ detail::test_case_sub_1_cargo_value };
	}

	inline bool is_testcase_sub_0( const cargo &x ) {
		return !::std::isfinite( x.value() ) && !::std::isnan( x.value() ) && !::std::signbit( x.value() );
	}

	inline bool is_testcase_sub_1( const cargo &x ) {
		return !::std::isfinite( x.value() ) && !::std::isnan( x.value() ) && ::std::signbit( x.value() );
	}

	inline bool is_normal_cargo( const cargo &x ) {
		return ::std::isfinite( x.value() );
	}

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_CARGO_HPP
