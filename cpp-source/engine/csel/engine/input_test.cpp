#include <catch2/catch.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>

#include "csel/engine/input.hpp"
#include "csel/util/cereal/archive_pairs.hpp"
#include "csel/util/cereal/serialize_fns.hpp"

using namespace csel;
using namespace csel::util::cereal;

using ::cereal::BinaryOutputArchive;
using ::cereal::JSONOutputArchive;
using ::cereal::PortableBinaryOutputArchive;
using ::cereal::XMLOutputArchive;

TEST_CASE( "input basic" ) {
	// clang-format off
	CHECK( to_string( loop_ctr_input       () ) == "     loop" );
	CHECK( to_string( normal_eg1           () ) == "    0.000" );
	CHECK( to_string( normal_eg2           () ) == "    1.000" );
	CHECK( to_string( test_case_sub_0_input() ) == "testcase0" );
	CHECK( to_string( test_case_sub_1_input() ) == "testcase1" );
	// clang-format on

	// clang-format off
	CHECK(   is_loop_ctr       ( loop_ctr_input        () ) );
	CHECK( ! is_normal_input   ( loop_ctr_input        () ) );
	CHECK( ! is_testcase_sub_0 ( loop_ctr_input        () ) );
	CHECK( ! is_testcase_sub_1 ( loop_ctr_input        () ) );

	CHECK( ! is_loop_ctr       ( normal_eg1            () ) );
	CHECK(   is_normal_input   ( normal_eg1            () ) );
	CHECK( ! is_testcase_sub_0 ( normal_eg1            () ) );
	CHECK( ! is_testcase_sub_1 ( normal_eg1            () ) );

	CHECK( ! is_loop_ctr       ( normal_eg2            () ) );
	CHECK(   is_normal_input   ( normal_eg2            () ) );
	CHECK( ! is_testcase_sub_0 ( normal_eg2            () ) );
	CHECK( ! is_testcase_sub_1 ( normal_eg2            () ) );

	CHECK( ! is_loop_ctr       ( test_case_sub_0_input () ) );
	CHECK( ! is_normal_input   ( test_case_sub_0_input () ) );
	CHECK(   is_testcase_sub_0 ( test_case_sub_0_input () ) );
	CHECK( ! is_testcase_sub_1 ( test_case_sub_0_input () ) );

	CHECK( ! is_loop_ctr       ( test_case_sub_1_input () ) );
	CHECK( ! is_normal_input   ( test_case_sub_1_input () ) );
	CHECK( ! is_testcase_sub_0 ( test_case_sub_1_input () ) );
	CHECK(   is_testcase_sub_1 ( test_case_sub_1_input () ) );
	// clang-format on
}

TEMPLATE_TEST_CASE( "input serialization round-trips", "[input]", BinaryOutputArchive, JSONOutputArchive, PortableBinaryOutputArchive, XMLOutputArchive ) {
	for ( const input &the_input : {
	        loop_ctr_input(),
	        normal_eg1(),
	        normal_eg2(),
	        test_case_sub_0_input(),
	        test_case_sub_1_input(),
	      } ) {
		CHECK( serialise_round_trip<input_archive_of_archive_t<TestType>, output_archive_of_archive_t<TestType>>( the_input )
		       == the_input );
	}
}
