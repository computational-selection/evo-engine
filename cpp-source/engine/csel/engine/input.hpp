#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INPUT_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INPUT_HPP

#include <cmath>
#include <iosfwd>
#include <limits>
#include <random>
#include <string>

#include <cereal/cereal.hpp>

#include "csel/util/cereal/serialize_fns.hpp"
#include "csel/util/cereal/traits.hpp"
#include "csel/util/cereal/unified_serializable.hpp"

namespace csel {

	namespace detail {

		/// \brief Type alias for the value used inside an input
		using input_value_t = float;

		/// \brief The special-case sentinel value representing a
		constexpr input_value_t loop_ctr_input_value = ::std::numeric_limits<detail::input_value_t>::signaling_NaN();

		/// \brief The special-case sentinel value representing a
		constexpr input_value_t test_case_sub_0_input_value = ::std::numeric_limits<detail::input_value_t>::infinity();

		/// \brief The special-case sentinel value representing a
		constexpr input_value_t test_case_sub_1_input_value = -::std::numeric_limits<detail::input_value_t>::infinity();

	} // namespace detail

	/// Some sort of input which is compared against the comparator to determine whether the node should execute
	///
	/// This can be set to a value or a reference to the loop counter or either testcase.
	/// The standard operations don't modify the value or repoint the reference.
	struct input : private util::unified_serializable<input> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<input>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

	  public:
		detail::input_value_t value = 0.0F;

		constexpr input() noexcept = default;
		constexpr explicit input( const detail::input_value_t & );

		/// Whether the two specified input values are equal
		///
		/// \param prm_lhs The first  input to compare
		/// \param prm_rhs The second input to compare
		friend constexpr bool operator==( const input &prm_lhs, const input &prm_rhs ) {
			return ( prm_lhs.value == prm_rhs.value || ( ::std::isnan( prm_lhs.value ) && ::std::isnan( prm_rhs.value ) ) );
		}
	};

	static_assert( sizeof( input ) == 4 );

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The input to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void input::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		if constexpr ( !util::cereal::is_text_archive_v<Archive> ) {
			prm_archive( prm_serializee.value );
		} else {
			constexpr auto loop_ctr_name   = "loop_ctr";
			constexpr auto testcase_0_name = "testcase_0";
			constexpr auto testcase_1_name = "testcase_1";

			if constexpr ( dirn == util::cereal_dirn::SAVING ) {
				constexpr auto type_name   = "type";
				constexpr auto value_name  = "value";
				constexpr auto normal_name = "normal";

				// clang-format off
				const ::std::string type_str = is_loop_ctr      ( prm_serializee ) ? loop_ctr_name   :
				                               is_testcase_sub_0( prm_serializee ) ? testcase_0_name :
				                               is_testcase_sub_1( prm_serializee ) ? testcase_1_name :
				                                                                     normal_name ;
				// clang-format on
				prm_archive( ::cereal::make_nvp( type_name, type_str ) );

				if ( is_normal_input( prm_serializee ) ) {
					prm_archive( ::cereal::make_nvp( value_name, prm_serializee.value ) );
				}
			} else {
				const ::std::string type_str = util::cereal::deserialize_value_to<::std::string>( prm_archive );
				if ( type_str == loop_ctr_name ) {
					prm_serializee.value = detail::loop_ctr_input_value;
				} else if ( type_str == testcase_0_name ) {
					prm_serializee.value = detail::test_case_sub_0_input_value;
				} else if ( type_str == testcase_1_name ) {
					prm_serializee.value = detail::test_case_sub_1_input_value;
				} else {
					prm_serializee.value = util::cereal::deserialize_value_to<detail::input_value_t>( prm_archive );
				}
			}
		}
	}

	constexpr input::input( const detail::input_value_t &prm_value ) : value{ prm_value } {
	}

	constexpr input loop_ctr_input() {
		return input{ detail::loop_ctr_input_value };
	}

	constexpr input normal_eg1() {
		return input{ 0.0F };
	}

	constexpr input normal_eg2() {
		return input{ 1.0F };
	}

	constexpr input test_case_sub_0_input() {
		return input{ detail::test_case_sub_0_input_value };
	}

	constexpr input test_case_sub_1_input() {
		return input{ detail::test_case_sub_1_input_value };
	}

	inline bool is_loop_ctr( const input &x ) {
		return ::std::isnan( x.value );
	}

	inline bool is_normal_input( const input &x ) {
		return ::std::isfinite( x.value );
	}

	inline bool is_testcase_sub_0( const input &x ) {
		return !::std::isfinite( x.value ) && !::std::isnan( x.value ) && !::std::signbit( x.value );
	}

	inline bool is_testcase_sub_1( const input &x ) {
		return !::std::isfinite( x.value ) && !::std::isnan( x.value ) && ::std::signbit( x.value );
	}

	input generate_random_input( ::std::mt19937 & );

	void mutate_input( input &, ::std::mt19937 & );

	::std::string to_string( const input & );

	::std::ostream &operator<<( ::std::ostream &, const input & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INPUT_HPP
