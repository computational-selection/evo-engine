#ifndef _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INDIVIDUAL_HPP
#define _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INDIVIDUAL_HPP

#include <array>
#include <iosfwd>
#include <random>
#include <string>
#include <type_traits>
#include <vector>

#include <cereal/types/vector.hpp>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/view/indices.hpp>
#include <range/v3/view/transform.hpp>

#include "csel/engine/node.hpp"
#include "csel/util/exception/csel_throw_exception.hpp"
#include "csel/util/type_traits.hpp"

namespace csel {

	/// Whether to permit or forbid infinities
	enum class float_mutn_policy : bool {
		PERMIT_INFINITIES, ///< Permit infinities
		FORBID_INFINITIES  ///< Forbid infinities
	};

	/// An individual
	class individual : private util::unified_serializable<individual> {
	  private:
		friend ::cereal::access;
		friend util::unified_serializable<individual>;

		template <util::cereal_dirn dirn, typename Serializee, typename Archive>
		static void unified_serialize( Serializee &&, Archive &&, const ::std::uint32_t & );

		/// The nodes making up the individual
		::std::vector<node> nodes;

	  public:
		individual &move_node_from_to( const size_t &, const size_t & );

		/// const_iterator type alias as part of making this a range
		using const_iterator = ::std::vector<node>::const_iterator;

		explicit individual( ::std::vector<node> );

		individual &mutate( ::std::mt19937 & );

		[[nodiscard]] bool   empty() const;
		[[nodiscard]] size_t size() const;

		[[nodiscard]] const_iterator begin() const;
		[[nodiscard]] const_iterator end() const;
	};

	/// Serialize
	///
	/// This code is used for loading and saving, indicated by dirn
	///
	/// \param prm_serializee The individual to be serialized (ref if loading; const-ref if saving)
	/// \param prm_archive    The archive from/to which the data should be serialized
	/// \param prm_version    The version of the serialization format
	template <util::cereal_dirn dirn, typename Serializee, typename Archive>
	void individual::unified_serialize( Serializee &&prm_serializee, Archive &&prm_archive, const ::std::uint32_t & /* prm_version */ ) {
		prm_archive( ::cereal::make_nvp( "nodes", prm_serializee.nodes ) );
	}

	/// Move the node at the specified index to the specified index
	/// and update internal references accordingly
	///
	/// TODOCUMENT: Full precision about what prm_to means and how this works
	///
	/// \param prm_from The index of the node to move
	/// \param prm_to   The index to which the node should be moved
	inline individual &individual::move_node_from_to( const size_t &prm_from, const size_t &prm_to ) {
		const bool rotate_is_right = ( prm_to < prm_from );

		if ( rotate_is_right ) {
			::std::rotate( ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_to ) ),
			               ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_from ) ),
			               ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_from + 1 ) ) );
		} else {
			::std::rotate( ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_from ) ),
			               ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_from + 1 ) ),
			               ::std::next( ::std::begin( nodes ), static_cast<ptrdiff_t>( prm_to + 1 ) ) );
		}

		for ( node &the_node : nodes ) {
			const ::std::uint16_t old_target = the_node.the_target.node_index();
			if ( old_target >= ::std::min( prm_from, prm_to ) && old_target <= ::std::max( prm_from, prm_to ) ) {
				const ::std::uint16_t new_target = ( old_target == prm_from )
				                                     ? static_cast<::std::uint16_t>( prm_to )
				                                     : rotate_is_right ? static_cast<::std::uint16_t>( old_target + 1U )
				                                                       : static_cast<::std::uint16_t>( old_target - 1U );
				the_node.the_target.update_node_idx( new_target );
			}

			if ( has_other_node_index( the_node.the_action ) ) {
				const ::std::uint16_t other_node_idx = the_node.the_action.other_node_idx();
				if ( other_node_idx >= ::std::min( prm_from, prm_to ) && other_node_idx <= ::std::max( prm_from, prm_to ) ) {
					const ::std::uint16_t new_other_node_idx = ( other_node_idx == prm_from )
					                                             ? static_cast<::std::uint16_t>( prm_to )
					                                             : rotate_is_right
					                                                 ? static_cast<::std::uint16_t>( other_node_idx + 1U )
					                                                 : static_cast<::std::uint16_t>( other_node_idx - 1U );
					the_node.the_action.update_other_node_idx( new_other_node_idx );
				}
			}
		}

		return *this;
	}

	/// Ctor
	///
	/// \param prm_nodes The nodes from which the individual should be formed
	inline individual::individual( ::std::vector<node> prm_nodes ) : nodes{ ::std::move( prm_nodes ) } {
	}

	/// Randomly mutate the specified float
	///
	/// \param prm_float             The float to mutate
	/// \param prm_rng               The random number engine to use
	/// \param prm_float_mutn_policy Whether to permit infinities
	inline void mutate_float( float &                  prm_float,
	                          ::std::mt19937 &         prm_rng,
	                          const float_mutn_policy &prm_float_mutn_policy = float_mutn_policy::FORBID_INFINITIES ) {
		constexpr float RESET_DISTN_MIN = -100.0F;
		constexpr float RESET_DISTN_MAX = -100.0F;
		constexpr float MULT_DISTN_MIN  = 0.995F;
		constexpr float MULT_DISTN_MAX  = 1.005F;
		if ( ::std::bernoulli_distribution{}( prm_rng ) ) {
			if ( prm_float_mutn_policy == float_mutn_policy::FORBID_INFINITIES || ::std::bernoulli_distribution{}( prm_rng ) ) {
				prm_float = ::std::uniform_real_distribution{ RESET_DISTN_MIN, RESET_DISTN_MAX }( prm_rng );
			} else {
				prm_float = ::std::bernoulli_distribution{}( prm_rng ) ? std::numeric_limits<float>::infinity()
				                                                       : -std::numeric_limits<float>::infinity();
			}
		} else {
			prm_float *= ::std::uniform_real_distribution{ MULT_DISTN_MIN, MULT_DISTN_MAX }( prm_rng );
		}
	}

	/// Mutate this individual
	///
	/// \param prm_rng The random number engine to use
	inline individual &individual::mutate( ::std::mt19937 &prm_rng ) {
		Expects( size() >= 2UL );
		constexpr auto NUM_MUTN_OPS = 5UL;
		const size_t   node_index   = ::std::uniform_int_distribution{ 0UL, size() - 1UL }( prm_rng );
		switch ( ::std::uniform_int_distribution{ 1UL, NUM_MUTN_OPS }( prm_rng ) ) {
			case ( 1UL ): {
				nodes[ node_index ] = generate_random_node( prm_rng, static_cast<::std::uint16_t>( size() ) );
				break;
			}
			case ( 2UL ): {
				mutate_float( nodes[ node_index ].the_cargo.value(), prm_rng, float_mutn_policy::PERMIT_INFINITIES );
				break;
			}
			case ( 3UL ): {
				mutate_float( nodes[ node_index ].the_comparator.value, prm_rng );
				break;
			}
			case ( 4UL ): {
				mutate_input( nodes[ node_index ].the_input, prm_rng );
				break;
			}
			case ( NUM_MUTN_OPS ): {
				const size_t dest_node = ::std::uniform_int_distribution{ 0UL, size() - 2UL }( prm_rng );
				move_node_from_to( node_index, ( dest_node < node_index ) ? dest_node : dest_node + 1 );
				break;
			}
			default: {
				CSEL_THROW( ::std::logic_error( "oh dear - you should not have come here" ) );
			}
		}
		return *this;
	}

	/// Whether this individual is empty (has no nodes)
	inline bool individual::empty() const {
		return nodes.empty();
	}

	/// The number of nodes in this individual
	inline size_t individual::size() const {
		return nodes.size();
	}

	/// A const_iterator pointing before the first
	inline auto individual::begin() const -> const_iterator {
		return ::std::cbegin( nodes );
	}

	/// A const_iterator pointing after the last node
	inline auto individual::end() const -> const_iterator {
		return ::std::cend( nodes );
	}

	namespace detail {

		/// An array with the cargo values from the specified nodes at the nodes of the specified index_sequence
		///
		/// This is a helper to get an array containing the values of the first N nodes, where N is a NTTP
		///
		/// \TODO Come C++23, this has hopefully been obviated by Barry Revzin's P1858
		template <size_t... Indices>
		auto get_first_n_cargo_values( const ::std::vector<node> &prm_eval_nodes, ::std::index_sequence<Indices...> ) {
			return ::std::array{ prm_eval_nodes[ Indices ].the_cargo.value()... };
		}

		/// The sum of errors (all negative) between got and wanted
		///
		/// \param prm_eval     The data to assess
		/// \param prm_testcase The testcase
		template <size_t... Indices, typename EvalT, typename TestcaseT>
		float error_sum( ::std::index_sequence<Indices...>, const EvalT &prm_eval, const TestcaseT &prm_testcase ) {
			return ( 0.0F - ... - ::std::abs( ::std::get<Indices>( prm_eval ) - ::std::get<Indices + 2>( prm_testcase ) ) );
		}

	} // namespace detail

	/// The result of evaluating the specified nodes for the specified testcase for the specified number of iterations
	///
	/// \param prm_eval_nodes     The nodes to evaluate
	/// \param prm_testcase_0     The first  dimension of the testcase
	/// \param prm_testcase_1     The second dimension of the testcase
	/// \param prm_num_iterations The number of iterations
	template <size_t NumOutputs>
	inline auto evaluate_nodes( ::std::vector<node> &prm_eval_nodes,
	                            const float &        prm_testcase_0,
	                            const float &        prm_testcase_1,
	                            const size_t &       prm_num_iterations ) {
		// Initialise cargo nodes that specify a testcase with the testcase value
		for ( const size_t &node_ctr : ::ranges::views::indices( prm_eval_nodes.size() ) ) {
			node &the_node = prm_eval_nodes[ node_ctr ];
			if ( is_testcase_sub_0( the_node.the_cargo ) ) {
				the_node.the_cargo.value() = prm_testcase_0;
			}
			if ( is_testcase_sub_1( the_node.the_cargo ) ) {
				the_node.the_cargo.value() = prm_testcase_1;
			}
		}
		for ( const size_t &iter_ctr : ::ranges::views::indices( prm_num_iterations ) ) {
			const auto loop_ctr_as_data = static_cast<float>( iter_ctr );

			for ( const size_t &node_ctr : ::ranges::views::indices( prm_eval_nodes.size() ) ) {
				const node & the_node  = prm_eval_nodes[ node_ctr ];
				const float &the_value = the_node.the_cargo.value();
				// clang-format off
				const float &the_input = is_loop_ctr      ( the_node.the_input ) ? loop_ctr_as_data :
				                         is_testcase_sub_0( the_node.the_input ) ? prm_testcase_0   :
				                         is_testcase_sub_1( the_node.the_input ) ? prm_testcase_1   :
				                                                                //    the_node.the_cargo.value; /// WOAH, isn't something wrong here??
				                                                                   the_node.the_input.value; /// WOAH, isn't something wrong here??
				// clang-format on
				if ( the_node.the_comparator.value < the_input ) {
					continue;
				}

				const action_meaning the_meaning = the_node.the_target.meaning();
				if ( the_meaning != action_meaning::APPLY_ACTION ) {
					CSEL_THROW( ::std::logic_error( "cannot handle that meaning yet" ) );
				}
				node &dest_node = prm_eval_nodes[ the_node.the_target.node_index() ];

				float &dest_slot = ( the_node.the_target.part() == target_part::COMP_OR_TC0_OR_LOOP )
				                     ? dest_node.the_comparator.value
				                     : dest_node.the_cargo.value();

				const action_cat the_action_cat = the_node.the_action.cat();

				switch ( the_action_cat ) {

					case ( action_cat::ADD_MY_VALUE_TO ): {
						dest_slot += the_value;
						break;
					}
					case ( action_cat::SUBTR_MY_VALUE_FROM ): {
						dest_slot -= the_value;
						break;
					}
					case ( action_cat::MULTIPLY_MY_VALUE_TO ): {
						dest_slot *= the_value;
						break;
					}
					case ( action_cat::DIVIDE_MY_VALUE_FROM ): {
						dest_slot /= the_value;
						if ( !::std::isfinite( dest_slot ) ) {
							dest_slot = 0.0;
						};
						break;
					}
					case ( action_cat::WRITE_MY_VALUE_OVER ): {
						dest_slot += the_value;
						break;
					}
					case ( action_cat::CHANGE_INPUT_TO_TC ): {
						dest_node.the_input = ( the_node.the_target.part() == target_part::COMP_OR_TC0_OR_LOOP )
						                        ? test_case_sub_0_input()
						                        : test_case_sub_1_input();
						break;
					}
					case ( action_cat::CHANGE_INPUT_TO_XX ): {
						dest_node.the_input = ( the_node.the_target.part() == target_part::COMP_OR_TC0_OR_LOOP )
						                        ? loop_ctr_input()
						                        : input{ the_value };
						break;
					}
					case ( action_cat::CHANGE_TARGET_TO_OTHER ): {
						dest_node.the_target = target( the_node.the_target.part(),
						                               dest_node.the_target.meaning(),
						                               the_node.the_action.other_node_idx() );
						break;
					}
					case ( action_cat::ADD_OTHERS_VALUE_TO ): {
						dest_slot += prm_eval_nodes[ the_node.the_action.other_node_idx() ].the_cargo.value();
						break;
					}
					case ( action_cat::SUBTR_OTHERS_VALUE_FROM ): {
						dest_slot -= prm_eval_nodes[ the_node.the_action.other_node_idx() ].the_cargo.value();
						break;
					}
					case ( action_cat::MULTIPLY_OTHERS_VALUE_TO ): {
						dest_slot *= prm_eval_nodes[ the_node.the_action.other_node_idx() ].the_cargo.value();
						break;
					}
					case ( action_cat::DIVIDE_OTHERS_VALUE_FROM ): {
						dest_slot /= prm_eval_nodes[ the_node.the_action.other_node_idx() ].the_cargo.value();
						if ( !::std::isfinite( dest_slot ) ) {
							dest_slot = 0.0;
						};
						break;
					}
					case ( action_cat::WRITE_OTHERS_VALUE_OVER ): {
						dest_slot = prm_eval_nodes[ the_node.the_action.other_node_idx() ].the_cargo.value();
						break;
					}
					default: {
						CSEL_THROW( ::std::logic_error( "cannot handle action " + to_string( the_action_cat ) + " yet" ) );
					}
				}
				// const ::std::uint16_t x = the_node.the_action.other_node_idx();
			}
		}
		return detail::get_first_n_cargo_values( prm_eval_nodes, ::std::make_index_sequence<NumOutputs>{} );
	}

	/// The output of evaluating the specified input over the specified data for the specified number of iterations
	///
	/// TODO: Why is this using plain_output and returning cargo rather than get_first_n_cargo_values
	///       and returning floats?
	///
	/// \param prm_eval_nodes     The nodes to evaluate
	/// \param prm_individual     The individual to evaluate
	/// \param prm_data           Each input datapoint
	/// \param prm_num_iterations The number of iterations to perform within each evaluation
	template <size_t NumOutputDims, typename DataRng>
	inline ::std::vector<::std::array<float, NumOutputDims>> evaluation_output( ::std::vector<node> &prm_eval_nodes,
	                                                                            const individual &   prm_individual,
	                                                                            const DataRng &      prm_input_data,
	                                                                            const size_t &prm_num_iterations ) {
		// This should be OK: default execution policy is seq; op may have sensible side effects since C++11.
		return ::ranges::views::transform(
		         prm_input_data,
		         [&]( const auto &testcase ) {
			         constexpr size_t num_input_dims = ::std::tuple_size_v<remove_cvref_t<decltype( testcase )>>;
			         static_assert( num_input_dims == 2, "evaluation_output() requires inputs with 2 dimensions" );

			         prm_eval_nodes.assign( ::std::cbegin( prm_individual ), ::std::cend( prm_individual ) );
			         return evaluate_nodes<NumOutputDims>(
			           prm_eval_nodes, ::std::get<0>( testcase ), ::std::get<1>( testcase ), prm_num_iterations );
		         } )
		       | ::ranges::to_vector;
	}

	/// The output of evaluating the specified input over the specified data for the specified number of iterations
	///
	/// TODO: Why is this using plain_output and returning cargo rather than get_first_n_cargo_values
	///       and returning floats?
	///
	/// \param prm_eval_nodes     The nodes to evaluate
	/// \param prm_individual     The individual to evaluate
	/// \param prm_data           Each input datapoint
	/// \param prm_num_iterations The number of iterations to perform within each evaluation
	template <size_t NumOutputDims, typename DataRng>
	inline ::std::vector<::std::array<float, NumOutputDims>> evaluation_output( const individual &prm_individual,
	                                                                            const DataRng &   prm_input_data,
	                                                                            const size_t &    prm_num_iterations ) {
		::std::vector<node> nodes;
		return evaluation_output<NumOutputDims>( nodes, prm_individual, prm_input_data, prm_num_iterations );
	}

	/// Calculate the fitness of the specified individual evaluated for the specified number of iterations over the specified testcases
	///
	/// \param prm_eval_nodes     Nodes to re-use over the multiple evaluations
	/// \param prm_individual     The individual to evaluate
	/// \param prm_data           Each datapoint, which contains 2 dimensions of input with some further dimensions of "correct" output
	/// \param prm_num_iterations The number of iterations to perform within each evaluation
	template <typename DataRng>
	inline float fitness( ::std::vector<node> &prm_eval_nodes,
	                      const individual &   prm_individual,
	                      const DataRng &      prm_data,
	                      const size_t &       prm_num_iterations ) {
		float error = 0.0;
		for ( const auto &testcase : prm_data ) {
			prm_eval_nodes.assign( ::std::cbegin( prm_individual ), ::std::cend( prm_individual ) );
			constexpr size_t num_output_dims = ::std::tuple_size_v<remove_cvref_t<decltype( testcase )>> - 2;

			const auto evaluation_result = evaluate_nodes<num_output_dims>(
			  prm_eval_nodes, ::std::get<0>( testcase ), ::std::get<1>( testcase ), prm_num_iterations );

			error += detail::error_sum( ::std::make_index_sequence<num_output_dims>{}, evaluation_result, testcase );
		}
		return error;
	}

	/// Calculate the fitness of the specified individual evaluated for the specified number of iterations over the specified testcases
	///
	/// \param prm_individual     The individual to evaluate
	/// \param prm_data           Each datapoint, which contains 2 dimensions of input with some further dimensions of "correct" output
	/// \param prm_num_iterations The number of iterations to perform within each evaluation
	template <typename DataRng>
	inline float fitness( const individual &prm_individual, const DataRng &prm_data, const size_t &prm_num_iterations ) {
		::std::vector<node> nodes;
		return fitness( nodes, prm_individual, prm_data, prm_num_iterations );
	}

	individual generate_random_individual_with_n_nodes( ::std::mt19937 &, const ::std::uint16_t & );

	::std::vector<node> generate_n_random_nodes( ::std::mt19937 &, const ::std::uint16_t & );

	::std::string to_string( const individual & );

	::std::ostream &operator<<( ::std::ostream &, const individual & );

} // namespace csel

#endif // _EVO_ENGINE_CPP_SOURCE_ENGINE_CSEL_ENGINE_INDIVIDUAL_HPP
