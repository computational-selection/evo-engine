#include <catch2/catch.hpp>

#include <filesystem>
#include <limits>

#include "csel/problem/picture/bitmap.hpp"

using ::std::array; // NOLINT(misc-unused-using-decls) using declaration is used but this check is currently confused by CTAD, see https://bugs.llvm.org/show_bug.cgi?id=38981
using ::std::filesystem::path;

using namespace ::csel;
using namespace ::csel::prob::pict;

using ::std::numeric_limits;

/// A constant for the maximum possible value of an unsigned char expressed as a color_value::data_type
inline static constexpr color_value::data_type MAX_CHAR_VALUE =
  static_cast<color_value::data_type>( numeric_limits<unsigned char>::max() );

/// Get a color_value from three unsigned char components
///
/// \param prm_r The red component
/// \param prm_g The green component
/// \param prm_b The blue component
static constexpr color_value color_value_from_chars( const unsigned char &prm_r,
                                                     const unsigned char &prm_g,
                                                     const unsigned char &prm_b ) {
	return { static_cast<color_value::data_type>( prm_r ) / MAX_CHAR_VALUE,
		     static_cast<color_value::data_type>( prm_g ) / MAX_CHAR_VALUE,
		     static_cast<color_value::data_type>( prm_b ) / MAX_CHAR_VALUE };
}

// clang-format off
constexpr array image_pixels = {
	color_value_from_chars( 188, 188, 188 ),
	color_value_from_chars(  75,  73,  74 ),
	color_value_from_chars( 255, 254, 255 ),
	color_value_from_chars( 195, 195, 195 ),

	color_value_from_chars( 107, 107, 107 ),
	color_value_from_chars(  82,  82,  82 ),
	color_value_from_chars( 255,   1,   1 ),
	color_value_from_chars( 194, 194, 192 ),

	color_value_from_chars(  60,  60,  60 ),
	color_value_from_chars(  42,  42,  42 ),
	color_value_from_chars( 146, 146, 146 ),
	color_value_from_chars( 158, 158, 158 ),

	color_value_from_chars( 144, 144, 144 ),
	color_value_from_chars(   0,   0,   0 ),
	color_value_from_chars(  95,  95,  95 ),
	color_value_from_chars(  85,  85,  85 )
};
// clang-format on

TEST_CASE( "read_from_png works" ) {
	const path source_image_file{ "/home/lewis/evo-engine/16-pixel-image.png" };

	const bitmap loaded_pic = read_from_png( source_image_file );

	const bitmap expected_bitmap = bitmap_from_row_major_pixels( image_pixels, 4, 4 );

	/// TODO: Create an bitmap equality operator and use it here

	REQUIRE( loaded_pic.x_size() == expected_bitmap.x_size() );
	REQUIRE( loaded_pic.y_size() == expected_bitmap.x_size() );

	CHECK( loaded_pic.color_at( 0, 0 ) == image_pixels[ 0 ] );
	CHECK( loaded_pic.color_at( 1, 0 ) == image_pixels[ 1 ] );
	CHECK( loaded_pic.color_at( 2, 0 ) == image_pixels[ 2 ] );
	CHECK( loaded_pic.color_at( 3, 0 ) == image_pixels[ 3 ] );

	CHECK( loaded_pic.color_at( 0, 1 ) == image_pixels[ 4 ] );
	CHECK( loaded_pic.color_at( 1, 1 ) == image_pixels[ 5 ] );
	CHECK( loaded_pic.color_at( 2, 1 ) == image_pixels[ 6 ] );
	CHECK( loaded_pic.color_at( 3, 1 ) == image_pixels[ 7 ] );

	CHECK( loaded_pic.color_at( 0, 2 ) == image_pixels[ 8 ] );
	CHECK( loaded_pic.color_at( 1, 2 ) == image_pixels[ 9 ] );
	CHECK( loaded_pic.color_at( 2, 2 ) == image_pixels[ 10 ] );
	CHECK( loaded_pic.color_at( 3, 2 ) == image_pixels[ 11 ] );

	CHECK( loaded_pic.color_at( 0, 3 ) == image_pixels[ 12 ] );
	CHECK( loaded_pic.color_at( 1, 3 ) == image_pixels[ 13 ] );
	CHECK( loaded_pic.color_at( 2, 3 ) == image_pixels[ 14 ] );
	CHECK( loaded_pic.color_at( 3, 3 ) == image_pixels[ 15 ] );
}

TEST_CASE( "write_to_png works" ) {
	// const path source_image_file{ "/home/lewis/evo-engine/16-pixel-image.png" };

	const bitmap the_bitmap = bitmap_from_row_major_pixels( image_pixels, 4, 4 );

	write_to_png( the_bitmap, path{ "/tmp/fred.png" } );
}
