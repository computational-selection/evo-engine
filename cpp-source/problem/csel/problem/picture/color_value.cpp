#include "color_value.hpp"

#include <ostream>

#include <fmt/core.h>

using ::std::ostream;
using ::std::string;

/// \brief Generate a string describing the contents of the specified color_value
///
/// \param prm_color_pixel The color_value to describe
string csel::prob::pict::contents_to_string( const color_value &prm_color_pixel ) {
	return fmt::format( "{:>5.3f} {:>5.3f} {:>5.3f}", prm_color_pixel.red, prm_color_pixel.green, prm_color_pixel.blue );
}

/// \brief Generate a string describing the specified color_value
///
/// \param prm_color_pixel The color_value to describe
string csel::prob::pict::to_string( const color_value &prm_color_pixel ) {
	return fmt::format( "color_value[{}]", contents_to_string( prm_color_pixel ) );
}

/// \brief Insert a description of the specified color_value into the specified ostream
///
/// \param prm_ostream     The ostream into which the description should be inserted
/// \param prm_color_pixel The color_value to describe
ostream &csel::prob::pict::operator<<( ostream &prm_ostream, const color_value &prm_color_pixel ) {
	prm_ostream << to_string( prm_color_pixel );
	return prm_ostream;
}
