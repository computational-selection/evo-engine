#include "bitmap.hpp"

#include <limits>
#include <stdexcept>

#include <boost/numeric/conversion/cast.hpp>

#include <spdlog/spdlog.h>

#include <fmt/core.h>

#include "CImg.h"

#include "csel/util/exception/csel_throw_exception.hpp"
#include "csel/util/exception/error_info.hpp"
#include "csel/util/str_join.hpp"

using namespace csel::prob::pict;
using namespace csel::except;

using ::boost::numeric_cast;
using ::std::filesystem::path;
using ::std::numeric_limits;
using ::std::ostream;
using ::std::runtime_error;
using ::std::string;

namespace views = ::ranges::views;

/// \brief Write a color_value to the specified location of the specified CImg
///
/// \param prm_img         The CImg to which the value should be written
/// \param prm_x           The x coordinate in the CImg
/// \param prm_y           The y coordinate in the CImg
/// \param prm_color_pixel The color_value to write
template <typename... Ts>
static void write_color_pixel( cimg_library::CImg<Ts...> &prm_img,
                               const uint32_t &           prm_x,
                               const uint32_t &           prm_y,
                               const color_value &        prm_color_pixel ) {
	using cimg_data_type    = typename cimg_library::CImg<Ts...>::value_type;
	constexpr auto cimg_max = static_cast<color_value::data_type>( numeric_limits<cimg_data_type>::max() );

	prm_img( prm_x, prm_y, 0 ) = numeric_cast<cimg_data_type>( cimg_max * prm_color_pixel.red );
	prm_img( prm_x, prm_y, 1 ) = numeric_cast<cimg_data_type>( cimg_max * prm_color_pixel.green );
	prm_img( prm_x, prm_y, 2 ) = numeric_cast<cimg_data_type>( cimg_max * prm_color_pixel.blue );
}

/// \brief Read a color_pixed from the specified location of the specified CImg
///
/// \param prm_img The CImg from which the value should be read
/// \param prm_x   The x coordinate in the CImg
/// \param prm_y   The y coordinate in the CImg
template <typename... Ts>
static color_value read_color_pixel( const cimg_library::CImg<Ts...> &prm_img, const uint32_t &prm_x, const uint32_t &prm_y ) {
	using cimg_data_type    = typename cimg_library::CImg<Ts...>::value_type;
	constexpr auto cimg_max = static_cast<color_value::data_type>( numeric_limits<cimg_data_type>::max() );

	return { numeric_cast<color_value::data_type>( prm_img( prm_x, prm_y, 0 ) ) / cimg_max,
		     numeric_cast<color_value::data_type>( prm_img( prm_x, prm_y, 1 ) ) / cimg_max,
		     numeric_cast<color_value::data_type>( prm_img( prm_x, prm_y, 2 ) ) / cimg_max };
}

/// \brief Check that the specified CImg has a depth() of 1 and spectrum() (number of color channels) of 3
///
/// \param prm_img The CImg to check
template <typename... Ts>
static void check_depth_and_spectrum_of_cimg( const cimg_library::CImg<Ts...> &prm_img ) {
	if ( prm_img.depth() != 1 ) {
		CSEL_THROW(
		  runtime_error( fmt::format( "Cannot process an image with depth {}, must be 1", prm_img.depth() ) ) );
	}
	if ( prm_img.spectrum() != 3 ) {
		CSEL_THROW(
		  runtime_error( fmt::format( "Cannot process an image with {} colour channel(s), must be 3", prm_img.depth() ) ) );
	}
}

/// Ctor from the width and height
///
/// \param prm_x_size The width in pixels
/// \param prm_y_size The height in pixels
bitmap::bitmap( const size_t &prm_x_size, const size_t &prm_y_size ) :
        pixels( prm_x_size * prm_y_size, BLACK_COLOR_VALUE ), x_spec{ 1, prm_x_size }, y_spec{ prm_x_size, prm_y_size } {
}

/// Set the color_value at the specified location
///
/// \param prm_x     The x coordinate in the CImg
/// \param prm_y     The y coordinate in the CImg
/// \param prm_pixel The color_value to write
bitmap &bitmap::set_color( const size_t &prm_x, const size_t &prm_y, const color_value &prm_pixel ) {
	color_at( prm_x, prm_y ) = prm_pixel;
	return *this;
}

/// Write the specified color bitmap to the specified JPEG file
///
/// Beware: JPEG isn't completely lossless, even at quality=100
///
/// \param prm_bitmap     The bitmap to write
/// \param prm_image_file The file to which the JPEG bitmap should be written
void csel::prob::pict::write_to_jpeg( const bitmap &prm_bitmap, const path &prm_image_file ) {
	try {
		cimg_library::CImg<unsigned char> img(
		  numeric_cast<unsigned int>( prm_bitmap.x_size() ), numeric_cast<unsigned int>( prm_bitmap.y_size() ), 1, 3 );
		check_depth_and_spectrum_of_cimg( img );
		for ( const auto &[ x, y ] : x_y_of( prm_bitmap ) ) {
			write_color_pixel( img, numeric_cast<uint32_t>( x ), numeric_cast<uint32_t>( y ), prm_bitmap.color_at( x, y ) );
		}
		img.save_jpeg( prm_image_file.string().c_str() );
	} catch ( const ::boost::exception &ex ) {
		ex << except::input_file_error_info( prm_image_file );
		throw;
	}
}

/// Write the specified color bitmap to the specified PNG file
///
/// \param prm_bitmap     The bitmap to write
/// \param prm_image_file The file to which the PNG bitmap should be written
void csel::prob::pict::write_to_png( const bitmap &prm_bitmap, const path &prm_image_file ) {
	try {
		cimg_library::CImg<unsigned char> img(
		  numeric_cast<unsigned int>( prm_bitmap.x_size() ), numeric_cast<unsigned int>( prm_bitmap.y_size() ), 1, 3 );
		check_depth_and_spectrum_of_cimg( img );
		for ( const auto &[ x, y ] : x_y_of( prm_bitmap ) ) {
			write_color_pixel( img, numeric_cast<uint32_t>( x ), numeric_cast<uint32_t>( y ), prm_bitmap.color_at( x, y ) );
		}
		img.save_png( prm_image_file.string().c_str() );
	} catch ( const ::boost::exception &ex ) {
		ex << except::input_file_error_info( prm_image_file );
		throw;
	}
}

/// Read a color bitmap from the specified jpeg file
///
/// \param prm_image_file The jpeg file from which the image should b read
bitmap csel::prob::pict::read_from_jpeg( const path &prm_image_file ) {
	try {
		cimg_library::CImg<unsigned char> img;
		img.load_jpeg( prm_image_file.string().c_str() );
		check_depth_and_spectrum_of_cimg( img );
		bitmap result( numeric_cast<size_t>( img.width() ), numeric_cast<size_t>( img.height() ) );
		for ( const auto &[ x, y ] : x_y_of( result ) ) {
			result.set_color( x, y, read_color_pixel( img, numeric_cast<uint32_t>( x ), numeric_cast<uint32_t>( y ) ) );
		}
		return result;
	} catch ( const ::boost::exception &ex ) {
		ex << input_file_error_info( prm_image_file );
		throw;
	}
}

/// Read a color bitmap from the specified PNG file
///
/// \param prm_image_file The PNG file from which the image should b read
bitmap csel::prob::pict::read_from_png( const path &prm_image_file ) {
	try {
		cimg_library::CImg<unsigned char> img;
		img.load_png( prm_image_file.string().c_str() );
		check_depth_and_spectrum_of_cimg( img );
		bitmap result( numeric_cast<size_t>( img.width() ), numeric_cast<size_t>( img.height() ) );
		for ( const auto &[ x, y ] : x_y_of( result ) ) {
			result.set_color( x, y, read_color_pixel( img, numeric_cast<uint32_t>( x ), numeric_cast<uint32_t>( y ) ) );
		}
		return result;
	} catch ( const ::boost::exception &ex ) {
		ex << input_file_error_info( prm_image_file );
		throw;
	}
}

/// \brief Generate a string describing the specified bitmap
///
/// \param prm_bitmap The bitmap to describe
string csel::prob::pict::row_string( const bitmap &prm_bitmap, const size_t &prm_y ) {
	// clang-format off
	return str_join(
		views::indices( prm_bitmap.x_size() )
			| views::transform( [&] (const size_t &x) {
				return fmt::format( "[{}]", contents_to_string( prm_bitmap.color_at( x, prm_y ) ) );
			} ),
		", "
	);
	// clang-format on
}

/// \brief Generate a string describing the specified bitmap
///
/// \param prm_bitmap The bitmap to describe
string csel::prob::pict::to_string( const bitmap &prm_bitmap ) {
	const string join_string = ", ";
	return fmt::format( "bitmap[ {} x {} :", prm_bitmap.x_size(), prm_bitmap.y_size() )
		+ str_join(
			views::indices( prm_bitmap.y_size() )
				| views::transform( [&]( const size_t &y ) {
					return "\n\t" + row_string( prm_bitmap, y );
				} )
		)
		+ "\n]";
}

/// \brief Insert a description of the specified bitmap into the specified ostream
///
/// \param prm_ostream The ostream into which the description should be inserted
/// \param prm_bitmap  The bitmap to describe
ostream &csel::prob::pict::operator<<( ostream &prm_ostream, const bitmap &prm_bitmap ) {
	prm_ostream << to_string( prm_bitmap );
	return prm_ostream;
}
