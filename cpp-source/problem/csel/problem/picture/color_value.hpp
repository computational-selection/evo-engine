#ifndef _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_COLOR_VALUE_HPP
#define _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_COLOR_VALUE_HPP

#include <iosfwd>
#include <string>

namespace csel::prob::pict {

	/// \brief Represent a color value as three floats in [0, 1] for the R, G, B components
	///
	/// All colours to be within [ 0, 1 ]
	///
	/// Not currently ::boost::equality_comparable<color_value> because that breaks aggregate initialization
	struct color_value {

		/// The underlying type to use for each of the components
		using data_type = float;

		/// The red component in [0, 1]
		data_type red;

		/// The green component in [0, 1
		data_type green;

		/// The blue component in [0, 1
		data_type blue;
	};

	/// Whether the two color_values are equal
	///
	/// \param prm_lhs The first  color_value to compare
	/// \param prm_rhs The second color_value to compare
	constexpr bool operator==( const color_value &prm_lhs, const color_value &prm_rhs ) {
		// clang-format off
		return (
			( prm_lhs.red   == prm_rhs.red   )
			&&
			( prm_lhs.green == prm_rhs.green )
			&&
			( prm_lhs.blue  == prm_rhs.blue  )
		);
		// clang-format on
	}

	::std::string contents_to_string( const color_value & );

	::std::string to_string( const color_value & );

	::std::ostream &operator<<( ::std::ostream &, const color_value & );

	/// I see a red door but...
	inline constexpr color_value BLACK_COLOR_VALUE = { 0.0F, 0.0F, 0.0F };

} // namespace csel::prob::pict

#endif // _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_COLOR_VALUE_HPP
