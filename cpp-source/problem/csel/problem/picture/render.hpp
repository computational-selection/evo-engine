#ifndef _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_RENDER_HPP
#define _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_RENDER_HPP

#include <vector>

#include "csel/engine/individual.hpp"
#include "csel/problem/picture/bitmap.hpp"
#include "csel/problem/picture/dimension_range.hpp"

namespace csel::prob::pict {

	bitmap render( const csel::individual &, const dimension_range &, const size_t &, const dimension_range &, const size_t & );

} // namespace csel::prob::pict

#endif // _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_RENDER_HPP
