#include "render.hpp"

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/view/indices.hpp>

#include "csel/problem/picture/bitmap.hpp"

using namespace csel;
using namespace csel::prob::pict;

namespace views = ::ranges::views;

/// \brief Render the specified individual over the specified range into a bitmap with the specified dimensions
///
/// \param prm_individual   The individual to render
/// \param prm_x_dim_range  The x range over which the individual should be rendered
/// \param prm_num_x_pixels The number of pixels on the x axis
/// \param prm_y_dim_range  The y range over which the individual should be rendered
/// \param prm_num_y_pixels The number of pixels on the y axis
bitmap csel::prob::pict::render( [[maybe_unused]] const individual &     prm_individual,
                                 const dimension_range &prm_x_dim_range,
                                 const size_t &         prm_num_x_pixels,
                                 const dimension_range &prm_y_dim_range,
                                 const size_t &         prm_num_y_pixels ) {
	// Create the bitmap
	bitmap result( prm_num_x_pixels, prm_num_y_pixels );

	// Loop over the x and y to cover all pixels
	for ( const size_t &index_x : views::indices( prm_num_x_pixels ) ) {
		const float x = steps_through_dim_range( index_x, prm_num_x_pixels, prm_x_dim_range );
		for ( const size_t &index_y : views::indices( prm_num_y_pixels ) ) {
			const float y = steps_through_dim_range( index_y, prm_num_y_pixels, prm_y_dim_range );

			// Compute the value of the individual at the point and set the corresponding color in result
			// const color_value the_pixel = compute( prm_individual, x, y );
			const color_value the_pixel{ x, y, 0.0F };
			result.set_color( index_x, index_y, the_pixel );
		}
	}

	// Return the resulting bitmap
	return result;
}
