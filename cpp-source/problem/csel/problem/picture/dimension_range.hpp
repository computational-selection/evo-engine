#ifndef _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_DIMENSION_RANGE_HPP
#define _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_DIMENSION_RANGE_HPP

#include <gsl/gsl>

#include "csel/util/lerp.hpp"
#include "csel/util/size_t_literal.hpp"

namespace csel::prob::pict {

	/// \pre start and stop must be normal float values that don't equal each other
	///
	/// stop should typically be greater than start, though it might make sense to invert
	/// in some circumstances
	struct dimension_range {
		float start;
		float stop;
	};

	/// \brief Find the real value corresponding to the specified index with the specified number of points
	///        over the specified range
	///
	/// \param prm_point_index The index of interest
	/// \param prm_num_points  The number of points
	/// \param prm_dim_range   The range of interest
	constexpr float steps_through_dim_range( const size_t &         prm_point_index,
	                                         const size_t &         prm_num_points,
	                                         const dimension_range &prm_dim_range ) {
		Expects( prm_num_points > 0 );
		Expects( prm_point_index < prm_num_points );

		constexpr auto HALF = static_cast<decltype( dimension_range::start )>( 0.5 );

		return lerp( prm_dim_range.start,
		             prm_dim_range.stop,
		             ( prm_num_points > 1 )
		               ? static_cast<float>( static_cast<double>( prm_point_index ) / static_cast<double>( prm_num_points - 1_z ) )
		               : HALF );
	}

} // namespace csel::prob::pict

#endif // _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_DIMENSION_RANGE_HPP
