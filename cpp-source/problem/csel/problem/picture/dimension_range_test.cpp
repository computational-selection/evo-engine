#include <catch2/catch.hpp>

#include "csel/problem/picture/dimension_range.hpp"

using namespace csel::prob::pict;

TEST_CASE( "dimension_range ctor" ) {
	STATIC_REQUIRE( dimension_range{ 0.0F, 1.0F }.start == 0.0F );
	STATIC_REQUIRE( dimension_range{ 0.0F, 1.0F }.stop == 1.0F );
}

TEST_CASE( "steps_through_dim_range" ) {
	STATIC_REQUIRE( steps_through_dim_range( 0, 1, dimension_range{ 10.0F, 20.0F } ) == 15.0F );

	STATIC_REQUIRE( steps_through_dim_range( 0, 2, dimension_range{ 10.0F, 20.0F } ) == 10.0F );
	STATIC_REQUIRE( steps_through_dim_range( 1, 2, dimension_range{ 10.0F, 20.0F } ) == 20.0F );

	STATIC_REQUIRE( steps_through_dim_range( 0, 3, dimension_range{ 10.0F, 20.0F } ) == 10.0F );
	STATIC_REQUIRE( steps_through_dim_range( 1, 3, dimension_range{ 10.0F, 20.0F } ) == 15.0F );
	STATIC_REQUIRE( steps_through_dim_range( 2, 3, dimension_range{ 10.0F, 20.0F } ) == 20.0F );

	STATIC_REQUIRE( steps_through_dim_range( 0, 5, dimension_range{ 10.0F, 20.0F } ) == 10.0F );
	STATIC_REQUIRE( steps_through_dim_range( 1, 5, dimension_range{ 10.0F, 20.0F } ) == 12.5F );
	STATIC_REQUIRE( steps_through_dim_range( 2, 5, dimension_range{ 10.0F, 20.0F } ) == 15.0F );
	STATIC_REQUIRE( steps_through_dim_range( 3, 5, dimension_range{ 10.0F, 20.0F } ) == 17.5F );
	STATIC_REQUIRE( steps_through_dim_range( 4, 5, dimension_range{ 10.0F, 20.0F } ) == 20.0F );
}
