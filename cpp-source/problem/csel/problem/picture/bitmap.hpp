#ifndef _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_BITMAP_HPP
#define _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_BITMAP_HPP

#include <filesystem>
#include <tuple>
#include <vector>

#ifdef _GLIBCXX_DEBUG
#define RANGES_NO_STD_FORWARD_DECLARATIONS
#endif
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/indices.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>

#include "csel/problem/picture/color_value.hpp"
#include "csel/util/exception/csel_throw_exception.hpp"

namespace csel::prob::pict {

	namespace detail {

		/// \brief The mdspan-like details (ie stride and size) for one dimension for use in bitmap
		struct mdspanlike_dim {

			/// \brief The stride for this dimension, ie the step size to move one element
			///        forward in this dimension
			size_t stride;

			/// \brief The number of elements in this dimension
			size_t size;
		};

	} // namespace detail

	/// \brief Represent a color bitmap
	class bitmap {
	  private:
		/// \brief The pixels
		::std::vector<color_value> pixels;

		/// \brief The mdspan-like details for acessing the x-dimension
		detail::mdspanlike_dim x_spec;

		/// \brief The mdspan-like details for acessing the y-dimension
		detail::mdspanlike_dim y_spec;

		/// \brief Implementation const/non-const common static-method for accessing the color at the specified coordinates
		///
		/// \param prm_bitmap The bitmap to query
		/// \param prm_x      The x coordinate
		/// \param prm_y      The y coordinate
		template <typename BM>
		static auto color_at_impl( BM &prm_bitmap, const size_t &prm_x, const size_t &prm_y )
		  -> decltype( prm_bitmap.color_at( prm_x, prm_y ) ) {
			return prm_bitmap.pixels[ ( prm_x * prm_bitmap.x_spec.stride ) + ( prm_y * prm_bitmap.y_spec.stride ) ];
		}

		color_value &color_at( const size_t &, const size_t & );

	  public:
		bitmap( const size_t &, const size_t & );

		[[nodiscard]] const color_value &color_at( const size_t &, const size_t & ) const;
		bitmap &                         set_color( const size_t &, const size_t &, const color_value & );

		[[nodiscard]] const size_t &x_size() const;
		[[nodiscard]] const size_t &y_size() const;
	};

	/// Non-const (private) access to the color at the specified coordinates
	///
	/// \param prm_x The x coordinate
	/// \param prm_y The y coordinate
	inline color_value &bitmap::color_at( const size_t &prm_x, const size_t &prm_y ) {
		return color_at_impl( *this, prm_x, prm_y );
	}

	/// Const access to the color at the specified coordinates
	///
	/// \param prm_x The x coordinate
	/// \param prm_y The y coordinate
	inline const color_value &bitmap::color_at( const size_t &prm_x, const size_t &prm_y ) const {
		return color_at_impl( *this, prm_x, prm_y );
	}

	/// The width of the bitmap
	inline const size_t &bitmap::x_size() const {
		return x_spec.size;
	}

	/// The height of the bitmap
	inline const size_t &bitmap::y_size() const {
		return y_spec.size;
	}

	/// Get a range over all [ x, y ] coordinates (row-major)
	///
	/// \param prm_bitmap The bitmap whose coordinates should be returned
	inline decltype( auto ) x_y_of( const bitmap &prm_bitmap ) {
		return ::ranges::views::cartesian_product( ::ranges::views::indices( prm_bitmap.y_size() ),
		                                           ::ranges::views::indices( prm_bitmap.x_size() ) )
		       | ::ranges::views::transform( []( const auto &y_and_x ) {
			         const auto &[ y, x ] = y_and_x;
			         return ::std::tuple{ x, y };
		         } );
	}

	template <typename Rng>
	bitmap bitmap_from_row_major_pixels( Rng &&prm_pixels_data, const size_t &prm_width, const size_t &prm_height ) {
		if ( ::ranges::size( prm_pixels_data ) != prm_width * prm_height ) {
			CSEL_THROW(std::invalid_argument("Number of pixels doesn't match widht * height"));
		}

		bitmap result( prm_width, prm_height );
		for (const auto &[ x_and_y, source_pixel ] : ::ranges::views::zip( x_y_of( result ), prm_pixels_data ) ) {
			const auto &[ x, y ] = x_and_y;
			result.set_color( x, y, source_pixel );
		}
		return result;
	}

	void write_to_jpeg( const bitmap &, const ::std::filesystem::path & );

	void write_to_png( const bitmap &, const ::std::filesystem::path & );

	bitmap read_from_jpeg( const ::std::filesystem::path & );

	bitmap read_from_png( const ::std::filesystem::path & );

	::std::string row_string( const bitmap &, const size_t & );

	::std::string to_string( const bitmap & );

	::std::ostream &operator<<( ::std::ostream &, const bitmap & );

} // namespace csel::prob::pict

#endif // _EVO_ENGINE_CPP_SOURCE_PROBLEM_CSEL_PROBLEM_PICTURE_BITMAP_HPP
