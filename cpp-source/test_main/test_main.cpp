#include <string>

#include <fmt/core.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <boost/exception/diagnostic_information.hpp>

#include "csel/util/exception/retrieve_exception_info.hpp"
#include "csel/util/source_location_details.hpp"

using namespace ::csel::except;

using ::std::string;

/// Register an exception translator so that the tests will give more info if
/// for any unexpected boost::exceptions that get thrown during the tests
///
/// \param prm_exception The Boost exception to handle
CATCH_TRANSLATE_EXCEPTION( const ::boost::exception &prm_exception ) {
	return string{ "In " } + CSEL_SOURCE_LOCATION_STRING + ", CSEL code translated an exception on behalf of catch:\n"
	       + retrieve_exception_info( prm_exception );
}
